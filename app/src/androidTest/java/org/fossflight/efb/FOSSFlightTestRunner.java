package org.fossflight.efb;


import com.squareup.rx2.idler.Rx2Idler;

import androidx.test.runner.AndroidJUnitRunner;
import io.reactivex.plugins.RxJavaPlugins;

public class FOSSFlightTestRunner extends AndroidJUnitRunner {
    @Override
    public void onStart() {
        /*  Register an IdlingResource with Espresso when creating a new Thread
            This will make Espresso wait until that thread is free before proceeding. */
        RxJavaPlugins.setInitNewThreadSchedulerHandler(
                Rx2Idler.create("RxJava 2.x New Thread Scheduler"));
        RxJavaPlugins.setInitIoSchedulerHandler(
                Rx2Idler.create("RxJava 2.x IO Scheduler"));

        super.onStart();
    }
}
