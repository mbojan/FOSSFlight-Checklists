package org.fossflight.efb.matchers;

import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.view.View;
import android.widget.ImageView;

import org.hamcrest.Description;
import org.hamcrest.TypeSafeMatcher;

import androidx.core.content.ContextCompat;

public class BitmapDrawableMatcher extends TypeSafeMatcher<View> {

    private final int expectedId;
    private String expectedResourceName;

    BitmapDrawableMatcher(int resourceId) {
        super(View.class);
        this.expectedId = resourceId;
    }

    @Override
    protected boolean matchesSafely(View target) {
        expectedResourceName = target.getContext().getResources().getResourceEntryName(expectedId);

        if (!(target instanceof ImageView)){
            return false;
        }
        ImageView actualImageView = (ImageView) target;

        if (expectedId < 0){
            return actualImageView.getDrawable() == null;
        }

        Drawable expectedDrawable = ContextCompat.getDrawable(target.getContext(), expectedId);
        if (expectedDrawable == null) {
            return false;
        }

        Bitmap expectedBitmap = ((BitmapDrawable) expectedDrawable).getBitmap();
        Bitmap actualBitmap = ((BitmapDrawable) actualImageView.getDrawable()).getBitmap();

        return expectedBitmap.sameAs(actualBitmap);
    }

    @Override
    public void describeTo(Description description) {
        description.appendText("with drawable from resource id: ");
        description.appendValue(expectedId);
        if (expectedResourceName != null) {
            description.appendText("[");
            description.appendText(expectedResourceName);
            description.appendText("]");
        }
    }
}