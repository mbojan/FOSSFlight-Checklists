package org.fossflight.efb.matchers;

import android.view.View;

import com.google.android.material.textfield.TextInputLayout;

import org.hamcrest.Description;
import org.hamcrest.TypeSafeMatcher;

public class TextInputLayoutErrorMatcher extends TypeSafeMatcher<View> {

    private final String expectedErrorText;
    private String actualErrorMessage;

    TextInputLayoutErrorMatcher(final String expectedErrorText) {
        super(View.class);
        this.expectedErrorText = expectedErrorText;
    }

    @Override
    public boolean matchesSafely(View view) {
        if (!(view instanceof TextInputLayout)) {
            return false;
        }

        CharSequence error = ((TextInputLayout) view).getError();

        if (error == null) {
            return false;
        }

        actualErrorMessage = error.toString();
        return expectedErrorText.equals(actualErrorMessage);
    }

    @Override
    public void describeTo(Description description) {
        description.appendText("Error message: ");
        description.appendValue(expectedErrorText);
    }
}
