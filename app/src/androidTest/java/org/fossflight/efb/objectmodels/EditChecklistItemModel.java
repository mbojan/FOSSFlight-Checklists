package org.fossflight.efb.objectmodels;

import org.fossflight.efb.R;

import androidx.annotation.NonNull;
import androidx.test.espresso.DataInteraction;
import androidx.test.espresso.ViewInteraction;

import static androidx.test.espresso.Espresso.onData;
import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.matcher.ViewMatchers.withId;
import static org.hamcrest.CoreMatchers.instanceOf;
import static org.hamcrest.core.AllOf.allOf;
import static org.hamcrest.core.Is.is;

public class EditChecklistItemModel {

    @NonNull
    public static DataInteraction onChecklistItemTypeSectionHeader() {
        return onData(allOf(is(instanceOf(String.class)), is("SECTION HEADER")));
    }

    public static ViewInteraction onChecklistItemTypeSpinner() {
        return onView(withId(R.id.checklist_item_type_spinner));
    }

    public static ViewInteraction onChecklistItemNameInput() {
        return onView(withId(R.id.checklist_item_name_edit));
    }

    public static ViewInteraction onChecklistItemActionInput() {
        return onView(withId(R.id.checklist_item_action_edit));
    }
}
