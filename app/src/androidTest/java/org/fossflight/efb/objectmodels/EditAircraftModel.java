package org.fossflight.efb.objectmodels;

import org.fossflight.efb.R;

import androidx.test.espresso.ViewInteraction;

import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.matcher.ViewMatchers.withId;

public class EditAircraftModel {
    public static ViewInteraction onTailNumberInput() {
        return onView(withId(R.id.aircraft_tail_number_edit));
    }

    public static ViewInteraction onManufacturerInput() {
        return onView(withId(R.id.aircraft_manufacturer_edit));
    }

    public static ViewInteraction onModelInput() {
        return onView(withId(R.id.aircraft_model_edit));
    }

    public static ViewInteraction onYearInput() {
        return onView(withId(R.id.aircraft_year_edit));
    }

    public static ViewInteraction checklistTypeSpinner() {
        return onView(withId(R.id.checklist_type_spinner));
    }
}
