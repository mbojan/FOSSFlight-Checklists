package org.fossflight.efb.objectmodels.assertion;

import org.fossflight.efb.R;

import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.assertion.ViewAssertions.matches;
import static androidx.test.espresso.matcher.ViewMatchers.isClickable;
import static androidx.test.espresso.matcher.ViewMatchers.withId;
import static org.fossflight.efb.objectmodels.RecyclerViewActions.onTheListView;
import static org.fossflight.efb.objectmodels.assertion.RecyclerViewAssertions.cardAtPositionHasContentText;
import static org.hamcrest.CoreMatchers.not;

public class ChecklistCollectionAssert {
    public static void assertHasChecklistCount(int expectedCount) {
        onTheListView().check(RecyclerViewAssertions.hasItemCount(expectedCount));
    }

    public static void assertCardAtPositionHasChecklistName(int position, String checklistName) {
        onTheListView().check(cardAtPositionHasContentText(position, checklistName));
    }

    public static void assertPlayChecklistButtonIsDisabled() {
        onView(withId(R.id.play_button)).check(matches(not(isClickable())));
    }

    public static void assertPlayChecklistButtonIsEnabled() {
        onView(withId(R.id.play_button)).check(matches(isClickable()));
    }
}
