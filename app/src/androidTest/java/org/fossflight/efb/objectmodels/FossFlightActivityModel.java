package org.fossflight.efb.objectmodels;

import org.fossflight.efb.R;

import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.action.ViewActions.click;
import static androidx.test.espresso.matcher.ViewMatchers.withContentDescription;
import static androidx.test.espresso.matcher.ViewMatchers.withId;

public class FossFlightActivityModel {

    public static void tapSaveButton() {
        onView(withId(R.id.save)).perform(click());
    }

    public static void tapCancelButton() {
        onView(withContentDescription(R.string.abc_action_bar_up_description)).perform(click());
    }
}
