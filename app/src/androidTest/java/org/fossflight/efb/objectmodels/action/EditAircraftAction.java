package org.fossflight.efb.objectmodels.action;

import static androidx.test.espresso.Espresso.onData;
import static androidx.test.espresso.action.ViewActions.clearText;
import static androidx.test.espresso.action.ViewActions.click;
import static androidx.test.espresso.action.ViewActions.closeSoftKeyboard;
import static androidx.test.espresso.action.ViewActions.typeText;
import static org.fossflight.efb.objectmodels.EditAircraftModel.checklistTypeSpinner;
import static org.fossflight.efb.objectmodels.EditAircraftModel.onManufacturerInput;
import static org.fossflight.efb.objectmodels.EditAircraftModel.onModelInput;
import static org.fossflight.efb.objectmodels.EditAircraftModel.onTailNumberInput;
import static org.fossflight.efb.objectmodels.EditAircraftModel.onYearInput;
import static org.hamcrest.CoreMatchers.instanceOf;
import static org.hamcrest.core.AllOf.allOf;
import static org.hamcrest.core.Is.is;

public class EditAircraftAction {
    public static void fillInTailNumber(String tailNumber) {
        onTailNumberInput().perform(typeText(tailNumber), closeSoftKeyboard());
    }

    public static void updateTailNumberTo(String newTailNumber) {
        onTailNumberInput().perform(clearText(), typeText(newTailNumber), closeSoftKeyboard());
    }

    public static void updateManufacturerTo(String newManufacturer) {
        onManufacturerInput().perform(clearText(), typeText(newManufacturer), closeSoftKeyboard());
    }

    public static void updateModelTo(String newModel) {
        onModelInput().perform(clearText(), typeText(newModel), closeSoftKeyboard());
    }

    public static void updateYearTo(String newYear) {
        onYearInput().perform(clearText(), typeText(newYear), closeSoftKeyboard());
    }

    public static void selectChecklistCollectionType(String checklistTypeName) {
        checklistTypeSpinner().perform(click());
        onData(allOf(is(instanceOf(String.class)), is(checklistTypeName))).perform(click());
    }
}