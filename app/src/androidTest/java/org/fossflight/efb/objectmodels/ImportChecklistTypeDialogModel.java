package org.fossflight.efb.objectmodels;

import org.fossflight.efb.R;

import androidx.test.espresso.ViewInteraction;

import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.matcher.ViewMatchers.withText;

public class ImportChecklistTypeDialogModel {

    public static ViewInteraction onFltPlanGoChecklistText() {
        return onView(withText(R.string.fltplan_go_checklist));
    }

    public static ViewInteraction onFossFLightChecklistText() {
        return onView(withText(R.string.fossflight_checklist));
    }
}
