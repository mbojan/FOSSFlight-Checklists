package org.fossflight.efb.objectmodels.action;

import static androidx.test.espresso.action.ViewActions.click;
import static org.fossflight.efb.objectmodels.AddAircraftDialogModel.onAddAircraftManuallyText;
import static org.fossflight.efb.objectmodels.AddAircraftDialogModel.onImportAircraftText;
import static org.fossflight.efb.objectmodels.AddAircraftDialogModel.onImportExampleAircraftText;

public class AddAircraftDialogAction {
    public static void tapImportAircraftItemInDialog() {
        onImportAircraftText().perform(click());
    }

    public static void tapAddAircraftManuallyItemInDialog() {
        onAddAircraftManuallyText().perform(click());
    }

    public static void tapImportExampleAircraftItemInDialog() {
        onImportExampleAircraftText().perform(click());
    }
}