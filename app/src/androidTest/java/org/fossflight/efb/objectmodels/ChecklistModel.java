package org.fossflight.efb.objectmodels;

import org.fossflight.efb.R;

import androidx.test.espresso.ViewInteraction;

import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.Espresso.openActionBarOverflowOrOptionsMenu;
import static androidx.test.espresso.matcher.ViewMatchers.withId;
import static androidx.test.espresso.matcher.ViewMatchers.withText;
import static androidx.test.platform.app.InstrumentationRegistry.getInstrumentation;

public class ChecklistModel {

    public static ViewInteraction onPlayButton() {
        return onView(withId(R.id.play_button));
    }

    public static ViewInteraction onItemCompletedButton() {
        return onView(withId(R.id.play_button));
    }

    public static ViewInteraction onNextChecklistButton() {
        return onView(withId(R.id.play_button));
    }

    public static ViewInteraction onCancelText() {
        return onView(withText(R.string.cancel));
    }

    public static ViewInteraction onNoText() {
        return onView(withText(R.string.no));
    }

    public static ViewInteraction onYesText() {
        return onView(withText(R.string.yes));
    }

    public static ViewInteraction onSkipButton() {
        return onView(withId(R.id.skip_button));
    }

    public static ViewInteraction onPreviouslySkippedButton() {
        return onView(withId(R.id.revisit_previously_skipped_button));
    }

    public static ViewInteraction onResetChecklistText() {
        return onView(withText(R.string.reset_checklist));
    }

    public static ViewInteraction onAddChecklistItemText() {
        return onView(withText(R.string.add_checklist_item));
    }

    public static void openOptionsMenu() {
        openActionBarOverflowOrOptionsMenu(getInstrumentation().getTargetContext());
    }
}
