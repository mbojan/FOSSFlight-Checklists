package org.fossflight.efb.objectmodels.action;

import static androidx.test.espresso.action.ViewActions.click;
import static org.fossflight.efb.objectmodels.ChecklistModel.onAddChecklistItemText;
import static org.fossflight.efb.objectmodels.ChecklistModel.onItemCompletedButton;
import static org.fossflight.efb.objectmodels.ChecklistModel.onNextChecklistButton;
import static org.fossflight.efb.objectmodels.ChecklistModel.onNoText;
import static org.fossflight.efb.objectmodels.ChecklistModel.onPlayButton;
import static org.fossflight.efb.objectmodels.ChecklistModel.onPreviouslySkippedButton;
import static org.fossflight.efb.objectmodels.ChecklistModel.onResetChecklistText;
import static org.fossflight.efb.objectmodels.ChecklistModel.onSkipButton;
import static org.fossflight.efb.objectmodels.ChecklistModel.onYesText;

public class ChecklistAction {
    public static void playChecklist() {
        onPlayButton().perform(click());
    }

    public static void markItemAsCompleted() {
        onItemCompletedButton().perform(click());
    }

    public static void tapNoToDismissDialog() {
        onNoText().perform(click());
    }

    public static void tapYesToStartNextChecklist() {
        onYesText().perform(click());
    }

    public static void tapYesToReplaySkippedItems() {
        onYesText().perform(click());
    }

    public static void skipItem() {
        onSkipButton().perform(click());
    }

    public static void tapGoToPreviouslySkippedButton() {
        onPreviouslySkippedButton().perform(click());
    }

    public static void tapNextChecklistButton() {
        onNextChecklistButton().perform(click());
    }

    public static void tapResetChecklistText() {
        onResetChecklistText().perform(click());
    }

    public static void tapAddChecklistItemText() {
        onAddChecklistItemText().perform(click());
    }
}
