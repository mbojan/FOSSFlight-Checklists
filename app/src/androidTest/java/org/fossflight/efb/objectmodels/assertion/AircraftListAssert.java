package org.fossflight.efb.objectmodels.assertion;

import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.assertion.ViewAssertions.matches;
import static androidx.test.espresso.matcher.ViewMatchers.isDisplayed;
import static androidx.test.espresso.matcher.ViewMatchers.withText;
import static org.fossflight.efb.objectmodels.RecyclerViewActions.onTheListView;
import static org.fossflight.efb.objectmodels.assertion.RecyclerViewAssertions.cardAtPositionHasContentText;
import static org.fossflight.efb.objectmodels.assertion.RecyclerViewAssertions.cardAtPositionHasShortDescriptionText;

public class AircraftListAssert {
    public static void assertCardAtPositionHasTailNumber(int position, String tailNumber) {
        onTheListView().check(cardAtPositionHasContentText(position, tailNumber));
    }

    public static void assertCardAtPositionHasAircraftDescription(int position, String description) {
        onTheListView().check(cardAtPositionHasShortDescriptionText(position, description));
    }

    public static void assertHasAircraftCount(int expectedCount) {
        onTheListView().check(RecyclerViewAssertions.hasItemCount(expectedCount));
    }

    public static void assertPermissionsExplanationDialogIsShown() {
        onView(withText("We need write permissions to export!")).check(matches(isDisplayed()));
    }
}
