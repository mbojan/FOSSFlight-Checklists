package org.fossflight.efb.objectmodels.action;

import static androidx.test.espresso.action.ViewActions.clearText;
import static androidx.test.espresso.action.ViewActions.click;
import static androidx.test.espresso.action.ViewActions.closeSoftKeyboard;
import static androidx.test.espresso.action.ViewActions.typeText;
import static org.fossflight.efb.objectmodels.EditChecklistModel.onAddChecklistText;
import static org.fossflight.efb.objectmodels.EditChecklistModel.onChecklistNameInput;

public class EditChecklistAction {
    public static void fillInNameOfChecklist(String checklistName) {
        onChecklistNameInput()
                .perform(typeText(checklistName), closeSoftKeyboard());
    }

    public static void updateChecklistNameTo(String newChecklistName) {
        onChecklistNameInput()
                .perform(clearText(), typeText(newChecklistName), closeSoftKeyboard());
    }

    public static void tapAddChecklistText() {
        onAddChecklistText().perform(click());
    }
}