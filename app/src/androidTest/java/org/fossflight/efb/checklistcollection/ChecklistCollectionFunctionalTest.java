package org.fossflight.efb.checklistcollection;

import org.fossflight.efb.MainActivity;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;

import androidx.test.rule.ActivityTestRule;

import static org.fossflight.efb.objectmodels.ChecklistModel.openOptionsMenu;
import static org.fossflight.efb.objectmodels.RecyclerViewActions.deleteCardWithText;
import static org.fossflight.efb.objectmodels.RecyclerViewActions.numberOfCardsInTheRecyclerView;
import static org.fossflight.efb.objectmodels.RecyclerViewActions.tapCardWithText;
import static org.fossflight.efb.objectmodels.action.AddAircraftDialogAction.tapImportExampleAircraftItemInDialog;
import static org.fossflight.efb.objectmodels.action.AircraftListAction.tapAddAircraftText;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;

public class ChecklistCollectionFunctionalTest {

    @Rule
    public ActivityTestRule<MainActivity> mainActivityTestRule = new ActivityTestRule<>(MainActivity.class);

    @Before
    public void setup() {
        openOptionsMenu();
        tapAddAircraftText();
        tapImportExampleAircraftItemInDialog();
        tapCardWithText("NEXFC1");
    }

    @Test
    public void shouldDeleteAChecklist() {
        int checklistCountBeforeDeleting = numberOfCardsInTheRecyclerView();
        deleteCardWithText("Before Starting Engine");
        int checklistCountAfterDeleting = numberOfCardsInTheRecyclerView();

        assertThat(checklistCountBeforeDeleting - checklistCountAfterDeleting, is(1));
    }
}
