package org.fossflight.efb

import org.fossflight.efb.data.persistence.FFRepository

class FossFlightTestApplication : FossFlightApplication() {

    private var testAppModule: TestAppModule? = null

    override var repository: FFRepository
        get() = super.repository
        set(value) {
            super.repository = value
        }

    override var appModule: AppModule
        get() {
            return testAppModule ?: TestAppModule(this)
        }
        set(value) {
            super.appModule = value
        }

    fun setAppModule(appModule: TestAppModule) {
        this.testAppModule = appModule
        initComponent()
    }
}
