package org.fossflight.efb.checklistCollection

import android.os.Bundle
import androidx.fragment.app.testing.launchFragmentInContainer
import androidx.recyclerview.widget.RecyclerView
import androidx.test.core.app.ApplicationProvider
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.assertThat
import androidx.test.espresso.matcher.ViewMatchers.isDisplayed
import androidx.test.espresso.matcher.ViewMatchers.withId
import androidx.test.espresso.matcher.ViewMatchers.withText
import androidx.test.ext.junit.runners.AndroidJUnit4
import io.mockk.every
import io.mockk.mockk
import io.mockk.verify
import io.reactivex.Flowable
import io.reactivex.Single
import org.fossflight.efb.BundleKeys
import org.fossflight.efb.ForceTrampolineSchedulers
import org.fossflight.efb.FossFlightTestApplication
import org.fossflight.efb.R
import org.fossflight.efb.TestAppModule
import org.fossflight.efb.data.model.Aircraft
import org.fossflight.efb.data.persistence.FFRepository
import org.fossflight.efb.data.randomId
import org.fossflight.efb.data.randomName
import org.fossflight.efb.data.testChecklist
import org.hamcrest.CoreMatchers.`is`
import org.hamcrest.CoreMatchers.not
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.robolectric.annotation.Config


@Config(application = FossFlightTestApplication::class)
@RunWith(AndroidJUnit4::class)
class ChecklistCollectionPageFragmentTest {

    @get:Rule
    var forceTrampolineSchedulers = ForceTrampolineSchedulers()

    private val checklistCollectionId = randomId()
    private val checklists = listOf(
            testChecklist(),
            testChecklist(),
            testChecklist()
    )

    val instantiate: () -> ChecklistCollectionPageFragment = {
        val fragment = ChecklistCollectionPageFragment()
        fragment.setParentActivity(mockChecklistCollectionViewPagerFragmentListener)
        fragment
    }

    private val mockRepository = mockk<FFRepository>()
    private val mockChecklistCollectionViewPagerFragmentListener = mockk<ChecklistCollectionViewPagerFragmentListener>()

    @Before
    fun `set up`() {
        val application = ApplicationProvider.getApplicationContext<FossFlightTestApplication>()
        val testAppModule = TestAppModule(
                context = application.applicationContext,
                repository = mockRepository
        )

        application.setAppModule(testAppModule)

        every { mockRepository.allAircraft } returns Flowable.just<List<Aircraft>>(listOf())
        every { mockRepository.getChecklists(checklistCollectionId) } returns Single.just(checklists)

        every { mockChecklistCollectionViewPagerFragmentListener.displaySnackbar(any()) } returns Unit
    }

    @Test
    fun `should update the RecyclerView with the Aircraft Checklists`() {
        val arguments = Bundle()
        arguments.putLong(BundleKeys.CHECKLIST_COLLECTION_ID, checklistCollectionId)

        val scenario = launchFragmentInContainer(
                instantiate = instantiate,
                fragmentArgs = arguments
        )

        scenario.onFragment {
            val actualItemCount = it.requireView().findViewById<RecyclerView>(R.id.list).adapter!!.itemCount
            assertThat(actualItemCount, `is`(checklists.size))
        }
    }

    @Test
    fun `should hide the Empty List Message when Checklists are present`() {
        val arguments = Bundle()
        arguments.putLong(BundleKeys.CHECKLIST_COLLECTION_ID, checklistCollectionId)

        launchFragmentInContainer(
                instantiate = instantiate,
                fragmentArgs = arguments
        )

        onView(withId(R.id.empty)).check(matches(not(isDisplayed())))
    }

    @Test
    fun `should display the Empty Checklist Message when there are zero Checklists`() {
        val emptyChecklistCollectionId = randomId()

        every { mockRepository.getChecklists(emptyChecklistCollectionId) } returns Single.just(listOf())

        val arguments = Bundle()
        arguments.putLong(BundleKeys.CHECKLIST_COLLECTION_ID, emptyChecklistCollectionId)

        launchFragmentInContainer(
                instantiate = instantiate,
                fragmentArgs = arguments
        )

        val emptyListMessage = "It looks like you don't have any Checklists yet.\nTap 'Add Checklist' in the options menu to add one."
        onView(withId(R.id.empty)).check(matches(isDisplayed()))
        onView(withId(R.id.empty)).check(matches(withText(emptyListMessage)))
    }

    @Test
    fun `should display the Delete Checklist Dialog after deleting a Checklist`() {
        val arguments = Bundle()
        arguments.putLong(BundleKeys.CHECKLIST_COLLECTION_ID, checklistCollectionId)

        val scenario = launchFragmentInContainer(
                instantiate = instantiate,
                fragmentArgs = arguments
        )

        val checklistName = randomName()

        scenario.onFragment {
            it.displayDeletedChecklistDialog(checklistName)
        }

        val expectedMessage = "Deleted Checklist: $checklistName"
        verify { mockChecklistCollectionViewPagerFragmentListener.displaySnackbar(expectedMessage) }
    }
}