package org.fossflight.efb.checklistCollection

import io.mockk.every
import io.mockk.mockk
import io.mockk.verify
import io.reactivex.Single
import io.reactivex.disposables.CompositeDisposable
import org.fossflight.efb.BundleKeys
import org.fossflight.efb.ForceTrampolineSchedulers
import org.fossflight.efb.data.model.ChecklistCollection
import org.fossflight.efb.data.persistence.FFRepository
import org.fossflight.efb.data.randomId
import org.fossflight.efb.data.randomTailNumber
import org.fossflight.efb.data.testChecklistCollection
import org.hamcrest.core.Is.`is`
import org.junit.Assert.assertThat
import org.junit.Before
import org.junit.Rule
import org.junit.Test

class ChecklistCollectionViewPagerPresenterTest {

    @get:Rule
    var forceTrampolineSchedulers = ForceTrampolineSchedulers()

    private val mockFFRepository = mockk<FFRepository>()
    private val mockChecklistCollectionViewPagerView = mockk<ChecklistCollectionViewPagerView>()

    private val aircraftId = randomId()
    private val tailNumber = randomTailNumber()
    private val compositeDisposable = CompositeDisposable()

    private val collections = listOf(
            testChecklistCollection(name = "Normal"),
            testChecklistCollection(name = "Emergency")
    )

    private val checklistCollectionViewPagerPresenter = ChecklistCollectionViewPagerPresenter(
            checklistCollectionViewPagerView = mockChecklistCollectionViewPagerView,
            ffRepository = mockFFRepository,
            compositeDisposable = compositeDisposable,
            aircraftId = aircraftId
    )

    @Before
    fun `set up`() {
        every { mockFFRepository.getChecklistCollections(aircraftId) } returns Single.just(collections)
        every { mockFFRepository.getAircraftTailNumber(aircraftId) } returns Single.just(tailNumber)

        every { mockChecklistCollectionViewPagerView.setPageAdapterTabs(any()) } returns Unit
        every { mockChecklistCollectionViewPagerView.updateToolbarTailNumber(any()) } returns Unit
    }

    @Test
    fun `should set the Pager Adapter with the Pager Tab Names`() {
        checklistCollectionViewPagerPresenter.presentData()
        verify(exactly = 1) { mockChecklistCollectionViewPagerView.setPageAdapterTabs(collections) }
    }

    @Test
    fun `should Update the Toolbar with the Tail Number`() {
        checklistCollectionViewPagerPresenter.presentData()
        verify(exactly = 1) { mockChecklistCollectionViewPagerView.updateToolbarTailNumber(tailNumber) }
    }

    @Test
    fun `should Dispose of the Composite Disposable`() {
        checklistCollectionViewPagerPresenter.dispose()
        assertThat(compositeDisposable.isDisposed, `is`(true))
    }

    @Test
    fun `should add Two Subscriptions when Presenting the Data`() {
        checklistCollectionViewPagerPresenter.presentData()
        assertThat(compositeDisposable.size(), `is`(2))
    }

    @Test
    fun `should Update the Pager Tabs with a Default Checklist Type when an Aircraft's Checklist Collection is Empty`() {
        val aircraftIdWithEmptyChecklistCollection = randomId()

        val emptyChecklistCollections = Single.just<List<ChecklistCollection>>(listOf())

        every { mockFFRepository.getChecklistCollections(aircraftIdWithEmptyChecklistCollection) } returns emptyChecklistCollections
        every { mockFFRepository.getAircraftTailNumber(aircraftIdWithEmptyChecklistCollection) } returns Single.just(tailNumber)

        val checklistCollectionViewPagerPresenter = ChecklistCollectionViewPagerPresenter(
                checklistCollectionViewPagerView = mockChecklistCollectionViewPagerView,
                ffRepository = mockFFRepository,
                aircraftId = aircraftIdWithEmptyChecklistCollection
        )

        checklistCollectionViewPagerPresenter.presentData()

        val expectedDefaultChecklistTypes = listOf(
                ChecklistCollection(
                        id = BundleKeys.UNDEFINED,
                        aircraftId = aircraftIdWithEmptyChecklistCollection,
                        name = BundleKeys.STANDARD_CHECKLIST_COLLECTION_NAME,
                        type = "NORMAL",
                        orderNumber = 0,
                        checklists = listOf()
                )
        )

        verify { mockChecklistCollectionViewPagerView.setPageAdapterTabs(expectedDefaultChecklistTypes) }
    }
}