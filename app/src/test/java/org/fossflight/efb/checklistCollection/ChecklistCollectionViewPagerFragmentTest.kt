package org.fossflight.efb.checklistCollection

import android.os.Bundle
import android.view.MenuItem
import android.view.WindowManager
import androidx.fragment.app.testing.launchFragmentInContainer
import androidx.test.core.app.ApplicationProvider
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.viewpager.widget.ViewPager
import io.mockk.every
import io.mockk.mockk
import io.mockk.verify
import io.reactivex.Flowable
import io.reactivex.Single
import org.fossflight.efb.BundleKeys
import org.fossflight.efb.ForceTrampolineSchedulers
import org.fossflight.efb.FossFlightTestApplication
import org.fossflight.efb.R
import org.fossflight.efb.TestAppModule
import org.fossflight.efb.data.persistence.FFRepository
import org.fossflight.efb.data.randomId
import org.fossflight.efb.data.randomTailNumber
import org.hamcrest.CoreMatchers.`is`
import org.hamcrest.MatcherAssert.assertThat
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.robolectric.Shadows.shadowOf
import org.robolectric.annotation.Config

@Config(application = FossFlightTestApplication::class)
@RunWith(AndroidJUnit4::class)
class ChecklistCollectionViewPagerFragmentTest {

    @get:Rule
    var forceTrampolineSchedulers = ForceTrampolineSchedulers()

    private val tailNumber = randomTailNumber()
    private val aircraftId = randomId()

    private val mockRepository = mockk<FFRepository>()
    private val mockChecklistCollectionViewPagerFragmentListener = mockk<ChecklistCollectionViewPagerFragmentListener>()

    val instantiate: () -> ChecklistCollectionViewPagerFragment = {
        val fragment = ChecklistCollectionViewPagerFragment()
        fragment.setParentActivity(mockChecklistCollectionViewPagerFragmentListener)
        fragment
    }
    val arguments = Bundle()

    @Before
    fun `set up`() {
        val application = ApplicationProvider.getApplicationContext<FossFlightTestApplication>()
        val testAppModule = TestAppModule(
                context = application.applicationContext,
                repository = mockRepository
        )

        application.setAppModule(testAppModule)

        arguments.putLong(BundleKeys.AIRCRAFT_ID, aircraftId)

        every { mockRepository.allAircraft } returns Flowable.just(listOf())
        every { mockRepository.getAircraftTailNumber(aircraftId) } returns Single.just(tailNumber)
        every { mockRepository.getChecklistCollections(aircraftId) } returns Single.just(mutableListOf())
        every { mockRepository.getChecklists(BundleKeys.UNDEFINED) } returns Single.just(listOf())

        every { mockChecklistCollectionViewPagerFragmentListener.updateToolbarTitle(any()) } returns Unit
        every { mockChecklistCollectionViewPagerFragmentListener.updateToolbarSubtitle(any()) } returns Unit
        every { mockChecklistCollectionViewPagerFragmentListener.finishFragment() } returns Unit
        every { mockChecklistCollectionViewPagerFragmentListener.startAddChecklistFragment(any()) } returns Unit
    }

    @Test
    fun `should set The Toolbar Text Appropriately`() {
        launchFragmentInContainer(
                instantiate = instantiate,
                fragmentArgs = arguments
        )

        verify { mockChecklistCollectionViewPagerFragmentListener.updateToolbarTitle("All Checklists") }
        verify { mockChecklistCollectionViewPagerFragmentListener.updateToolbarSubtitle(tailNumber) }
    }

    @Test
    fun `should finish the fragment after a Home Click`() {
        val scenario = launchFragmentInContainer(
                instantiate = instantiate,
                fragmentArgs = arguments
        )

        scenario.onFragment {
            it.onOptionsItemSelected(withMenuItem(android.R.id.home))
        }

        verify { mockChecklistCollectionViewPagerFragmentListener.finishFragment() }
    }

    @Test
    fun `should include Add Checklist on the Options Menu`() {
        val scenario = launchFragmentInContainer(
                instantiate = instantiate,
                fragmentArgs = arguments
        )

        scenario.onFragment {
            val activity = it.requireActivity()

            val menuItem = shadowOf(activity).optionsMenu.findItem(R.id.add_checklist)

            val expectedMenuItemText = activity.getString(R.string.add_checklist)

            assertThat(menuItem.title.toString(), `is`(expectedMenuItemText))
            assertThat(menuItem.isVisible, `is`(true))
        }
    }

    @Test
    fun `should Start the EditChecklistFragment when Adding a Checklist from the Options Menu`() {
        val scenario = launchFragmentInContainer(
                instantiate = instantiate,
                fragmentArgs = arguments
        )

        scenario.onFragment {
            it.onOptionsItemSelected(withMenuItem(R.id.add_checklist))
        }

        verify { mockChecklistCollectionViewPagerFragmentListener.startAddChecklistFragment(aircraftId) }
    }

    @Test
    fun `should set a Pager Adapter on the View Pager`() {
        val scenario = launchFragmentInContainer(
                instantiate = instantiate,
                fragmentArgs = arguments
        )

        scenario.onFragment {
            val adapter = it.requireActivity().findViewById<ViewPager>(R.id.pager_fragment).adapter
            assertThat(adapter!!.javaClass.name, `is`(ChecklistCollectionViewPagerViewAdapter::class.java.name))
        }
    }

    @Test
    fun `should set the Keep Screen on Flag`() {
        val scenario = launchFragmentInContainer(
                instantiate = instantiate,
                fragmentArgs = arguments
        )

        scenario.onFragment {
            val flagKeepScreenOn = WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON
            val shadowWindow = shadowOf(it.requireActivity().window)

            assertThat(shadowWindow.getFlag(flagKeepScreenOn), `is`(true))
        }
    }

    private fun withMenuItem(menuItemId: Int): MenuItem {
        val mockMenuItem = mockk<MenuItem>()
        every { mockMenuItem.itemId } returns menuItemId
        return mockMenuItem
    }
}