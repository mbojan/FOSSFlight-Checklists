package org.fossflight.efb.data.io

import android.app.Activity
import io.mockk.every
import io.mockk.mockk
import io.mockk.verify
import io.reactivex.Single
import io.reactivex.disposables.CompositeDisposable
import org.fossflight.efb.ForceTrampolineSchedulers
import org.fossflight.efb.PermissionsManager
import org.fossflight.efb.data.persistence.FFRepository
import org.fossflight.efb.data.randomId
import org.fossflight.efb.data.randomTailNumber
import org.fossflight.efb.data.testAircraft
import org.hamcrest.core.Is.`is`
import org.junit.Assert.assertThat
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import java.io.File
import java.io.IOException

class ExporterTest {

    @get:Rule
    var forceTrampolineSchedulers = ForceTrampolineSchedulers()

    private val compositeDisposable = CompositeDisposable()
    private val fossFlightDirectory = "FossFlight"
    private val aircraftId = randomId()
    private val tailNumber = randomTailNumber()
    private val aircraft = testAircraft(id = aircraftId, tailNumber = tailNumber)

    private val mockPermissionsManager = mockk<PermissionsManager>()
    private val mockActivity = mockk<Activity>()
    private val mockExportListener = mockk<ExporterListener>()
    private val mockFossFlightFileWriter = mockk<FossFlightFileWriter>()
    private val mockWriteableFile = mockk<File>()
    private val mockRepository = mockk<FFRepository>()

    private var exporter = Exporter(
            mockExportListener,
            mockPermissionsManager,
            mockRepository,
            compositeDisposable,
            mockFossFlightFileWriter
    )

    @Before
    fun `set up mocks`() {
        every { mockPermissionsManager.getWritableDirectory(any()) } returns mockWriteableFile
        every { mockPermissionsManager.requestWritePermission(any()) } returns Unit
        every { mockPermissionsManager.hasWritePermission(any()) } returns true
        every { mockPermissionsManager.canWriteToStorage() } returns true

        every { mockWriteableFile.absolutePath } returns fossFlightDirectory

        every { mockRepository.getAircraft(aircraftId) } returns Single.just(aircraft)

        every { mockWriteableFile.mkdirs() } returns true

        every { mockExportListener.displayWriteProblems() } returns Unit
    }

    @Test
    fun `should Request Write Permissions When Not Previously Obtained`() {
        every { mockPermissionsManager.hasWritePermission(mockActivity) } returns false

        exporter.startExport(mockActivity, randomId())

        verify { mockPermissionsManager.requestWritePermission(mockActivity) }
    }

    @Test
    fun `should display Write Problems Dialog when Unable to write to storage`() {
        every { mockPermissionsManager.hasWritePermission(mockActivity) } returns true
        every { mockPermissionsManager.canWriteToStorage() } returns false

        exporter.startExport(mockActivity, randomId())

        verify { mockExportListener.displayWriteProblems() }
    }

    @Test
    fun `should dispose of the CompositeDisposable after Continue Exporting`() {
        every { mockWriteableFile.exists() } returns true

        exporter.startExport(mockActivity, aircraftId)
        exporter.continueExport()

        assertThat(compositeDisposable.isDisposed, `is`(true))
    }

    @Test
    fun `should dispose of the CompositeDisposable when has wWrite Permission and Can Write to Storage`() {
        every { mockPermissionsManager.hasWritePermission(mockActivity) } returns true
        every { mockPermissionsManager.canWriteToStorage() } returns true

        every { mockWriteableFile.exists() } returns false

        exporter.startExport(mockActivity, aircraftId)

        assertThat(compositeDisposable.isDisposed, `is`(true))
    }

    @Test
    @Throws(IOException::class)
    fun `should Write the ExportFile after retrieving Aircraft Info`() {
        every { mockPermissionsManager.hasWritePermission(mockActivity) } returns false
        every { mockWriteableFile.exists() } returns true

        exporter.startExport(mockActivity, aircraftId)
        exporter.continueExport()

        val expectedExportFileName = fossFlightDirectory + "/" + tailNumber + "_ffc.csv"

        verify { mockFossFlightFileWriter.write(any(), eq(expectedExportFileName)) }
    }

    @Test
    fun `should Create the FossFlight Directory when Missing`() {
        every { mockPermissionsManager.hasWritePermission(mockActivity) } returns false
        every { mockWriteableFile.exists() } returns false

        exporter.startExport(mockActivity, aircraftId)
        exporter.continueExport()

        verify { mockWriteableFile.mkdirs() }
    }

    @Test
    @Throws(IOException::class)
    fun `should display the Problem Writing Alert Dialog when writing Throws an Exception`() {
        every { mockPermissionsManager.hasWritePermission(mockActivity) } returns true
        every { mockPermissionsManager.canWriteToStorage() } returns true

        every { mockWriteableFile.exists() } returns false

        every { mockFossFlightFileWriter.write(any(), any()) } throws RuntimeException()

        exporter.startExport(mockActivity, aircraftId)

        verify { mockExportListener.displayWriteProblems() }
    }
}