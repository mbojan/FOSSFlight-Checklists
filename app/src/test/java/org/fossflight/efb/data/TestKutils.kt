package org.fossflight.efb.data

import org.apache.commons.lang3.RandomStringUtils
import org.fossflight.efb.data.model.Aircraft
import org.fossflight.efb.data.model.Checklist
import org.fossflight.efb.data.model.ChecklistCollection
import org.fossflight.efb.data.model.ChecklistItem
import org.fossflight.efb.data.model.ChecklistItemState
import org.fossflight.efb.data.model.ChecklistItemType
import java.util.*
import java.util.concurrent.ThreadLocalRandom

fun randomId() = ThreadLocalRandom.current().nextLong(java.lang.Long.MAX_VALUE)

fun randomTailNumber(): String = RandomStringUtils.random(6)

fun randomName(): String = RandomStringUtils.random(255)
fun randomChecklistItemAction() = RandomStringUtils.random(50)

fun randomManufacturer(): String = RandomStringUtils.random(255)
fun randomModel(): String = RandomStringUtils.random(255)
fun randomYearInt(): Int = Random().nextInt((2050 - 1900) + 1) + 1900
fun randomYearString(): String = randomYearInt().toString()

fun randomOrderNumber(): Long = Random().nextLong()

fun randomChecklistItemState() = ChecklistItemState.values()[Random().nextInt(ChecklistItemState.values().size)]

fun testAircraft(
        id: Long = randomId(),
        tailNumber: String  = randomTailNumber(),
        manufacturer: String  = randomName(),
        model: String  = randomName(),
        year: Int  = randomYearInt(),
        checklistCollections: List<ChecklistCollection> = listOf()
) = Aircraft(
        id = id,
        tailNumber = tailNumber,
        manufacturer = manufacturer, model = model, year = year,
        checklistCollections = checklistCollections
)

fun testChecklistCollection(
        id: Long = randomId(),
        aircraftId: Long = randomId(),
        name: String = randomName(),
        orderNumber: Long = randomId(),
        checklists: List<Checklist> = listOf()
) = ChecklistCollection(
        id = id,
        aircraftId = aircraftId,
        name = name,
        orderNumber = orderNumber,
        checklists = checklists
)

fun testChecklist(
        id: Long = randomId(),
        checklistCollectionId: Long = randomId(),
        name: String = randomName(),
        orderNumber: Long = randomId(),
        items: List<ChecklistItem> = listOf()
) = Checklist(
        id = id,
        checklistCollectionId = checklistCollectionId,
        name = name,
        orderNumber = orderNumber,
        items = items
)

fun testChecklistItem(
        id: Long = randomId(),
        checkListId: Long = randomId(),
        name: String = randomName(),
        action: String = randomName(),
        type: String = ChecklistItemType.NORMAL.toString(),
        orderNumber: Long = randomId(),
        state: ChecklistItemState = randomChecklistItemState()
) = ChecklistItem(
        id = id,
        checkListId = checkListId,
        name = name,
        action = action,
        type = type,
        orderNumber = orderNumber,
        state = state
)