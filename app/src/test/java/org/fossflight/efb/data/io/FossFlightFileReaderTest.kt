package org.fossflight.efb.data.io

import org.hamcrest.core.Is.`is`
import org.junit.Assert.assertNotNull
import org.junit.Assert.assertThat
import org.junit.Assert.fail
import org.junit.Rule
import org.junit.Test
import org.junit.rules.ExpectedException
import java.io.File
import java.io.FileInputStream

class FossFlightFileReaderTest {

    @get:Rule
    var expectedException = ExpectedException.none()

    private var fossFlightFileReader = FossFlightFileReader()

    private fun assertNumberOfLinesReadFromFile(value: Int, file: File) {

        try {
            FileInputStream(file).use { inputStream ->
                val lines = fossFlightFileReader.readFile(inputStream)
                assertNotNull(lines)
                assertThat(lines.size, `is`(value))
            }
        } catch (e: Exception) {
            fail("There was an exception while trying to read the test file $e")
        }
    }

    @Test
    fun `should read A file`() {
        val testFile = getFileFromPath(this, "testFile.csv")
        assertNumberOfLinesReadFromFile(3, testFile)
    }

    @Test
    fun `should ignore blank lines when reading a file`() {
        val testFile = getFileFromPath(this, "testFileWithBlankLines.csv")
        assertNumberOfLinesReadFromFile(4, testFile)
    }

    private fun getFileFromPath(obj: Any, fileName: String): File {
        val classLoader = obj.javaClass.classLoader!!
        val resource = classLoader.getResource(fileName)
        return File(resource.path)
    }
}
