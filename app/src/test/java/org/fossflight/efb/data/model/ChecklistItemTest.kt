package org.fossflight.efb.data.model

import org.fossflight.efb.data.randomChecklistItemAction
import org.fossflight.efb.data.randomId
import org.fossflight.efb.data.randomName
import org.hamcrest.core.Is.`is`
import org.junit.Assert.assertEquals
import org.junit.Assert.assertThat
import org.junit.Test

class ChecklistItemTest {

    private val itemId = randomId()
    private val checklistId = randomId()
    private val checklistItemName = randomName()
    private val checklistItemAction = randomChecklistItemAction()
    private val checklistItemType = ChecklistItemType.NORMAL

    val itemOne = ChecklistItem(
            id = itemId,
            checkListId = checklistId,
            name = checklistItemName,
            action = checklistItemAction,
            type = checklistItemType.toString()
    )

    @Test
    fun `should be Equal`() {
        val itemTwo = ChecklistItem(
                id = itemId,
                checkListId = checklistId,
                name = checklistItemName,
                action = checklistItemAction,
                type = checklistItemType.toString()
        )
        assertThat(itemOne == itemTwo, `is`(true))
        assertThat(itemTwo == itemOne, `is`(true))
    }

    @Test
    fun `two Lists of Equal Items are Equal`() {
        val itemTwo = ChecklistItem(
                id = itemId,
                checkListId = checklistId,
                name = checklistItemName,
                action = checklistItemAction,
                type = checklistItemType.toString()
        )

        val listOne = listOf(itemOne)
        val listTwo = listOf(itemTwo)
        assertThat(listOne, `is`(listTwo))
        assertEquals(listOne, listTwo)
        assertThat(listOne == listTwo, `is`(true))
        assertThat(listTwo == listOne, `is`(true))
    }

    @Test
    fun `items with Different Ids should be Different`() {
        val differentId = randomId()
        val itemTwo = itemOne.copy(id = differentId)

        assertItemsAreDifferent(itemOne, itemTwo)
    }

    @Test
    fun `items with Different Checklist Ids should be Different`() {
        val differentChecklistId = randomId()
        val itemTwo = itemOne.copy(checkListId = differentChecklistId)

        assertItemsAreDifferent(itemOne, itemTwo)
    }

    @Test
    fun `items with Different Names should be Different`() {
        val differentChecklistItemName = randomName()
        val itemTwo = itemOne.copy(name = differentChecklistItemName)

        assertItemsAreDifferent(itemOne, itemTwo)
    }

    @Test
    fun `items with Different Actions should be Different`() {
        val differentChecklistItemAction = randomChecklistItemAction()
        val itemTwo = itemOne.copy(action = differentChecklistItemAction)

        assertItemsAreDifferent(itemOne, itemTwo)
    }

    @Test
    fun `items with Different Item Types should be Different`() {
        val differentChecklistItemType = ChecklistItemType.SECTION_HEADER
        val itemTwo = itemOne.copy(type = differentChecklistItemType.toString())

        assertItemsAreDifferent(itemOne, itemTwo)
    }

    private fun assertItemsAreDifferent(itemOne: ChecklistItem, itemTwo: ChecklistItem) {
        assertThat(itemOne == itemTwo, `is`(false))
        assertThat(itemTwo == itemOne, `is`(false))
    }
}