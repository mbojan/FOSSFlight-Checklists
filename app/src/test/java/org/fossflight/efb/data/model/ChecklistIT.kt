package org.fossflight.efb.data.model

import androidx.test.ext.junit.runners.AndroidJUnit4
import org.fossflight.efb.data.randomId
import org.fossflight.efb.data.randomName
import org.fossflight.efb.data.randomOrderNumber
import org.fossflight.efb.data.testChecklistItem
import org.fossflight.efb.data.testParcel
import org.hamcrest.CoreMatchers.`is`
import org.hamcrest.CoreMatchers.equalTo
import org.hamcrest.CoreMatchers.not
import org.hamcrest.CoreMatchers.sameInstance
import org.hamcrest.MatcherAssert.assertThat
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class ChecklistIT {

    @Test
    fun `should parcelerize a checklist`() {
        val originalChecklist = Checklist(
                id = randomId(),
                checklistCollectionId = randomId(),
                name = randomName(),
                orderNumber = randomOrderNumber(),
                items = listOf(testChecklistItem(), testChecklistItem(), testChecklistItem())
        )

        originalChecklist.testParcel()
                .apply {
                    assertThat(this, `is`(equalTo(originalChecklist)))
                    assertThat(this, `is`(not(sameInstance(originalChecklist))))
                }
    }
}
