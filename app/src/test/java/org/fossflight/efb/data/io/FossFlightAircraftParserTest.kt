package org.fossflight.efb.data.io

import org.fossflight.efb.data.model.Aircraft
import org.fossflight.efb.data.model.ChecklistItemType
import org.fossflight.efb.data.randomChecklistItemAction
import org.fossflight.efb.data.randomName
import org.fossflight.efb.data.randomTailNumber
import org.fossflight.efb.data.randomYearString
import org.fossflight.efb.data.testAircraft
import org.fossflight.efb.data.testChecklist
import org.fossflight.efb.data.testChecklistCollection
import org.fossflight.efb.data.testChecklistItem
import org.hamcrest.core.Is.`is`
import org.junit.Assert.assertThat
import org.junit.Test

class FossFlightAircraftParserTest {

    private val itemOneName = randomName()
    private val itemOneAction = randomChecklistItemAction()
    private val checklistOneName = randomName()
    private val checklistTwoName = randomName()
    private val itemTwoName = randomName()
    private val itemTwoAction = randomChecklistItemAction()
    private val itemThreeName = randomName()
    private val itemThreeAction = randomChecklistItemAction()
    private val checklistCollectionOneName = randomName()
    private val checklistCollectionTwoName = randomName()

    @Test
    fun `should include the Aircraft Information on separate lines with identifiers at the beginning of the parsed line list`() {
        val tailNumber = randomTailNumber()
        val manufacturer = randomName()
        val model = randomName()
        val year = randomYearString()
        val aircraft = Aircraft(
                tailNumber = tailNumber,
                manufacturer = manufacturer,
                model = model,
                year = year.toInt()
        )

        val parsedLines = FossFlightAircraftParser().parseAircraft(aircraft)

        val expectedTailNumberLine = "$tailNumber,TailNumber\n"
        val expectedManufacturer = "$manufacturer,AircraftManufacturer\n"
        val expectedModel = "$model,AircraftModel\n"
        val expectedYear = "$year,AircraftYear\n"
        assertThat(parsedLines[0], `is`(expectedTailNumberLine))
        assertThat(parsedLines[1], `is`(expectedManufacturer))
        assertThat(parsedLines[2], `is`(expectedModel))
        assertThat(parsedLines[3], `is`(expectedYear))
        assertThat(parsedLines[4], `is`("\n"))
    }

    @Test
    fun `should exclude Aircraft Information when that information is blank`() {
        val tailNumber = randomTailNumber()
        val manufacturer = ""
        val model = ""
        val aircraft = Aircraft(
                tailNumber = tailNumber,
                manufacturer = manufacturer,
                model = model,
                year = 0
        )

        val parsedLines = FossFlightAircraftParser().parseAircraft(aircraft)

        assertThat(parsedLines.size, `is`(2))
        val expectedTailNumberLine = "$tailNumber,TailNumber\n"
        assertThat(parsedLines[0], `is`(expectedTailNumberLine))
        assertThat(parsedLines[1], `is`("\n"))
    }

    @Test
    fun `should Include The Checklist Name And Type On One Line Separated By A Comma Followed By A Blank Line`() {
        val checklistOne = testChecklist(name = checklistOneName)

        val checklistTwo = testChecklist(name = checklistTwoName)

        val checklistCollectionOne = testChecklistCollection(
                name = checklistCollectionOneName,
                checklists = listOf(checklistOne)
        )

        val checklistCollectionTwo = testChecklistCollection(
                name = checklistCollectionTwoName,
                checklists = listOf(checklistTwo)
        )

        val aircraft = testAircraft(
                checklistCollections = listOf(checklistCollectionOne, checklistCollectionTwo)
        )

        val parsedLines = FossFlightAircraftParser().parseAircraft(aircraft)

        val lineOne = parsedLines[5]
        val expectedParsedLine = "$checklistOneName,$checklistCollectionOneName\n"
        assertThat(lineOne, `is`(expectedParsedLine))

        val lineTwo = parsedLines[7]
        val expectedParsedLineTwo = "$checklistTwoName,$checklistCollectionTwoName\n"
        assertThat(lineTwo, `is`(expectedParsedLineTwo))
    }

    @Test
    fun `should Include A Checklists Items Under The Checklist Name And Type Line Each On Their Own Line`() {
        val checklistOne = testChecklist(
                name = checklistOneName,
                items = listOf(checklistItemOne(), checklistItemTwo(), checklistItemThree())
        )

        val checklistCollection = testChecklistCollection(checklists = listOf(checklistOne))

        val aircraft = testAircraft(checklistCollections = listOf(checklistCollection))

        val parsedLines = FossFlightAircraftParser().parseAircraft(aircraft)

        val lineOne = parsedLines[6]
        val expectedParsedLine = "$itemOneName,$itemOneAction\n"
        assertThat(lineOne, `is`(expectedParsedLine))

        val lineTwo = parsedLines[7]
        val expectedParsedLineTwo = "$itemTwoName,$itemTwoAction\n"
        assertThat(lineTwo, `is`(expectedParsedLineTwo))

        val lineThree = parsedLines[8]
        val expectedParsedLineThree = "$itemThreeName,$itemThreeAction\n"
        assertThat(lineThree, `is`(expectedParsedLineThree))
    }

    @Test
    fun `should Safely Parse A Checklist With Zero Checklist Items`() {
        val checklistOne = testChecklist(name = checklistOneName, items = listOf())

        val checklistCollection = testChecklistCollection(checklists = listOf(checklistOne))

        val aircraft = testAircraft(checklistCollections = listOf(checklistCollection))

        val parsedLines = FossFlightAircraftParser().parseAircraft(aircraft)

        assertThat(parsedLines.size, `is`(7))
    }

    @Test
    fun `should Safely Parse A Checklist Collection With Zero Checklists`() {
        val checklistCollection = testChecklistCollection(checklists = listOf())

        val aircraft = testAircraft(checklistCollections = listOf(checklistCollection))

        val parsedLines = FossFlightAircraftParser().parseAircraft(aircraft)

        assertThat(parsedLines.size, `is`(5))
    }

    @Test
    fun `should Indicate A Section Header Checklist Item With Dashes`() {

        val checklistItemTwo = testChecklistItem(
                name = checklistTwoName,
                type = ChecklistItemType.SECTION_HEADER.toString()
        )

        val checklistOne = testChecklist(
                name = checklistOneName,
                items = listOf(checklistItemOne(), checklistItemTwo, checklistItemThree())
        )

        val collectionOne = testChecklistCollection(
                name = "checklist collection name",
                checklists = listOf(checklistOne)
        )

        val aircraft = testAircraft(
                tailNumber = "tail number",
                checklistCollections = listOf(collectionOne)
        )

        val parsedLines = FossFlightAircraftParser().parseAircraft(aircraft)

        val line = parsedLines[7]
        val lineTokens = line.split(",".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()

        assertThat(lineTokens[1], `is`("-----\n"))
    }

    @Test
    fun `should quote checklist items that contain a comma`() {
        val checklistName = "a checklist, name"
        val checklistAction = "this, is a sample item"
        val checklistItem = testChecklistItem(
                name = checklistName,
                action = checklistAction
        )
        val checklistOne = testChecklist(
                name = checklistOneName,
                items = listOf(checklistItemOne(), checklistItem, checklistItemThree())
        )

        val checklistCollection = testChecklistCollection(checklists = listOf(checklistOne))

        val aircraft = testAircraft(checklistCollections = listOf(checklistCollection))

        val parsedLines = FossFlightAircraftParser().parseAircraft(aircraft)

        assertThat(parsedLines[7], `is`("\"$checklistName\",\"$checklistAction\"\n"))
    }

    private fun checklistItemThree() =
            testChecklistItem(
                    name = itemThreeName,
                    action = itemThreeAction,
                    type = ChecklistItemType.NORMAL.toString()
            )

    private fun checklistItemTwo() =
            testChecklistItem(
                    name = itemTwoName,
                    action = itemTwoAction,
                    type = ChecklistItemType.NORMAL.toString()
            )

    private fun checklistItemOne() =
            testChecklistItem(
                    name = itemOneName,
                    action = itemOneAction,
                    type = ChecklistItemType.NORMAL.toString()
            )
}