package org.fossflight.efb.data.model

import androidx.test.espresso.matcher.ViewMatchers.assertThat
import androidx.test.ext.junit.runners.AndroidJUnit4
import org.fossflight.efb.data.randomChecklistItemAction
import org.fossflight.efb.data.randomChecklistItemState
import org.fossflight.efb.data.randomId
import org.fossflight.efb.data.randomName
import org.fossflight.efb.data.randomOrderNumber
import org.fossflight.efb.data.testParcel
import org.hamcrest.CoreMatchers.`is`
import org.hamcrest.CoreMatchers.equalTo
import org.hamcrest.CoreMatchers.not
import org.hamcrest.CoreMatchers.sameInstance
import org.junit.Test
import org.junit.runner.RunWith


@RunWith(AndroidJUnit4::class)
class ChecklistItemIT {

    @Test
    fun `should parcelerize a checklist item`() {
        val originalChecklistItem = ChecklistItem(
                id = randomId(),
                checkListId = randomId(),
                name = randomName(),
                action = randomChecklistItemAction(),
                type = ChecklistItemType.NORMAL.toString(),
                orderNumber = randomOrderNumber(),
                state = randomChecklistItemState()
        )

        originalChecklistItem.testParcel()
                .apply {
                    assertThat(this, `is`(equalTo(originalChecklistItem)))
                    assertThat(this, `is`(not(sameInstance(originalChecklistItem))))
                }
    }
}