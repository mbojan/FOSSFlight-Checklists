package org.fossflight.efb.data.io

import org.fossflight.efb.data.model.ChecklistItemType
import org.hamcrest.core.Is.`is`
import org.junit.Assert.assertThat
import org.junit.Test

class FossFlightCSVParserTest {

    private val parser = FossFlightCSVParser()
    private val fileContents = listOf(
                "NEXFC1,TailNumber",
                "Cessna,AircraftManufacturer",
                "172N,AircraftModel",
                "1976,AircraftYear",
                "Some Gibberish Text Checklist,",
                ",",
                "PREFLIGHT,Standard Checklist",
                "Interior Preflight,-----",
                "Maintenance Status,Check",
                "Weather,Check",
                "AROW Documents,Check",
                "Exterior Checklist,-----,,",
                "Left Wing Fuel Sump,OK",
                "Left Wheel / Tire / Brakes,OK",
                "Left Flaps,OK,",
                "BEFORE STARTING ENGINE,Standard Checklist,,,,,,",
                "Seatbelts,On",
                "Doors,Closed and Latched",
                "Fuel Selector,Both",
                "Mixture,Full Rich",
                "Am I missing anything?,\"well, am I?\"",
                "\"first, second\", this is the third",
                "Run Up,Standard Checklist",
                "Position into wind,OK",
                "Trim for takeoff,Set",
                "Flight Controls,Free and Correct",
                ",",
                "ICING,Emergency Checklist",
                "Pitot Heat,On",
                "Carb Heat,On"
        )

    @Test
    fun `should parse the Aircraft details`() {
        val aircraft = parser.parse(fileContents)
        assertThat(aircraft.tailNumber, `is`("NEXFC1"))
        assertThat(aircraft.manufacturer, `is`("Cessna"))
        assertThat(aircraft.model, `is`("172N"))
        assertThat(aircraft.year, `is`(1976))
    }

    @Test
    fun `should parse Aircraft details to empty string when missing`() {
        val checklistLines = listOf(
                "PREFLIGHT,Standard Checklist",
                "BEFORE STARTING ENGINE,Standard Checklist",
                "ICING,Emergency Checklist"
        )

        val aircraft = parser.parse(checklistLines)
        assertThat(aircraft.tailNumber, `is`(""))
        assertThat(aircraft.manufacturer, `is`(""))
        assertThat(aircraft.model, `is`(""))
        assertThat(aircraft.year, `is`(0))
    }

    @Test
    fun `should Identify Checklist Collection Types`() {
        val collections = parser.parse(fileContents).checklistCollections

        assertThat(collections[0].name, `is`("Standard Checklist"))
        assertThat(collections[1].name, `is`("Emergency Checklist"))
        assertThat(collections.size, `is`(2))
    }

    @Test
    fun `should Correctly Identify Checklists`() {
        val collections = parser.parse(fileContents).checklistCollections

        val (_, _, _, _, _, standardChecklists) = collections[0]
        assertThat(standardChecklists.size, `is`(3))
    }

    @Test
    fun `should Identify A Checklist Section Header When Second Token Is At Least Five Dashes`() {
        val collections = parser.parse(fileContents).checklistCollections
        val (_, _, _, _, items) = collections[0].checklists[0]
        val (_, _, name, _, type) = items[0]
        assertThat(name, `is`("Interior Preflight"))
        assertThat(type, `is`(ChecklistItemType.SECTION_HEADER.toString()))
    }

    @Test
    fun `should Preserve The Order Of Checklists As Read`() {
        val collections = parser.parse(fileContents).checklistCollections
        val checklists = collections[0].checklists

        val (_, _, name, orderNumber) = checklists[0]
        assertThat(name, `is`("PREFLIGHT"))
        assertThat(orderNumber, `is`(0L))

        val (_, _, name1, orderNumber1) = checklists[1]
        assertThat(name1, `is`("BEFORE STARTING ENGINE"))
        assertThat(orderNumber1, `is`(1L))
    }

    @Test
    fun `should ignore lines that contain only a comma`() {
        val checklistCollections = parser.parse(fileContents).checklistCollections
        val checklists = checklistCollections[0].checklists

        assertThat(checklists[0].items.size, `is`(8))
        assertThat(checklists[1].items.size, `is`(6))
        assertThat(checklists[2].items.size, `is`(3))
    }

    @Test
    fun `should Identify Checklist Items When Line Has Two Tokens`() {
        val collections = parser.parse(fileContents).checklistCollections
        val checklists = collections[0].checklists

        val (_, _, _, _, firstChecklistItems) = checklists[0]
        assertThat(firstChecklistItems[0].name, `is`("Interior Preflight"))
        assertThat(firstChecklistItems[0].type, `is`(ChecklistItemType.SECTION_HEADER.toString()))

        assertThat(firstChecklistItems[1].name, `is`("Maintenance Status"))
        assertThat(firstChecklistItems[1].action, `is`("Check"))

        assertThat(firstChecklistItems[2].name, `is`("Weather"))
        assertThat(firstChecklistItems[2].action, `is`("Check"))

        assertThat(firstChecklistItems[3].name, `is`("AROW Documents"))
        assertThat(firstChecklistItems[3].action, `is`("Check"))

        val (_, _, _, _, secondChecklistItems) = checklists[1]
        assertThat(secondChecklistItems[0].name, `is`("Seatbelts"))
        assertThat(secondChecklistItems[0].action, `is`("On"))

        assertThat(secondChecklistItems[1].name, `is`("Doors"))
        assertThat(secondChecklistItems[1].action, `is`("Closed and Latched"))

        assertThat(secondChecklistItems[2].name, `is`("Fuel Selector"))
        assertThat(secondChecklistItems[2].action, `is`("Both"))

        assertThat(secondChecklistItems[3].name, `is`("Mixture"))
        assertThat(secondChecklistItems[3].action, `is`("Full Rich"))

        assertThat(secondChecklistItems[4].name, `is`("Am I missing anything?"))
        assertThat(secondChecklistItems[4].action, `is`("well, am I?"))

        assertThat(secondChecklistItems[5].name, `is`("first, second"))
        assertThat(secondChecklistItems[5].action, `is`("this is the third"))
    }

    @Test
    fun `should Parse An Aircraft With Checklists That Have Only Names`() {

        val emptyChecklistsFile = listOf(
                "PREFLIGHT,Standard Checklist",
                "BEFORE STARTING ENGINE,Standard Checklist",
                "ICING,Emergency Checklist"
        )

        val collections = parser.parse(emptyChecklistsFile).checklistCollections

        assertThat(collections.size, `is`(2))

        val (_, _, name, _, _, standardChecklists) = collections[0]
        assertThat(name, `is`("Standard Checklist"))

        assertThat(standardChecklists[0].name, `is`("PREFLIGHT"))
        assertThat(standardChecklists[1].name, `is`("BEFORE STARTING ENGINE"))

        val (_, _, name1, _, _, emergencyChecklists) = collections[1]
        assertThat(name1, `is`("Emergency Checklist"))

        assertThat(emergencyChecklists[0].name, `is`("ICING"))
    }

    @Test
    fun `should properly parse lines ending with extra commas`() {
        val fileContentsWithExtraCommas = listOf(
                "NEXFC1,TailNumber,,,,,,,,,,,,,,,",
                "Cessna,AircraftManufacturer,,,,,,",
                "172N,AircraftModel,,,,",
                "1976,AircraftYear,,,,,,,,,,,,",
                "Some Gibberish Text Checklist,,,,,,,,,,"
        )

        val aircraft = parser.parse(fileContentsWithExtraCommas)
        assertThat(aircraft.tailNumber, `is`("NEXFC1"))
        assertThat(aircraft.manufacturer, `is`("Cessna"))
        assertThat(aircraft.model, `is`("172N"))
        assertThat(aircraft.year, `is`(1976))


    }
}
