package org.fossflight.efb.data.persistence

import io.mockk.every
import io.mockk.mockk
import io.mockk.verify
import io.reactivex.Flowable
import io.reactivex.Single
import io.reactivex.schedulers.Schedulers
import org.fossflight.efb.BundleKeys
import org.fossflight.efb.data.model.Aircraft
import org.fossflight.efb.data.randomChecklistItemAction
import org.fossflight.efb.data.randomId
import org.fossflight.efb.data.randomManufacturer
import org.fossflight.efb.data.randomModel
import org.fossflight.efb.data.randomName
import org.fossflight.efb.data.randomTailNumber
import org.fossflight.efb.data.randomYearInt
import org.fossflight.efb.data.testAircraft
import org.fossflight.efb.data.testChecklist
import org.fossflight.efb.data.testChecklistCollection
import org.fossflight.efb.data.testChecklistItem
import org.hamcrest.CoreMatchers.`is`
import org.junit.Assert.assertThat
import org.junit.Before
import org.junit.Test

class FFRepositoryTest {

    private var mockDatabase = mockk<FFDatabase>()
    private val aircraftDao = mockk<AircraftDao>()
    private val checklistCollectionDao = mockk<ChecklistCollectionDao>()
    private val checklistDao = mockk<ChecklistDao>()
    private val checklistItemDao = mockk<ChecklistItemDao>()

    @Before
    fun `set up mocks and activity`() {
        every { mockDatabase.aircraftDao } returns aircraftDao
        every { mockDatabase.checklistCollectionDao } returns checklistCollectionDao
        every { mockDatabase.checklistDao } returns checklistDao
        every { mockDatabase.checklistItemDao } returns checklistItemDao

        every { checklistDao.deleteChecklist(any()) } returns Unit
        every { aircraftDao.deleteAircraft(any()) } returns Unit
    }

    @Test
    fun `should Return Observable with Aircraft`() {
        val checklistItemName = randomName()
        val checklistItemAction = randomChecklistItemAction()
        val checklistName = randomName()
        val checklistCollectionName = randomName()
        val tailNumber = randomTailNumber()

        val checklistItem = testChecklistItem(
                id = BundleKeys.UNDEFINED,
                checkListId = BundleKeys.UNDEFINED,
                name = checklistItemName,
                action = checklistItemAction
        )
        val checklist = testChecklist(
                id = BundleKeys.UNDEFINED,
                checklistCollectionId = BundleKeys.UNDEFINED,
                name = checklistName,
                items = listOf(checklistItem)
        )
        val collection = testChecklistCollection(
                id = BundleKeys.UNDEFINED,
                aircraftId = BundleKeys.UNDEFINED,
                name = checklistCollectionName,
                checklists = listOf(checklist)
        )
        val collectionList = mutableListOf(collection)

        val aircraft = testAircraft(
                id = BundleKeys.UNDEFINED,
                tailNumber = tailNumber,
                checklistCollections = collectionList
        )

        val assignedChecklistItemId = randomId()
        val assignedChecklistId = randomId()
        val assignedChecklistCollectionId = randomId()
        val assignedAircraftId = randomId()

        val collectionIds = listOf(assignedChecklistCollectionId)

        every { mockDatabase.aircraftDao.insertAircraft(aircraft) } returns assignedAircraftId

        every { mockDatabase.checklistCollectionDao.insertChecklistCollections(collectionList) } returns collectionIds

        every { mockDatabase.checklistDao.insertChecklist(checklist) } returns assignedChecklistId

        every { mockDatabase.checklistItemDao.insertChecklistItem(checklistItem) } returns assignedChecklistItemId

        val repository = FFRepository(mockDatabase)

        val aircrafts = mutableListOf<Aircraft>()
        repository.insertAircraft(aircraft)
                .subscribeOn(Schedulers.trampoline())
                .observeOn(Schedulers.trampoline())
                .subscribe { aircrafts.add(it) }

        // Ensure Aircraft is saved
        assertThat(aircrafts.size, `is`(1))
        val (id, tailNumber1, _, _, _, actualCollections) = aircrafts[0]
        assertThat(id, `is`(assignedAircraftId))
        assertThat(tailNumber1, `is`(tailNumber))

        // Ensure Checklist Collection is saved
        assertThat(actualCollections.size, `is`(1))
        val (id1, aircraftId, name, _, _, checklists) = actualCollections[0]
        assertThat(id1, `is`(assignedChecklistCollectionId))
        assertThat(aircraftId, `is`(assignedAircraftId))
        assertThat(name, `is`(checklistCollectionName))
        // order number?

        // Ensure checklists are saved
        assertThat(checklists[0].items.size, `is`(1))
        val (id2, checklistCollectionId, name1, _, items) = checklists[0]
        assertThat(id2, `is`(assignedChecklistId))
        assertThat(checklistCollectionId, `is`(assignedChecklistCollectionId))
        assertThat(name1, `is`(checklistName))
        // order number?

        // Ensure checklistItems are saved
        assertThat(items.size, `is`(1))
        val (id3, checkListId, name2, action) = items[0]
        assertThat(id3, `is`(assignedChecklistItemId))
        assertThat(checkListId, `is`(assignedChecklistId))
        assertThat(name2, `is`(checklistItemName))
        assertThat(action, `is`(checklistItemAction))
        // order number?
    }

    @Test
    fun `should Get a Flowable List of Aircraft`() {
        val aircraftId = randomId()
        val tailNumber = randomTailNumber()
        val checklistCollectionId = randomId()
        val checklistCollectionName = randomName()

        val collection = testChecklistCollection(
                id = checklistCollectionId,
                aircraftId = aircraftId,
                name = checklistCollectionName,
                checklists = listOf()
        )

        val collectionList = mutableListOf(collection)

        val aircraft = testAircraft(
                id = aircraftId,
                tailNumber = tailNumber
        )

        val aircraftList = listOf(aircraft)

        every { aircraftDao.aircraft } returns Single.just(aircraftList)

        every { checklistCollectionDao.getChecklistCollections(aircraftId) } returns Single.just(collectionList)

        val repository = FFRepository(mockDatabase)

        val returnedAircraftList = mutableListOf<Aircraft>()
        repository.allAircraft
                .subscribeOn(Schedulers.trampoline())
                .observeOn(Schedulers.trampoline())
                .subscribe { returnedAircraftList.addAll(it) }


        assertThat(returnedAircraftList.size, `is`(1))

        val actualAircraft = returnedAircraftList.first()
        assertThat(actualAircraft.id, `is`(aircraftId))
        assertThat(actualAircraft.tailNumber, `is`(tailNumber))
        assertThat(actualAircraft.checklistCollections.size, `is`(1))

        val actualChecklistCollection = actualAircraft.checklistCollections.first()
        assertThat(actualChecklistCollection.id, `is`(checklistCollectionId))
        assertThat(actualChecklistCollection.aircraftId, `is`(aircraftId))
        assertThat(actualChecklistCollection.name, `is`(checklistCollectionName))
    }

    @Test
    fun `should Get Aircraft Tail Number when Given a ChecklistId`() {

        val checklistId = randomId()
        val checklistCollectionId = randomId()
        val aircraftId = randomId()
        val tailNumber = randomTailNumber()

        every { checklistDao.getChecklistCollectionId(checklistId) } returns Single.just(checklistCollectionId)
        every { checklistCollectionDao.getAircraftIdForChecklistCollection(checklistCollectionId) } returns Single.just(aircraftId)
        every { aircraftDao.getTailNumber(aircraftId) } returns Single.just(tailNumber)

        val repository = FFRepository(mockDatabase)

        val actualTailNumber = repository
                .getAircraftTailNumberForChecklist(checklistId)
                .blockingGet()

        assertThat(actualTailNumber, `is`(tailNumber))
    }

    @Test
    fun `should Get a Checklist when Given a ChecklistId`() {
        val checklistId = randomId()

        val itemOne = testChecklistItem()
        val itemTwo = testChecklistItem()

        val items = listOf(itemOne, itemTwo)

        val checklist = testChecklist(id = checklistId)

        every { checklistItemDao.getItemsForChecklist(checklistId) } returns Single.just(items)
        every { checklistDao.getChecklist(checklistId) } returns Flowable.just(checklist)

        val repository = FFRepository(mockDatabase)

        val (id, _, _, _, items1) = repository.getChecklist(checklistId).blockingGet()

        assertThat(id, `is`(checklistId))
        assertThat(items1.size, `is`(2))
        assertThat(items1[0], `is`(itemOne))
        assertThat(items1[1], `is`(itemTwo))
    }

    @Test
    fun `should save the aircraft collections when saving an aircraft`() {
        val assignedAircraftId = randomId()
        val tailNumber = randomTailNumber()
        val manufacturer = randomManufacturer()
        val model = randomModel()
        val year = randomYearInt()

        val collectionOne = testChecklistCollection(aircraftId = 0)
        val collectionTwo = testChecklistCollection(aircraftId = 0)
        val defaultCollections = listOf(collectionOne, collectionTwo)

        val aircraft = testAircraft(
                id = BundleKeys.UNDEFINED,
                tailNumber = tailNumber,
                manufacturer = manufacturer,
                model = model,
                year = year,
                checklistCollections = defaultCollections
        )

        every { mockDatabase.aircraftDao.insertAircraft(aircraft) } returns assignedAircraftId
        every { mockDatabase.checklistCollectionDao.insertChecklistCollections(defaultCollections) } returns listOf(randomId(), randomId())

        val repository = FFRepository(mockDatabase)

        val aircrafts = mutableListOf<Aircraft>()
        repository.saveAircraft(aircraft)
                .subscribeOn(Schedulers.trampoline())
                .observeOn(Schedulers.trampoline())
                .subscribe { aircrafts.add(it) }

        // Ensure Aircraft is saved
        assertThat(aircrafts.size, `is`(1))
        val actualAircraft = aircrafts[0]
        assertThat(actualAircraft.id, `is`(assignedAircraftId))
        assertThat(actualAircraft.tailNumber, `is`(tailNumber))
        assertThat(actualAircraft.manufacturer, `is`(manufacturer))
        assertThat(actualAircraft.model, `is`(model))
        assertThat(actualAircraft.year, `is`(year))

        val actualChecklistCollections = actualAircraft.checklistCollections
        assertThat(actualChecklistCollections[0].aircraftId, `is`(assignedAircraftId))
        assertThat(actualChecklistCollections[1].aircraftId, `is`(assignedAircraftId))

    }

    @Test
    fun `should Delete an Aircraft`() {
        val aircraftId = randomId()

        val collectionOne = testChecklistCollection(aircraftId = aircraftId)
        val collectionTwo = testChecklistCollection(aircraftId = aircraftId)
        val aircraft = testAircraft(
                id = aircraftId,
                checklistCollections = listOf(collectionOne, collectionTwo)
        )

        val repository = FFRepository(mockDatabase)

        repository.deleteAircraft(aircraft).blockingAwait()

        verify(exactly = 1) { aircraftDao.deleteAircraft(aircraft) }
    }

    @Test
    fun `should Get an Aircraft`() {
        val aircraftId = randomId()
        val tailNumber = randomTailNumber()

        val checklistCollectionOneId = randomId()
        val checklistCollectionOneName = randomName()
        val checklistCollectionTwoId = randomId()
        val checklistCollectionTwoName = randomName()

        val checklistOneId = randomId()
        val checklistTwoId = randomId()

        val checklistItemOneId = randomId()
        val checklistItemOneName = randomName()
        val checklistItemOneAction = randomChecklistItemAction()
        val checklistItemOne = testChecklistItem(
                id = checklistItemOneId,
                checkListId = checklistOneId,
                name = checklistItemOneName,
                action = checklistItemOneAction
        )
        val checklistItemTwoId = randomId()
        val checklistItemTwoName = randomName()
        val checklistItemTwoAction = randomChecklistItemAction()
        val checklistItemTwo = testChecklistItem(
                id = checklistItemTwoId,
                checkListId = checklistOneId,
                name = checklistItemTwoName,
                action = checklistItemTwoAction
        )

        val checklistItemThreeId = randomId()
        val checklistItemThreeName = randomName()
        val checklistItemThreeAction = randomChecklistItemAction()
        val checklistItemThree = testChecklistItem(
                id = checklistItemThreeId,
                checkListId = checklistTwoId,
                name = checklistItemThreeName,
                action = checklistItemThreeAction
        )
        val checklistItemFourId = randomId()
        val checklistItemFourName = randomName()
        val checklistItemFourAction = randomChecklistItemAction()
        val checklistItemFour = testChecklistItem(
                id = checklistItemFourId,
                checkListId = checklistTwoId,
                name = checklistItemFourName,
                action = checklistItemFourAction
        )
        val checklistItemFiveId = randomId()
        val checklistItemFiveName = randomName()
        val checklistItemFiveAction = randomChecklistItemAction()
        val checklistItemFive = testChecklistItem(
                id = checklistItemFiveId,
                checkListId = checklistTwoId,
                name = checklistItemFiveName,
                action = checklistItemFiveAction
        )

        val checklistOneName = randomName()
        val checklistOne = testChecklist(
                id = checklistOneId,
                checklistCollectionId = checklistCollectionOneId,
                name = checklistOneName,
                orderNumber = 0
        )
        val checklistListOne = listOf(checklistOne)

        val checklistTwoName = randomName()
        val checklistTwo = testChecklist(
                id = checklistTwoId,
                checklistCollectionId = checklistCollectionTwoId,
                name = checklistTwoName,
                orderNumber = 1
        )
        val checklistListTwo = listOf(checklistTwo)

        val collectionOne = testChecklistCollection(
                id = checklistCollectionOneId,
                aircraftId = aircraftId,
                name = checklistCollectionOneName,
                orderNumber = 0
        )
        val collectionTwo = testChecklistCollection(
                id = checklistCollectionTwoId,
                aircraftId = aircraftId,
                name = checklistCollectionTwoName,
                orderNumber = 1
        )

        val collectionList = listOf(collectionOne, collectionTwo)
        val emptyAircraft = testAircraft(
                id = aircraftId,
                tailNumber = tailNumber,
                manufacturer = "",
                model = "",
                year = 0
        )

        val itemsOne = listOf(checklistItemOne, checklistItemTwo)

        every { checklistItemDao.getItemsForChecklist(checklistOneId) } returns Single.just(itemsOne)

        val itemsTwo = listOf(checklistItemThree, checklistItemFour, checklistItemFive)

        every { checklistItemDao.getItemsForChecklist(checklistTwoId) } returns Single.just(itemsTwo)

        every { checklistDao.getChecklistsForChecklistCollectionId(checklistCollectionOneId) } returns Single.just(checklistListOne)
        every { checklistDao.getChecklistsForChecklistCollectionId(checklistCollectionTwoId) } returns Single.just(checklistListTwo)

        every { checklistCollectionDao.getChecklistCollections(aircraftId) } returns Single.just(collectionList)

        every { aircraftDao.getAircraft(aircraftId) } returns Single.just(emptyAircraft)

        val repository = FFRepository(mockDatabase)
        val actualAircraftSingle = repository.getAircraft(aircraftId)
        val actualAircraft = actualAircraftSingle.blockingGet()

        val expectedChecklistOne = testChecklist(
                id = checklistOneId,
                checklistCollectionId = checklistCollectionOneId,
                name = checklistOneName,
                orderNumber = 0,
                items = listOf(checklistItemOne, checklistItemTwo)
        )

        val expectedChecklistTwo = testChecklist(
                id = checklistTwoId,
                checklistCollectionId = checklistCollectionTwoId,
                name = checklistTwoName,
                orderNumber = 1,
                items = listOf(checklistItemThree, checklistItemFour, checklistItemFive)
        )

        val expectedChecklistCollectionOne = testChecklistCollection(
                    id = checklistCollectionOneId,
                    aircraftId = aircraftId,
                    name = checklistCollectionOneName,
                    checklists = listOf(expectedChecklistOne),
                    orderNumber = 0
        )

        val expectedChecklistCollectionTwo = testChecklistCollection(
                id = checklistCollectionTwoId,
                aircraftId = aircraftId,
                name = checklistCollectionTwoName,
                checklists = listOf(expectedChecklistTwo),
                orderNumber = 1
        )

        val expectedAircraft = testAircraft(
                id = aircraftId,
                tailNumber = tailNumber,
                checklistCollections = listOf(expectedChecklistCollectionOne, expectedChecklistCollectionTwo),
                manufacturer = "",
                model = "",
                year = 0
        )

        assertThat(actualAircraft, `is`(expectedAircraft))
    }

    @Test
    fun `should Delete a Checklist`() {
        val checklistTwo = testChecklist()

        val repository = FFRepository(mockDatabase)

        repository.deleteChecklist(checklistTwo).blockingAwait()

        verify(exactly = 1) { checklistDao.deleteChecklist(checklistTwo) }
    }
}