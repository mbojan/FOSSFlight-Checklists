package org.fossflight.efb.aircraftList

import android.app.Activity.RESULT_CANCELED
import android.app.Activity.RESULT_OK
import android.content.Intent
import android.view.MenuItem
import android.view.View
import android.widget.TextView
import androidx.fragment.app.testing.launchFragmentInContainer
import androidx.test.core.app.ApplicationProvider
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.assertion.ViewAssertions
import androidx.test.espresso.matcher.ViewMatchers.isDisplayed
import androidx.test.espresso.matcher.ViewMatchers.withId
import androidx.test.espresso.matcher.ViewMatchers.withText
import androidx.test.ext.junit.runners.AndroidJUnit4
import io.mockk.every
import io.mockk.mockk
import io.mockk.verify
import io.reactivex.Flowable
import io.reactivex.Single
import org.fossflight.efb.BundleKeys
import org.fossflight.efb.ForceTrampolineSchedulers
import org.fossflight.efb.FossFlightApplication
import org.fossflight.efb.FossFlightTestApplication
import org.fossflight.efb.PermissionsManager
import org.fossflight.efb.R
import org.fossflight.efb.RequestCodes
import org.fossflight.efb.TestAppModule
import org.fossflight.efb.data.io.FossFlightFileWriter
import org.fossflight.efb.data.persistence.FFRepository
import org.fossflight.efb.data.randomId
import org.fossflight.efb.data.randomTailNumber
import org.fossflight.efb.data.testAircraft
import org.hamcrest.CoreMatchers
import org.hamcrest.CoreMatchers.`is`
import org.hamcrest.CoreMatchers.containsString
import org.hamcrest.MatcherAssert.assertThat
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.robolectric.Shadows.shadowOf
import org.robolectric.annotation.Config
import org.robolectric.shadows.ShadowAlertDialog
import java.io.File
import java.io.IOException

@Config(application = FossFlightTestApplication::class)
@RunWith(AndroidJUnit4::class)
class AircraftListFragmentTest {

    @get:Rule
    var forceTrampolineSchedulers = ForceTrampolineSchedulers()

    private val mockRepository = mockk<FFRepository>()
    private val mockPermissionsManager = mockk<PermissionsManager>()
    private val mockFossFlightFileWriter = mockk<FossFlightFileWriter>()
    private val mockWriteableFile = mockk<File>()
    private val fossFlightExportDirectory = "FossFlight"
    private val mockAircraftListFragmentListener = mockk<AircraftListFragmentListener>()

    val instantiate: () -> AircraftListFragment = {
        val fragment = AircraftListFragment()
        fragment.setParentActivity(mockAircraftListFragmentListener)
        fragment
    }

    @Before
    fun `setup mocks`() {
        val application = ApplicationProvider.getApplicationContext<FossFlightTestApplication>()

        val testAppModule = TestAppModule(
                context = application.applicationContext,
                repository = mockRepository,
                permissionsManager = mockPermissionsManager,
                fossFlightFileWriter = mockFossFlightFileWriter
        )

        application.setAppModule(testAppModule)

        every { mockRepository.allAircraft } returns Flowable.just(listOf())

        every { mockPermissionsManager.requestWritePermission(any()) } returns Unit

        every { mockAircraftListFragmentListener.updateToolbarTitle(any()) } returns Unit
        every { mockAircraftListFragmentListener.updateToolbarSubtitle(any()) } returns Unit
        every { mockAircraftListFragmentListener.updateToolbarIcon(any()) } returns Unit
        every { mockAircraftListFragmentListener.startSelectParserDialogFragment(any()) } returns Unit
        every { mockAircraftListFragmentListener.startEditAircraftFragment(any()) } returns Unit
        every { mockAircraftListFragmentListener.startAddAircraftDialog(any()) } returns Unit
        every { mockAircraftListFragmentListener.displayToast(any()) } returns Unit
        every { mockAircraftListFragmentListener.displayAlertDialog(any()) } returns Unit
    }

    @Test
    fun `should Display an AlertDialog with the Import Problem Message`() {
        val scenario = launchFragmentInContainer(
                instantiate = instantiate
        )

        scenario.onFragment {
            it.displayImportProblemDialog()
        }

        verify { mockAircraftListFragmentListener.displayAlertDialog(R.string.import_problem_message) }
    }

    @Test
    fun `should Handle a Cancelled Result from File Picker`() {
        val scenario = launchFragmentInContainer(
                instantiate = instantiate
        )

        scenario.onFragment {
            it.onActivityResult(RequestCodes.GET_CSV, RESULT_CANCELED, null)
        }
        //expect no exceptions
    }

    @Test
    fun `should Handle an Empty Intent from File Picker`() {
        val scenario = launchFragmentInContainer(
                instantiate = instantiate
        )

        scenario.onFragment {
            it.onActivityResult(RequestCodes.GET_CSV, RESULT_OK, null)
        }
        //expect no exceptions
    }

    @Test
    fun `should Display the Empty Aircraft Message by Default`() {
        launchFragmentInContainer(
                instantiate = instantiate
        )

        val emptyListMessage = "It looks like you don't have any aircraft yet.\nTap 'Add Aircraft' in the options menu to get started."
        onView(withId(R.id.empty)).check(ViewAssertions.matches(isDisplayed()))
        onView(withId(R.id.empty)).check(ViewAssertions.matches(withText(emptyListMessage)))
    }

    @Test
    fun `should hide the Empty Aircraft Message`() {
        val scenario = launchFragmentInContainer(
                instantiate = instantiate
        )

        scenario.onFragment {
            it.updateUIForNonEmptyItemList()
        }

        onView(withId(R.id.empty)).check(ViewAssertions.matches(CoreMatchers.not(isDisplayed())))
    }

    @Test
    fun `should include Add Aircraft on the Options Menu`() {
        val scenario = launchFragmentInContainer(
                instantiate = instantiate
        )

        scenario.onFragment {
            val expectedMenuItemText = it.requireActivity().getString(R.string.add_aircraft)
            val menuItem = shadowOf(it.activity).optionsMenu.findItem(R.id.add_aircraft)
            assertThat(menuItem.title.toString(), `is`(expectedMenuItemText))
            assertThat(menuItem.isVisible, `is`(true))
        }
    }

    @Test
    fun `should start the Edit Aircraft Fragment when adding an Aircraft manually`() {
        val scenario = launchFragmentInContainer(
                instantiate = instantiate
        )

        scenario.onFragment {
            val indexForAddingAnAircraftManually = 1
            it.addAircraft(indexForAddingAnAircraftManually)
        }

        verify { mockAircraftListFragmentListener.startEditAircraftFragment(BundleKeys.UNDEFINED) }
    }

    @Test
    fun `should start the select parser Dialog Fragment when importing an Aircraft`() {
        val scenario = launchFragmentInContainer(
                instantiate = instantiate
        )

        scenario.onFragment {
            val indexForImportingAnAircraft = 0
            it.addAircraft(indexForImportingAnAircraft)
            verify { mockAircraftListFragmentListener.startSelectParserDialogFragment(it) }
        }

    }

    @Test
    fun `should start the GetContent Intent when Importing an Aircraft`() {
        val scenario = launchFragmentInContainer(
                instantiate = instantiate
        )

        scenario.onFragment {
            val indexForImportingAnAircraft = 1
            it.importChecklistType(indexForImportingAnAircraft)
        }

        val actualIntent = shadowOf(ApplicationProvider.getApplicationContext<FossFlightApplication>()).nextStartedActivity
        assertThat(actualIntent.action, `is`(Intent.ACTION_GET_CONTENT))
        assertThat(actualIntent.type, `is`("text/*"))
    }

    @Test
    fun `should start the Add Aircraft Dialog when tapping Add Aircraft on the Options Menu`() {
        val scenario = launchFragmentInContainer(
                instantiate = instantiate
        )

        scenario.onFragment {
            it.onOptionsItemSelected(withMenuItem(R.id.add_aircraft))
            verify { mockAircraftListFragmentListener.startAddAircraftDialog(it) }
        }
    }

    @Test
    fun `should set the Toolbar text appropriately`() {
        val scenario = launchFragmentInContainer(
                instantiate = instantiate
        )

        scenario.onFragment {
            val expectedTitle = it.requireActivity().getString(R.string.app_name)
            val expectedSubtitle = it.requireActivity().getString(R.string.your_aircraft_list)
            verify { mockAircraftListFragmentListener.updateToolbarTitle(expectedTitle) }
            verify { mockAircraftListFragmentListener.updateToolbarSubtitle(expectedSubtitle) }
        }
    }

    @Test
    fun `should create an AlertDialog with Build Information from the Options Menu`() {
        val scenario = launchFragmentInContainer(
                instantiate = instantiate
        )

        scenario.onFragment {
            it.onOptionsItemSelected(withMenuItem(R.id.about))
        }

        val latestAlertDialog = ShadowAlertDialog.getLatestAlertDialog()
        val shadowAlertDialog = shadowOf(latestAlertDialog)
        val buildInfoView = shadowAlertDialog.view.findViewById<View>(R.id.build_info_view)
        val actualMessage = (buildInfoView as TextView).text.toString()

        val expectedBuildDatePrefix = "Build date:"
        assertThat(actualMessage, containsString(expectedBuildDatePrefix))
        val expectedCommitHashPrefix = "Build date:"
        assertThat(actualMessage, containsString(expectedCommitHashPrefix))
        val expectedApkVersionCodePrefix = "APK Version:"
        assertThat(actualMessage, containsString(expectedApkVersionCodePrefix))
    }

    @Test
    @Throws(IOException::class)
    fun `should Export an Aircraft when having previously obtained Write Permissions`() {
        val aircraftId = randomId()
        val tailNumber = randomTailNumber()
        val aircraft = testAircraft(id = aircraftId, tailNumber = tailNumber)

        every { mockRepository.getAircraft(aircraftId) } returns Single.just(aircraft)

        every { mockPermissionsManager.hasWritePermission(any()) } returns true
        every { mockPermissionsManager.canWriteToStorage() } returns true
        every { mockPermissionsManager.getWritableDirectory(any()) } returns mockWriteableFile

        every { mockWriteableFile.exists() } returns true
        every { mockWriteableFile.absolutePath } returns fossFlightExportDirectory

        val scenario = launchFragmentInContainer(
                instantiate = instantiate
        )

        scenario.onFragment {
            it.exportAircraft(aircraftId)
        }

        val expectedExportFileName = fossFlightExportDirectory + "/" + tailNumber + "_ffc.csv"

        verify { mockFossFlightFileWriter.write(any(), eq(expectedExportFileName)) }
    }

    @Test
    @Throws(IOException::class)
    fun `should Continue Exporting an Aircraft`() {
        val aircraftId = randomId()
        val tailNumber = randomTailNumber()
        val aircraft = testAircraft(id = aircraftId, tailNumber = tailNumber)

        every { mockRepository.getAircraft(aircraftId) } returns Single.just(aircraft)
        every { mockPermissionsManager.hasWritePermission(any()) } returns false
        every { mockPermissionsManager.getWritableDirectory(any()) } returns mockWriteableFile

        every { mockWriteableFile.exists() } returns true
        every { mockWriteableFile.absolutePath } returns fossFlightExportDirectory

        val scenario = launchFragmentInContainer(
                instantiate = instantiate
        )

        scenario.onFragment {
            it.exportAircraft(aircraftId)
            it.continueExportingAircraft()
        }

        val expectedExportFileName = fossFlightExportDirectory + "/" + tailNumber + "_ffc.csv"
        verify { mockFossFlightFileWriter.write(any(), eq(expectedExportFileName)) }
    }

    @Test
    fun `should Display an AlertDialog with the Export Problem Message`() {
        val scenario = launchFragmentInContainer(
                instantiate = instantiate
        )

        scenario.onFragment {
            it.displayWriteProblems()
        }

        verify { mockAircraftListFragmentListener.displayAlertDialog(R.string.export_problem_message)}
    }

    @Test
    fun `should Display The Deleted Aircraft Toast With The TailNumber when deleting an Aircraft`() {
        val tailNumber = randomTailNumber()

        val scenario = launchFragmentInContainer(
                instantiate = instantiate
        )

        scenario.onFragment {
            it.displayDeletedAircraftDialog(tailNumber)
        }

        verify { mockAircraftListFragmentListener.displayToast("Deleted: $tailNumber") }

    }

    private fun withMenuItem(menuItemId: Int): MenuItem {
        val mockMenuItem = mockk<MenuItem>()
        every { mockMenuItem.itemId } returns menuItemId
        return mockMenuItem
    }
}