package org.fossflight.efb.aircraftList

import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.widget.TextView
import androidx.test.core.app.ActivityScenario
import androidx.test.ext.junit.runners.AndroidJUnit4
import io.mockk.every
import io.mockk.mockk
import io.mockk.verify
import io.reactivex.Flowable
import org.fossflight.efb.MainActivity
import org.fossflight.efb.R
import org.fossflight.efb.data.model.Aircraft
import org.fossflight.efb.data.persistence.FFRepository
import org.fossflight.efb.data.randomId
import org.fossflight.efb.data.randomName
import org.fossflight.efb.data.randomTailNumber
import org.fossflight.efb.data.testAircraft
import org.hamcrest.CoreMatchers
import org.hamcrest.CoreMatchers.`is`
import org.junit.Assert.assertThat
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.robolectric.Shadows.shadowOf
import org.robolectric.shadows.ShadowPopupMenu
import java.util.concurrent.ThreadLocalRandom
import javax.inject.Inject

@RunWith(AndroidJUnit4::class)
class AircraftListItemViewHolderTest {

    private val mockAircraftListItemListener = mockk<AircraftListItemListener>()

    private val tailNumber = randomTailNumber()
    private val aircraftId = randomId()

    @Inject
    private val mockRepository = mockk<FFRepository>()

    @Before
    fun `set up mocks and activity`() {
        every { mockRepository.allAircraft } returns Flowable.just(listOf())

        every { mockAircraftListItemListener.editAircraft(any()) } returns Unit
        every { mockAircraftListItemListener.startChecklistCollectionFragment(any()) } returns Unit
        every { mockAircraftListItemListener.deleteAircraft(any()) } returns Unit
        every { mockAircraftListItemListener.exportAircraft(any()) } returns Unit
    }

    @Test
    fun `should get the Aircraft Name as the string`() {
        val aircraft = testAircraft(
                id = aircraftId,
                tailNumber = tailNumber
        )

        ActivityScenario.launch(MainActivity::class.java).use { scenario ->
            scenario.onActivity { mainActivity ->
                val view = createView(mainActivity)
                val aircraftListItemViewHolder = AircraftListItemViewHolder(view, mockAircraftListItemListener)
                aircraftListItemViewHolder.bind(aircraft)

                assertThat(aircraftListItemViewHolder.toString(), CoreMatchers.endsWith("'$tailNumber'"))
            }
        }
    }

    @Test
    fun `should bind the Aircraft Information To The View`() {

        val manfacturer = randomName()
        val model = randomName()
        val year = ThreadLocalRandom.current().nextInt(java.lang.Integer.MAX_VALUE)
        val aircraft = Aircraft(
                id = aircraftId,
                tailNumber = tailNumber,
                manufacturer = manfacturer,
                model = model,
                year = year
        )

        ActivityScenario.launch(MainActivity::class.java).use { scenario ->
            scenario.onActivity { mainActivity ->
                val view = createView(mainActivity)
                val aircraftListItemViewHolder = AircraftListItemViewHolder(view, mockAircraftListItemListener)
                aircraftListItemViewHolder.bind(aircraft)

                assertThat(textFromViewWithId(view, R.id.content), `is`(tailNumber))
                assertThat(textFromViewWithId(view, R.id.short_description), `is`("$year $manfacturer $model"))
            }
        }
    }

    @Test
    fun `should display a default message for the short description on the view when binding the Aircraft Information To The View`() {

        val aircraft = Aircraft(
                id = aircraftId,
                tailNumber = tailNumber,
                manufacturer = "",
                model = "",
                year = 0
        )

        ActivityScenario.launch(MainActivity::class.java).use { scenario ->
            scenario.onActivity { mainActivity ->
                val view = createView(mainActivity)
                val aircraftListItemViewHolder = AircraftListItemViewHolder(view, mockAircraftListItemListener)

                aircraftListItemViewHolder.bind(aircraft)

                assertThat(textFromViewWithId(view, R.id.content), `is`(tailNumber))
                assertThat(textFromViewWithId(view, R.id.short_description), `is`("An Aircraft"))
            }
        }
    }

    @Test
    fun `should display the Checklist Collections when tapped`() {
        val aircraft = testAircraft(
                id = aircraftId,
                tailNumber = tailNumber
        )

        ActivityScenario.launch(MainActivity::class.java).use { scenario ->
            scenario.onActivity { mainActivity ->
                val view = createView(mainActivity)
                val aircraftListItemViewHolder = AircraftListItemViewHolder(view, mockAircraftListItemListener)

                aircraftListItemViewHolder.bind(aircraft)

                view.performClick()

                verify { mockAircraftListItemListener.startChecklistCollectionFragment(aircraftId) }
            }
        }
    }

    @Test
    fun `should display the popup menu for the Aircraft`() {
        val aircraft = testAircraft(
                id = aircraftId,
                tailNumber = tailNumber
        )

        ActivityScenario.launch(MainActivity::class.java).use { scenario ->
            scenario.onActivity { mainActivity ->
                val view = createView(mainActivity)
                val aircraftListItemViewHolder = AircraftListItemViewHolder(view, mockAircraftListItemListener)

                aircraftListItemViewHolder.bind(aircraft)

                view.performLongClick()

                val latestPopupMenu = ShadowPopupMenu.getLatestPopupMenu()
                val shadowLatestPopupMenu = shadowOf(latestPopupMenu)

                assertThat(shadowLatestPopupMenu.isShowing, `is`(true))
            }
        }
    }

    @Test
    fun `should display the Delete Option on the popup menu for the Aircraft`() {
        val aircraft = testAircraft(
                id = aircraftId,
                tailNumber = tailNumber
        )

        ActivityScenario.launch(MainActivity::class.java).use { scenario ->
            scenario.onActivity { mainActivity ->
                val view = createView(mainActivity)
                val aircraftListItemViewHolder = AircraftListItemViewHolder(view, mockAircraftListItemListener)

                aircraftListItemViewHolder.bind(aircraft)

                view.performLongClick()

                val latestPopupMenu = ShadowPopupMenu.getLatestPopupMenu()

                val menu = latestPopupMenu.menu
                val deleteAircraftItem = menu.getItem(1)

                assertThat(deleteAircraftItem.title.toString(), `is`("Delete"))
            }
        }
    }

    @Test
    fun `should disable clickability after deleting an Aircraft`() {
        val aircraft = testAircraft(
                id = aircraftId,
                tailNumber = tailNumber
        )

        ActivityScenario.launch(MainActivity::class.java).use { scenario ->
            scenario.onActivity { mainActivity ->

                val view = createView(mainActivity)
                val aircraftListItemViewHolder = AircraftListItemViewHolder(view, mockAircraftListItemListener)

                aircraftListItemViewHolder.bind(aircraft)

                view.performLongClick()

                val latestPopupMenu = ShadowPopupMenu.getLatestPopupMenu()
                val shadowLatestPopupMenu = shadowOf(latestPopupMenu)

                val onMenuItemClickListener = shadowLatestPopupMenu.onMenuItemClickListener

                val mockMenuItem = getMenuItem(R.id.delete_item)

                onMenuItemClickListener.onMenuItemClick(mockMenuItem)

                assertThat(view.isClickable, `is`(false))

            }
        }
    }

    @Test
    fun `should delete an Aircraft from the popup menu`() {
        val aircraft = testAircraft(
                id = aircraftId,
                tailNumber = tailNumber
        )

        ActivityScenario.launch(MainActivity::class.java).use { scenario ->
            scenario.onActivity { mainActivity ->

                val view = createView(mainActivity)
                val aircraftListItemViewHolder = AircraftListItemViewHolder(view, mockAircraftListItemListener)

                aircraftListItemViewHolder.bind(aircraft)

                view.performLongClick()

                val latestPopupMenu = ShadowPopupMenu.getLatestPopupMenu()
                val shadowLatestPopupMenu = shadowOf(latestPopupMenu)

                val onMenuItemClickListener = shadowLatestPopupMenu.onMenuItemClickListener

                val mockMenuItem = getMenuItem(R.id.delete_item)

                onMenuItemClickListener.onMenuItemClick(mockMenuItem)

                verify { mockAircraftListItemListener.deleteAircraft(aircraft) }
            }
        }

    }

    @Test
    fun `should display the Edit Option on the popup menu`() {
        val aircraft = testAircraft(
                id = aircraftId,
                tailNumber = tailNumber
        )

        ActivityScenario.launch(MainActivity::class.java).use { scenario ->
            scenario.onActivity { mainActivity ->

                val view = createView(mainActivity)
                val aircraftListItemViewHolder = AircraftListItemViewHolder(view, mockAircraftListItemListener)

                aircraftListItemViewHolder.bind(aircraft)

                view.performLongClick()

                val latestPopupMenu = ShadowPopupMenu.getLatestPopupMenu()

                val menu = latestPopupMenu.menu
                val menuItem = menu.getItem(0)

                assertThat(menuItem.title.toString(), `is`("Edit"))
            }
        }
    }

    @Test
    fun `should edit an Aircraft from the popup menu`() {
        val aircraft = testAircraft(
                id = aircraftId,
                tailNumber = tailNumber
        )

        ActivityScenario.launch(MainActivity::class.java).use { scenario ->
            scenario.onActivity { mainActivity ->

                val view = createView(mainActivity)
                val aircraftListItemViewHolder = AircraftListItemViewHolder(view, mockAircraftListItemListener)

                aircraftListItemViewHolder.bind(aircraft)

                view.performLongClick()

                val latestPopupMenu = ShadowPopupMenu.getLatestPopupMenu()
                val shadowLatestPopupMenu = shadowOf(latestPopupMenu)

                val onMenuItemClickListener = shadowLatestPopupMenu.onMenuItemClickListener

                val mockMenuItem = getMenuItem(R.id.edit_item)

                onMenuItemClickListener.onMenuItemClick(mockMenuItem)

                verify { mockAircraftListItemListener.editAircraft(aircraftId) }

            }
        }
    }

    @Test
    fun `should disable clickability after tapping edit an Aircraft`() {
        val aircraft = testAircraft(
                id = aircraftId,
                tailNumber = tailNumber
        )

        ActivityScenario.launch(MainActivity::class.java).use { scenario ->
            scenario.onActivity { mainActivity ->

                val view = createView(mainActivity)
                val aircraftListItemViewHolder = AircraftListItemViewHolder(view, mockAircraftListItemListener)

                aircraftListItemViewHolder.bind(aircraft)

                view.performLongClick()

                val latestPopupMenu = ShadowPopupMenu.getLatestPopupMenu()
                val shadowLatestPopupMenu = shadowOf(latestPopupMenu)

                val onMenuItemClickListener = shadowLatestPopupMenu.onMenuItemClickListener

                val mockMenuItem = getMenuItem(R.id.edit_item)

                onMenuItemClickListener.onMenuItemClick(mockMenuItem)

                assertThat(view.isClickable, `is`(false))

            }
        }
    }

    @Test
    fun `should display the Export Option on the popup menu`() {
        val aircraft = testAircraft(
                id = aircraftId,
                tailNumber = tailNumber
        )

        ActivityScenario.launch(MainActivity::class.java).use { scenario ->
            scenario.onActivity { mainActivity ->

                val view = createView(mainActivity)
                val aircraftListItemViewHolder = AircraftListItemViewHolder(view, mockAircraftListItemListener)

                aircraftListItemViewHolder.bind(aircraft)

                view.performLongClick()

                val latestPopupMenu = ShadowPopupMenu.getLatestPopupMenu()

                val menu = latestPopupMenu.menu
                val menuItem = menu.getItem(2)

                assertThat(menuItem.title.toString(), `is`("Export Aircraft"))
            }
        }
    }

    @Test
    fun `should export an Aircraft from the popup menu`() {
        val aircraft = testAircraft(
                id = aircraftId,
                tailNumber = tailNumber
        )

        ActivityScenario.launch(MainActivity::class.java).use { scenario ->
            scenario.onActivity { mainActivity ->

                val view = createView(mainActivity)
                val aircraftListItemViewHolder = AircraftListItemViewHolder(view, mockAircraftListItemListener)

                aircraftListItemViewHolder.bind(aircraft)

                view.performLongClick()

                val latestPopupMenu = ShadowPopupMenu.getLatestPopupMenu()
                val shadowLatestPopupMenu = shadowOf(latestPopupMenu)

                val onMenuItemClickListener = shadowLatestPopupMenu.onMenuItemClickListener

                val mockMenuItem = getMenuItem(R.id.export_aircraft)

                onMenuItemClickListener.onMenuItemClick(mockMenuItem)

                verify { mockAircraftListItemListener.exportAircraft(aircraftId) }
            }
        }
    }

    private fun getMenuItem(menuItemId: Int): MenuItem {
        val mockMenuItem = mockk<MenuItem>()
        every { mockMenuItem.itemId } returns menuItemId
        return mockMenuItem
    }

    private fun textFromViewWithId(view: View, viewId: Int): String {
        return (view.findViewById<View>(viewId) as TextView).text.toString()
    }

    private fun createView(mainActivity: MainActivity): View {
        val inflater = LayoutInflater.from(mainActivity)
        return inflater.inflate(R.layout.listable_card, null)
    }
}
