package org.fossflight.efb.aircraftList

import android.widget.TextView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.test.core.app.ActivityScenario
import androidx.test.ext.junit.runners.AndroidJUnit4
import io.mockk.every
import io.mockk.mockk
import io.reactivex.Flowable
import org.fossflight.efb.MainActivity
import org.fossflight.efb.data.persistence.FFRepository
import org.fossflight.efb.data.testAircraft
import org.hamcrest.CoreMatchers.`is`
import org.hamcrest.CoreMatchers.containsString
import org.junit.Assert.assertThat
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import java.util.*
import javax.inject.Inject



@RunWith(AndroidJUnit4::class)
class AircraftListViewAdapterTest {

    @Inject
    private val mockRepository = mockk<FFRepository>()

    @Before
    fun setUp() {
        every { mockRepository.allAircraft } returns Flowable.just(listOf())
    }

    @Test
    fun `should create an AircraftListItemViewHolder`() {
        ActivityScenario.launch(MainActivity::class.java).use { scenario ->
            scenario.onActivity { activity ->

                val recyclerView = RecyclerView(activity)
                recyclerView.layoutManager = LinearLayoutManager(activity)

                val mockListener = mockk<AircraftListItemListener>()
                val viewAdapter = AircraftListViewAdapter(mockListener)

                val viewHolder = viewAdapter.onCreateViewHolder(recyclerView, 0)

                assertThat(viewHolder.itemView.id, `is`(org.fossflight.efb.R.id.listable_card))
                assertThat(viewHolder.toString(), containsString("AircraftListItemViewHolder"))
            }
        }
    }

    @Test
    fun `should bind the ViewHolder to an Item`() {
        ActivityScenario.launch(MainActivity::class.java).use { scenario ->
            scenario.onActivity { activity ->
                val recyclerView = RecyclerView(activity)
                recyclerView.layoutManager = LinearLayoutManager(activity)

                val aircrafts = listOf(testAircraft(), testAircraft(), testAircraft(), testAircraft())

                val mockListener = mockk<AircraftListItemListener>()
                val viewAdapter = AircraftListViewAdapter(mockListener)
                val viewHolder = viewAdapter.onCreateViewHolder(recyclerView, 0)

                viewAdapter.setAircrafts(aircrafts)

                val indexToBind = Random().nextInt(aircrafts.size)
                viewAdapter.bindViewHolder(viewHolder, indexToBind)

                val contentView = viewHolder.itemView.findViewById<TextView>(org.fossflight.efb.R.id.content)

                val actualTailNumber = contentView.text.toString()
                val expectedTailNumber = aircrafts[indexToBind].tailNumber

                assertThat(actualTailNumber, `is`(expectedTailNumber))
            }
        }
    }

    @Test
    fun `should get the Item Count from the ViewAdapter`() {
        ActivityScenario.launch(MainActivity::class.java).use { scenario ->
            scenario.onActivity { activity ->
                val recyclerView = RecyclerView(activity)
                recyclerView.layoutManager = LinearLayoutManager(activity)

                val aircrafts = listOf(testAircraft(), testAircraft(), testAircraft(), testAircraft())

                val mockListener = mockk<AircraftListItemListener>()
                val viewAdapter = AircraftListViewAdapter(mockListener)

                viewAdapter.setAircrafts(aircrafts)

                assertThat(viewAdapter.itemCount, `is`(aircrafts.size))
            }
        }
    }

}
