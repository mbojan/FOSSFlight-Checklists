package org.fossflight.efb

import android.view.View

import org.fossflight.efb.checklist.ChecklistItemListener
import org.fossflight.efb.data.model.ChecklistItem

class FossFlightItemViewHolderImpl internal constructor(
        view: View,
        checklistItemListener: ChecklistItemListener
) : FossFlightItemViewHolder(view, checklistItemListener) {

    override fun bind(checklistItem: ChecklistItem) {
        super.bind(checklistItem)
    }
}
