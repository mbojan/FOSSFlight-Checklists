package org.fossflight.efb.checklist

import org.fossflight.efb.data.model.ChecklistItemState
import org.fossflight.efb.data.model.ChecklistItemType
import org.fossflight.efb.data.randomId
import org.fossflight.efb.data.randomName
import org.fossflight.efb.data.testChecklist
import org.fossflight.efb.data.testChecklistItem
import org.hamcrest.CoreMatchers.`is`
import org.hamcrest.MatcherAssert.assertThat
import org.hamcrest.core.Is
import org.junit.Assert
import org.junit.Test

class ChecklistPlayerTest {

    private val itemOne = testChecklistItem(orderNumber = 0, state = ChecklistItemState.PRECHECKED)
    private val itemTwo = testChecklistItem(orderNumber = 1, state = ChecklistItemState.PRECHECKED)
    private val itemThree = testChecklistItem(orderNumber = 2, state = ChecklistItemState.PRECHECKED)
    private val itemFour = testChecklistItem(orderNumber = 2, state = ChecklistItemState.PRECHECKED)
    private val itemFive = testChecklistItem(orderNumber = 2, state = ChecklistItemState.PRECHECKED)

    private val itemSix = testChecklistItem(orderNumber = 2, state = ChecklistItemState.PRECHECKED)
    private val itemSeven = testChecklistItem(state = ChecklistItemState.PRECHECKED)
    private val itemEight = testChecklistItem(
            state = ChecklistItemState.PRECHECKED,
            type = ChecklistItemType.SECTION_HEADER.toString()
    )
    private val itemNine = testChecklistItem(state = ChecklistItemState.PRECHECKED)

    private val checklist = testChecklist(
            name = randomName(),
            checklistCollectionId = randomId(),
            items = listOf(itemOne, itemTwo, itemThree, itemFour, itemFive),
            orderNumber = 3
    )
    private val nextChecklist = testChecklist(
            id = randomId(),
            name = randomName(),
            items = listOf(itemSix, itemSeven, itemEight, itemNine),
            orderNumber = checklist.orderNumber + 1
    )

    @Test
    fun `should mark the first item as HIGHLIGHTED the first time 'mark item as checked' is called`() {

        val checklistPlayer = ChecklistPlayer()
        checklistPlayer.checklist = checklist

        checklistPlayer.markItemAsChecked()
        assertThat(itemOne.state, `is`(ChecklistItemState.HIGHLIGHTED))
    }

    @Test
    fun `should mark the second item as Highlighted the second time 'mark item as checked' is called`() {
        val checklistPlayer = ChecklistPlayer()
        checklistPlayer.checklist = checklist

        checklistPlayer.markItemAsChecked()
        checklistPlayer.markItemAsChecked()

        assertThat(itemOne.state, `is`(ChecklistItemState.CHECKED))
        assertThat(itemTwo.state, `is`(ChecklistItemState.HIGHLIGHTED))
    }

    @Test
    fun `should get the index of the Highlighted Item after marking an item as checked`() {
        val checklistPlayer = ChecklistPlayer()
        checklistPlayer.checklist = checklist

        checklistPlayer.markItemAsChecked()     // start the checklist and highlight item at index 0 (itemOne)
        checklistPlayer.markItemAsChecked()     // mark the item at index 0 as checked and highlight item at index 1 (itemTwo)

        val indexOfHighlightedItem = checklistPlayer.markItemAsChecked()  // mark the item at index 1 as checked and highlight item at index 2 (itemThree)
        assertThat(indexOfHighlightedItem, `is`(2))
        assertThat(itemThree.state, `is`(ChecklistItemState.HIGHLIGHTED))
    }

    @Test
    fun `should mark the next item as skipped`() {
        val checklistPlayer = ChecklistPlayer()
        checklistPlayer.checklist = checklist

        checklistPlayer.markItemAsChecked()
        checklistPlayer.markItemAsSkipped()

        assertThat(itemOne.state, Is.`is`(ChecklistItemState.SKIPPED))
    }

    @Test
    fun `should Highlight the Second Item in the Checklist when 'Mark the Next Item' as Skipped is Called`() {
        val checklistPlayer = ChecklistPlayer()
        checklistPlayer.checklist = checklist

        checklistPlayer.markItemAsChecked()
        checklistPlayer.markItemAsSkipped()

        assertThat(itemOne.state, Is.`is`(ChecklistItemState.SKIPPED))
        assertThat(itemTwo.state, `is`(ChecklistItemState.HIGHLIGHTED))
    }

    @Test
    fun `should get the index of the Highlighted Item after marking an item as skipped`() {
        val checklistPlayer = ChecklistPlayer()
        checklistPlayer.checklist = checklist

        checklistPlayer.markItemAsChecked()     // start the checklist and highlight item at index 0 (itemOne)
        checklistPlayer.markItemAsChecked()     // mark the item at index 0 as checked and highlight item at index 1 (itemTwo)

        val indexOfHighlightedItem = checklistPlayer.markItemAsSkipped()  // mark the item at index 1 as skipped and highlight item at index 2 (itemThree)
        assertThat(indexOfHighlightedItem, `is`(2))
        assertThat(itemThree.state, `is`(ChecklistItemState.HIGHLIGHTED))
    }

    @Test
    fun `should leave the checklist inactive when 'mark item as skipped' is called and the checklist is inactive`() {
        val checklistPlayer = ChecklistPlayer()
        checklistPlayer.checklist = checklist

        checklistPlayer.markItemAsSkipped()

        assertThat(itemOne.state, Is.`is`(ChecklistItemState.PRECHECKED))
        assertThat(itemTwo.state, Is.`is`(ChecklistItemState.PRECHECKED))
        assertThat(itemThree.state, Is.`is`(ChecklistItemState.PRECHECKED))
    }

    @Test
    fun `should indicate the checklist is complete when the last item is highlighted and 'mark item as checked' is called`() {

        val checklistPlayer = ChecklistPlayer()
        checklistPlayer.checklist = checklist

        checklistPlayer.markItemAsChecked()         // start checklist, highlight 0 (itemOne)
        checklistPlayer.markItemAsChecked()         // check 0 (itemOne), highlight 1 (itemTwo)
        checklistPlayer.markItemAsChecked()         // check 1 (itemTwo), highlight 2 (itemThree)
        checklistPlayer.markItemAsChecked()         // check 2 (itemThree), highlight 3 (itemFour)
        checklistPlayer.markItemAsChecked()         // check 3 (itemFour), highlight 4 (itemFive)
        checklistPlayer.markItemAsChecked()         // check 4 (itemFive) checklist complete

        assertThat(itemFive.state, Is.`is`(ChecklistItemState.CHECKED))
        assertThat(checklistPlayer.isCompleted(), `is`(true))
    }

    @Test
    fun `should indicate the checklist is complete when the last item is highlighted and 'mark item as skipped' is called`() {
        val checklistPlayer = ChecklistPlayer()
        checklistPlayer.checklist = checklist

        checklistPlayer.markItemAsChecked()
        checklistPlayer.markItemAsChecked()
        checklistPlayer.markItemAsChecked()
        checklistPlayer.markItemAsChecked()
        checklistPlayer.markItemAsChecked()
        checklistPlayer.markItemAsSkipped()

        assertThat(itemFive.state, `is`(ChecklistItemState.SKIPPED))
        assertThat(checklistPlayer.isCompleted(), `is`(true))
    }

    @Test
    fun `should set All Items to Prechecked State when Resetting a Checklist`() {
        val checklistPlayer = ChecklistPlayer()
        checklistPlayer.checklist = checklist

        checklistPlayer.markItemAsChecked()
        checklistPlayer.markItemAsChecked()
        checklistPlayer.markItemAsChecked()
        checklistPlayer.markItemAsChecked()

        checklistPlayer.resetChecklist()

        checklist.items.forEach { item ->
            assertThat(item.state, `is`(ChecklistItemState.PRECHECKED))
        }
    }

    @Test
    fun `should indicate a checklist as inactive when all of the items are Prechecked`() {
        val checklistPlayer = ChecklistPlayer()
        checklistPlayer.checklist = checklist

        checklistPlayer.markItemAsChecked()
        checklistPlayer.markItemAsChecked()
        checklistPlayer.markItemAsChecked()
        checklistPlayer.markItemAsChecked()

        checklistPlayer.resetChecklist()

        assertThat(checklistPlayer.isActive(), `is`(false))
    }

    @Test
    fun `should indicate a checklist as active when at least one item has been highlighted and has not yet completed`() {
        val checklistPlayer = ChecklistPlayer()
        checklistPlayer.checklist = checklist

        checklistPlayer.markItemAsChecked()
        checklistPlayer.markItemAsChecked()

        assertThat(checklistPlayer.isActive(), `is`(true))
        assertThat(checklistPlayer.isCompleted(), `is`(false))
    }

    @Test
    fun `should Highlight the First Item when Starting the Checklist after it has been reset after it has been completed`() {
        val checklistPlayer = ChecklistPlayer()
        checklistPlayer.checklist = checklist

        checklistPlayer.markItemAsChecked()
        checklistPlayer.markItemAsChecked()
        checklistPlayer.markItemAsChecked()
        checklistPlayer.markItemAsChecked()

        checklistPlayer.resetChecklist()

        assertThat(checklistPlayer.isActive(), `is`(false))
        assertThat(checklistPlayer.isCompleted(), `is`(false))
    }

    @Test
    fun `should delete a Checklist Item from the Checklist`() {
        val checklistPlayer = ChecklistPlayer()
        checklistPlayer.checklist = checklist

        checklistPlayer.deleteChecklistItem(itemTwo)

        assertThat(checklist.items.size, Is.`is`(4))
        assertThat(checklist.items.contains(itemTwo), `is`(false))
    }

    @Test
    fun `should mark the Last Skipped as Highlighted`() {
        val checklistPlayer = ChecklistPlayer()
        checklistPlayer.checklist = checklist

        checklistPlayer.markItemAsChecked()         // start playing the checklist and highlight itemOne
        checklistPlayer.markItemAsChecked()         // mark itemOne as checked, highlight itemTwo
        checklistPlayer.markItemAsChecked()         // mark itemTwo as checked, highlight itemThree
        checklistPlayer.markItemAsChecked()         // mark itemThree as checked, highlight itemFour
        checklistPlayer.markItemAsSkipped()         // mark itemFour as SKIPPED, highlight highlight itemFive
        checklistPlayer.markItemAsChecked()         // mark itemFive as checked, highlight highlight itemSix

        checklistPlayer.highlightLastSkippedItem()
        assertThat(itemFour.state, `is`(ChecklistItemState.HIGHLIGHTED))
    }

    @Test
    fun `should get the index of the Last Skipped Item after highlighting the last skipped item`() {
        val checklistPlayer = ChecklistPlayer()
        checklistPlayer.checklist = checklist

        checklistPlayer.markItemAsChecked()         // start playing the checklist and highlight itemOne
        checklistPlayer.markItemAsChecked()         // mark itemOne as checked, highlight itemTwo
        checklistPlayer.markItemAsChecked()         // mark itemTwo as checked, highlight itemThree
        checklistPlayer.markItemAsChecked()         // mark itemThree as checked, highlight itemFour
        checklistPlayer.markItemAsSkipped()         // mark itemFour as SKIPPED, highlight highlight itemFive
        checklistPlayer.markItemAsChecked()         // mark itemFive as checked, highlight highlight itemSix

        val indexOfLastSkippedItem = checklistPlayer.highlightLastSkippedItem()
        assertThat(indexOfLastSkippedItem, `is`(3))

    }

    @Test
    fun `should leave the Currently Highlighted Item Prechecked when Highlighting the Last Skipped Item`() {
        val checklistPlayer = ChecklistPlayer()
        checklistPlayer.checklist = checklist

        checklistPlayer.markItemAsChecked()         // start playing the checklist and highlight itemOne
        checklistPlayer.markItemAsChecked()         // mark itemOne as checked, highlight itemTwo
        checklistPlayer.markItemAsChecked()         // mark itemTwo as checked, highlight itemThree
        checklistPlayer.markItemAsSkipped()         // mark itemThree as SKIPPED, highlight itemFour
        checklistPlayer.markItemAsChecked()         // mark itemFour as checked, highlight highlight itemFive

        checklistPlayer.highlightLastSkippedItem()
        assertThat(itemThree.state, `is`(ChecklistItemState.HIGHLIGHTED))
        assertThat(itemFive.state, `is`(ChecklistItemState.PRECHECKED))
    }

    @Test
    fun `should Leave a Skipped Item as Skipped when going to 'Previously Skipped Item' from a Skipped Item`() {
        val checklistPlayer = ChecklistPlayer()
        checklistPlayer.checklist = checklist

        checklistPlayer.markItemAsChecked()         // start playing the checklist and highlight itemOne
        checklistPlayer.markItemAsChecked()         // mark itemOne as checked, highlight itemTwo
        checklistPlayer.markItemAsSkipped()         // mark itemTwo as SKIPPED, highlight itemThree
        checklistPlayer.markItemAsSkipped()         // mark itemThree as SKIPPED, highlight itemFour
        checklistPlayer.markItemAsSkipped()         // mark itemFour as SKIPPED, highlight highlight itemFive
        checklistPlayer.markItemAsChecked()         // mark itemFive as checked, display checklist complete dialog

        checklistPlayer.highlightLastSkippedItem()
        checklistPlayer.highlightLastSkippedItem()
        checklistPlayer.highlightLastSkippedItem()

        Assert.assertThat(itemTwo.state, Is.`is`(ChecklistItemState.HIGHLIGHTED))
        Assert.assertThat(itemThree.state, Is.`is`(ChecklistItemState.SKIPPED))
        Assert.assertThat(itemFour.state, Is.`is`(ChecklistItemState.SKIPPED))
    }

    @Test
    fun `should Leave the Lowest Skipped Item as Skipped when Highlighting the Lowest Skipped Item More Times Than the Number of Skipped Items`() {
        val checklistPlayer = ChecklistPlayer()
        checklistPlayer.checklist = checklist

        checklistPlayer.markItemAsChecked()         // start playing the checklist and highlight itemOne
        checklistPlayer.markItemAsChecked()         // mark 0 (itemOne) as checked, highlight 1 (itemTwo)
        checklistPlayer.markItemAsSkipped()         // mark 1 (itemTwo) as SKIPPED, highlight 2 (itemThree)
        checklistPlayer.markItemAsSkipped()         // mark 2 (itemThree) as SKIPPED, highlight 3 (itemFour)
        checklistPlayer.markItemAsSkipped()         // mark 3 (itemFour) as SKIPPED, highlight highlight 4 (itemFive)
        checklistPlayer.markItemAsChecked()         // mark 4 (itemFive) as checked, display checklist complete dialog

        checklistPlayer.highlightLastSkippedItem()
        checklistPlayer.highlightLastSkippedItem()
        checklistPlayer.highlightLastSkippedItem()
        checklistPlayer.highlightLastSkippedItem()
        checklistPlayer.highlightLastSkippedItem()
        checklistPlayer.highlightLastSkippedItem()

        assertThat(itemTwo.state, `is`(ChecklistItemState.HIGHLIGHTED))
        assertThat(itemThree.state, `is`(ChecklistItemState.SKIPPED))
        assertThat(itemFour.state, `is`(ChecklistItemState.SKIPPED))
    }

    @Test
    fun `should Highlight the Next Highest Skipped Item  when Marking a Skipped Item as Checked`() {
        val checklistPlayer = ChecklistPlayer()
        checklistPlayer.checklist = checklist

        checklistPlayer.markItemAsChecked()             // start playing the checklist and highlight itemOne
        checklistPlayer.markItemAsChecked()             // mark itemOne as checked, highlight itemTwo
        checklistPlayer.markItemAsSkipped()             // mark itemTwo as SKIPPED, highlight itemThree
        checklistPlayer.markItemAsChecked()             // mark itemThree as checked, highlight itemFour
        checklistPlayer.markItemAsSkipped()             // mark itemFour as SKIPPED, highlight highlight itemFive
        checklistPlayer.markItemAsChecked()             // mark itemFive as checked, display checklist complete dialog

        checklistPlayer.highlightLastSkippedItem()      // highlight itemFour
        checklistPlayer.highlightLastSkippedItem()      // highlight itemTwo
        checklistPlayer.markItemAsChecked()             // mark itemTwo as checked, highlight itemFour

        assertThat(itemTwo.state, `is`(ChecklistItemState.CHECKED))
        assertThat(itemThree.state, `is`(ChecklistItemState.CHECKED))
        assertThat(itemFour.state, `is`(ChecklistItemState.HIGHLIGHTED))
    }

    @Test
    fun `should Highlight the Next Highest Prechecked Item when Marking a Skipped Item as Checked`() {
        val checklistPlayer = ChecklistPlayer()
        checklistPlayer.checklist = checklist

        checklistPlayer.markItemAsChecked()             // start playing the checklist and highlight itemOne
        checklistPlayer.markItemAsSkipped()             // mark itemOne as SKIPPED, highlight itemTwo
        checklistPlayer.markItemAsChecked()             // mark itemTwo as checked, highlight itemThree
        checklistPlayer.markItemAsSkipped()             // mark itemThree as checked, highlight itemFour

        checklistPlayer.highlightLastSkippedItem()      // highlight itemTwo
        checklistPlayer.markItemAsChecked()             // mark itemTwo as checked, highlight itemFour

        assertThat(itemTwo.state, `is`(ChecklistItemState.CHECKED))
        assertThat(itemThree.state, `is`(ChecklistItemState.CHECKED))
        assertThat(itemFour.state, `is`(ChecklistItemState.HIGHLIGHTED))
        assertThat(itemFive.state, `is`(ChecklistItemState.PRECHECKED))
    }

    @Test
    fun `should Highlight the First Item after a Section Header when Marking an Item as Checked and the Next Item is a Section Header`() {
        val checklistPlayer = ChecklistPlayer()
        checklistPlayer.checklist = nextChecklist

        checklistPlayer.markItemAsChecked()             // start playing the checklist and highlight itemSix
        checklistPlayer.markItemAsChecked()             // mark itemSix as checked, highlight itemSeven
        checklistPlayer.markItemAsChecked()             // mark itemSeven as checked, hightlight itemNine (itemEight is a section header)

        assertThat(itemSeven.state, `is`(ChecklistItemState.CHECKED))
        assertThat(itemNine.state, `is`(ChecklistItemState.HIGHLIGHTED))
    }

    @Test
    fun `should Highlight the First Item after a Section Header when Marking an Item as Skipped and the Next Item is a Section Header`() {
        val checklistPlayer = ChecklistPlayer()
        checklistPlayer.checklist = nextChecklist

        checklistPlayer.markItemAsChecked()             // start playing the checklist and highlight itemSix
        checklistPlayer.markItemAsChecked()             // mark itemSix as checked, highlight itemSeven
        checklistPlayer.markItemAsSkipped()             // mark itemSeven as SKIPPED, hightlight itemNine (itemEight is a section header)

        assertThat(itemSeven.state, `is`(ChecklistItemState.SKIPPED))
        assertThat(itemNine.state, `is`(ChecklistItemState.HIGHLIGHTED))
    }

    @Test
    fun `should indicate there are skipped items when skipped items exist in the active checklist`() {
        val checklistPlayer = ChecklistPlayer()
        checklistPlayer.checklist = checklist

        checklistPlayer.markItemAsChecked()             // start playing the checklist and highlight itemOne
        checklistPlayer.markItemAsSkipped()             // mark 0 (itemOne) as SKIPPED, highlight (1) itemTwo
        checklistPlayer.markItemAsChecked()             // mark (1) itemTwo as checked, highlight (2) itemThree
        checklistPlayer.markItemAsSkipped()             // mark (2) itemThree as skipped, highlight (3) itemFour

        assertThat(checklistPlayer.hasSkippedItems(), `is`(true))
    }

    @Test
    fun `should mark the next item as Checked when a partially played checklist is assigned and 'mark item as checked' is called`() {
        itemOne.state = ChecklistItemState.CHECKED
        itemTwo.state = ChecklistItemState.CHECKED
        itemThree.state = ChecklistItemState.HIGHLIGHTED
        itemFour.state = ChecklistItemState.PRECHECKED
        val partiallyPlayedChecklist = testChecklist(items = listOf(itemOne, itemTwo, itemThree, itemFour))

        val checklistPlayer = ChecklistPlayer()
        checklistPlayer.checklist = partiallyPlayedChecklist

        checklistPlayer.markItemAsChecked()

        assertThat(itemThree.state, `is`(ChecklistItemState.CHECKED))
        assertThat(itemFour.state, `is`(ChecklistItemState.HIGHLIGHTED))
    }

    @Test
    fun `should give the index of the current highlighted item when all items are prechecked, checked, or highlighted`() {
        val checklistPlayer = ChecklistPlayer()
        checklistPlayer.checklist = checklist

        checklistPlayer.markItemAsChecked()             // start playing the checklist and highlight (0)itemOne
        checklistPlayer.markItemAsChecked()             // mark 0 (itemOne) as CHECKED, highlight (1) itemTwo
        checklistPlayer.markItemAsChecked()             // mark (1) itemTwo as CHECKED, highlight (2) itemThree

        val indexOfLastSkippedItem = checklistPlayer.highlightLastSkippedItem()

        assertThat(indexOfLastSkippedItem, `is`(2))
    }

    @Test
    fun `should consistently give the index of the first skipped item repeatedly when requesting the last skipped item after already reaching the first skipped item`() {

        val checklistPlayer = ChecklistPlayer()
        checklistPlayer.checklist = checklist

        checklistPlayer.markItemAsChecked()             // start playing the checklist and highlight (0)itemOne
        checklistPlayer.markItemAsChecked()             // mark 0 (itemOne) as CHECKED, highlight (1) itemTwo
        checklistPlayer.markItemAsSkipped()             // mark (1) itemTwo as SKIPPED, highlight (2) itemThree
        checklistPlayer.markItemAsSkipped()             // mark (2) itemThree as SKIPPED, highlight (3) itemFour
        checklistPlayer.markItemAsChecked()             // mark (3) itemFour as CHECKED, highlight (4) itemFive

        assertThat(checklistPlayer.highlightLastSkippedItem(), `is`(2))
        assertThat(checklistPlayer.highlightLastSkippedItem(), `is`(1))
        assertThat(checklistPlayer.highlightLastSkippedItem(), `is`(1))
        assertThat(checklistPlayer.highlightLastSkippedItem(), `is`(1))
    }

    @Test
    fun `should retrieve the first skipped item in a checklist with skipped items`() {
        val checklistPlayer = ChecklistPlayer()
        checklistPlayer.checklist = checklist

        checklistPlayer.markItemAsChecked()             // start playing the checklist and highlight (0)itemOne
        checklistPlayer.markItemAsChecked()             // mark 0 (itemOne) as CHECKED
        checklistPlayer.markItemAsSkipped()             // mark (1) itemTwo as SKIPPED
        checklistPlayer.markItemAsSkipped()             // mark (2) itemThree as SKIPPED
        checklistPlayer.markItemAsChecked()             // mark (3) itemFour as CHECKED

        assertThat(checklistPlayer.highlightFirstSkippedItem(), `is`(1))
    }

    @Test
    fun `should retrieve the active item index when requested`() {
        val checklistPlayer = ChecklistPlayer()
        checklistPlayer.checklist = checklist

        checklistPlayer.markItemAsChecked()             // start playing the checklist and highlight 0 (itemOne)
        checklistPlayer.markItemAsChecked()             // mark 0 (itemOne) as CHECKED, highlight 1 (itemTwo)
        checklistPlayer.markItemAsSkipped()             // mark 1 (itemTwo) as SKIPPED, highlight 2 (itemThree)
        checklistPlayer.markItemAsSkipped()             // mark 2 (itemThree) as SKIPPED, highlight 3 (itemFour)

        assertThat(checklistPlayer.activeItemIndex, `is`(3))
    }

    @Test
    fun `should indicate a checklist with section headers is completed when all items are CHECKED` () {
        val testChecklist = testChecklist(
                items = listOf(
                        testChecklistItem(state = ChecklistItemState.CHECKED),
                        testChecklistItem(state = ChecklistItemState.CHECKED),
                        testChecklistItem(
                                state = ChecklistItemState.PRECHECKED,
                                type = ChecklistItemType.SECTION_HEADER.toString()
                        ),
                        testChecklistItem(state = ChecklistItemState.CHECKED),
                        testChecklistItem(state = ChecklistItemState.CHECKED)
                )
        )

        val checklistPlayer = ChecklistPlayer()
        checklistPlayer.checklist = testChecklist

        assertThat(checklistPlayer.isCompleted(), `is`(true))
    }

    @Test
    fun `should indicate a checklist with section headers is completed when all items are SKIPPED` () {
        val testChecklist = testChecklist(
                items = listOf(
                        testChecklistItem(state = ChecklistItemState.SKIPPED),
                        testChecklistItem(state = ChecklistItemState.SKIPPED),
                        testChecklistItem(
                                state = ChecklistItemState.PRECHECKED,
                                type = ChecklistItemType.SECTION_HEADER.toString()
                        ),
                        testChecklistItem(state = ChecklistItemState.SKIPPED),
                        testChecklistItem(state = ChecklistItemState.SKIPPED)
                )
        )

        val checklistPlayer = ChecklistPlayer()
        checklistPlayer.checklist = testChecklist

        assertThat(checklistPlayer.isCompleted(), `is`(true))
    }

    @Test
    fun `should indicate the checklist is completed when all items are CHECKED or SKIPPED` () {
        val testChecklist = testChecklist(
                items = listOf(
                        testChecklistItem(state = ChecklistItemState.SKIPPED),
                        testChecklistItem(state = ChecklistItemState.CHECKED),
                        testChecklistItem(
                                state = ChecklistItemState.PRECHECKED,
                                type = ChecklistItemType.SECTION_HEADER.toString()
                        ),
                        testChecklistItem(state = ChecklistItemState.SKIPPED),
                        testChecklistItem(state = ChecklistItemState.CHECKED)
                )
        )

        val checklistPlayer = ChecklistPlayer()
        checklistPlayer.checklist = testChecklist

        assertThat(checklistPlayer.isCompleted(), `is`(true))
    }

    @Test
    fun `should indicate an empty checklist is not completed`() {
        val testChecklist = testChecklist(items = listOf())

        val checklistPlayer = ChecklistPlayer()
        checklistPlayer.checklist = testChecklist

        assertThat(checklistPlayer.isCompleted(), `is`(false))
    }
}