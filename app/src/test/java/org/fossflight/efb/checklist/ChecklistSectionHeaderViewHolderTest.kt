package org.fossflight.efb.checklist

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.core.content.ContextCompat
import androidx.test.core.app.ActivityScenario
import androidx.test.ext.junit.runners.AndroidJUnit4
import io.mockk.every
import io.mockk.mockk
import io.reactivex.Flowable
import org.fossflight.efb.MainActivity
import org.fossflight.efb.R
import org.fossflight.efb.data.model.Aircraft
import org.fossflight.efb.data.model.ChecklistItemType
import org.fossflight.efb.data.persistence.FFRepository
import org.fossflight.efb.data.testChecklistItem
import org.hamcrest.CoreMatchers.`is`
import org.hamcrest.CoreMatchers.endsWith
import org.hamcrest.CoreMatchers.startsWith
import org.hamcrest.MatcherAssert.assertThat
import org.junit.Before
import org.junit.Ignore
import org.junit.Test
import org.junit.runner.RunWith
import org.robolectric.shadows.ShadowPopupMenu
import javax.inject.Inject

@RunWith(AndroidJUnit4::class)
class ChecklistSectionHeaderViewHolderTest {

    private val mockChecklistItemListener = mockk<ChecklistItemListener>()

    @Inject
    private val mockRepository = mockk<FFRepository>()

    @Before
    fun `set up`() {
        every { mockRepository.allAircraft } returns Flowable.just<List<Aircraft>>(listOf())
    }

    @Test
    fun `should get the Text for the Content View as the String`() {
        ActivityScenario.launch(MainActivity::class.java).use { scenario ->
            scenario.onActivity { mainActivity ->

                val view = createRootCardView(mainActivity)

                val checklistItem = testChecklistItem(
                        name = "Item One",
                        type = ChecklistItemType.SECTION_HEADER.toString()
                )
                val sectionHeaderViewHolder = ChecklistSectionHeaderViewHolder(view, mockChecklistItemListener)
                sectionHeaderViewHolder.bind(checklistItem)

                assertThat(sectionHeaderViewHolder.toString(), startsWith("ChecklistSectionHeaderViewHolder"))
                assertThat(sectionHeaderViewHolder.toString(), endsWith("'" + checklistItem.name + "'"))
            }
        }
    }

    @Test
    fun `should Bind the Checklist Item to the View`() {
        ActivityScenario.launch(MainActivity::class.java).use { scenario ->
            scenario.onActivity { mainActivity ->

                val view = createRootCardView(mainActivity)

                val checklistItem = testChecklistItem(name = "Item One")
                val checklistSectionHeaderViewHolder = ChecklistSectionHeaderViewHolder(view, mockChecklistItemListener)
                checklistSectionHeaderViewHolder.bind(checklistItem)

                val actualName = (view.findViewById<View>(R.id.content) as TextView).text.toString()

                assertThat(actualName, `is`("Item One"))
            }
        }
    }

    @Test
    fun `should display the Delete Option on the Popup Menu for the Item`() {
        ActivityScenario.launch(MainActivity::class.java).use { scenario ->
            scenario.onActivity { mainActivity ->

                val view = createRootCardView(mainActivity)
                val checklistItem = testChecklistItem()

                every { mockChecklistItemListener.isEditable() } returns true

                val checklistSectionHeaderViewHolder = ChecklistSectionHeaderViewHolder(view, mockChecklistItemListener)
                checklistSectionHeaderViewHolder.bind(checklistItem)

                view.performLongClick()

                val latestPopupMenu = ShadowPopupMenu.getLatestPopupMenu()

                val menu = latestPopupMenu.menu
                val deleteItem = menu.getItem(1)

                assertThat(deleteItem.title.toString(), `is`("Delete"))
            }
        }
    }

    @Test
    fun `should apply Section Header Styles when a Checklist Item has a Type of a Section Header`() {
        ActivityScenario.launch(MainActivity::class.java).use { scenario ->
            scenario.onActivity { mainActivity ->

                val view = createRootCardView(mainActivity)

                val checklistItem = testChecklistItem(type = ChecklistItemType.SECTION_HEADER.toString())
                val checklistSectionHeaderViewHolder = ChecklistSectionHeaderViewHolder(view, mockChecklistItemListener)
                checklistSectionHeaderViewHolder.bind(checklistItem)

                val contentView = view.findViewById<TextView>(R.id.content)
                val actualContentTextColor = contentView.currentTextColor
                val expectedContentTextColor = ContextCompat.getColor(view.context, android.R.color.white)
                assertThat(actualContentTextColor, `is`(expectedContentTextColor))
            }
        }
    }

    @Test
    @Ignore("This test started failing when I moved the styles from the layout files to the style files")
    fun `should apply Section Header Background Color when a Checklist Item has a Type of a Section Header`() {
        ActivityScenario.launch(MainActivity::class.java).use { scenario ->
            scenario.onActivity { mainActivity ->
                val view = createRootCardView(mainActivity)

                val checklistItem = testChecklistItem(type = ChecklistItemType.SECTION_HEADER.toString())
                val checklistSectionHeaderViewHolder = ChecklistSectionHeaderViewHolder(view, mockChecklistItemListener)
                checklistSectionHeaderViewHolder.bind(checklistItem)

                val sectionHeaderBackgroundColor = ContextCompat.getColor(view.context, R.color.section_header_background)
                val actualCardBackgroundColor = (view as CardView).cardBackgroundColor.defaultColor
                assertThat(actualCardBackgroundColor, `is`(sectionHeaderBackgroundColor))
            }
        }
    }

    fun createRootCardView(mainActivity: MainActivity): View {
        val root = CardView(mainActivity)
        val layoutParams = ViewGroup.LayoutParams(
                ViewGroup.LayoutParams.WRAP_CONTENT,
                ViewGroup.LayoutParams.WRAP_CONTENT
        )
        layoutParams.height = ViewGroup.LayoutParams.WRAP_CONTENT
        root.layoutParams = layoutParams
        return LayoutInflater.from(mainActivity).inflate(R.layout.listable_section_header, root)
    }
}