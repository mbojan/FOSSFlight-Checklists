package org.fossflight.efb.checklist

import io.mockk.every
import io.mockk.mockk
import io.mockk.verify
import io.reactivex.Completable
import io.reactivex.Single
import io.reactivex.disposables.CompositeDisposable
import org.fossflight.efb.BundleKeys
import org.fossflight.efb.ForceTrampolineSchedulers
import org.fossflight.efb.data.model.ChecklistItemType
import org.fossflight.efb.data.persistence.FFRepository
import org.fossflight.efb.data.randomChecklistItemAction
import org.fossflight.efb.data.randomId
import org.fossflight.efb.data.randomName
import org.fossflight.efb.data.testChecklist
import org.fossflight.efb.data.testChecklistItem
import org.hamcrest.core.Is.`is`
import org.junit.Assert.assertThat
import org.junit.Before
import org.junit.Rule
import org.junit.Test

class EditChecklistItemPresenterTest {

    @get:Rule
    var forceTrampolineSchedulers = ForceTrampolineSchedulers()

    private val checklistId = randomId()
    private val itemId = randomId()
    private val checklistName = randomName()
    private val checklistItemName = randomName()
    private val checklistItemAction = randomChecklistItemAction()
    private val checklist = testChecklist(name = checklistName)

    private val mockFFRepository = mockk<FFRepository>()
    private val mockEditChecklistItemView = mockk<EditChecklistItemView>()

    @Before
    fun `set up`() {
        every { mockEditChecklistItemView.updateToolbarInfoForAdd(any()) } returns Unit
        every { mockEditChecklistItemView.updateToolbarInfoForUpdate(any()) } returns Unit
        every { mockEditChecklistItemView.updateEditableInformation(any(), any(), any()) } returns Unit
        every { mockEditChecklistItemView.displayNameRequiredViolation() } returns Unit
        every { mockEditChecklistItemView.saveSuccessful(any()) } returns Unit

        every { mockFFRepository.getChecklist(checklistId) } returns Single.just(checklist)
    }

    @Test
    fun `should Display The ChecklistItem Information When The ChecklistItem Is Defined`() {
        val checklistItem = testChecklistItem(type = ChecklistItemType.SECTION_HEADER.toString())

        every { mockFFRepository.getChecklistItem(itemId) } returns Single.just(checklistItem)

        val editChecklistItemPresenter = EditChecklistItemPresenter(
                editChecklistItemView = mockEditChecklistItemView,
                repository = mockFFRepository,
                checklistId = checklistId,
                itemId = itemId
        )
        editChecklistItemPresenter.presentData()

        val ordinalOfChecklistItemType = ChecklistItemType.parseCommonName(checklistItem.type).ordinal

        verify {
            mockEditChecklistItemView.updateEditableInformation(checklistItem.name, checklistItem.action, ordinalOfChecklistItemType)
        }
    }

    @Test
    fun `should Display Empty Strings For The Checklist Item Information When The Item Is Undefined`() {

        val editChecklistItemPresenter = EditChecklistItemPresenter(
                editChecklistItemView = mockEditChecklistItemView,
                repository = mockFFRepository,
                checklistId = checklistId,
                itemId = BundleKeys.UNDEFINED
        )
        editChecklistItemPresenter.presentData()

        verify {
            mockEditChecklistItemView.updateEditableInformation("", "", ChecklistItemType.NORMAL.ordinal)
        }
    }

    @Test
    fun `should Display The Name Required Violation When Saving An Item With An Empty Name`() {
        val checklistItemName = ""
        val checklistItem = testChecklistItem(
                checkListId = checklistId,
                name = checklistItemName,
                action = checklistItemAction
        )

        every { mockFFRepository.saveChecklistItem(checklistItem) } returns Completable.complete()

        val editChecklistItemPresenter = EditChecklistItemPresenter(
                editChecklistItemView = mockEditChecklistItemView,
                repository = mockFFRepository,
                checklistId = checklistId,
                itemId = BundleKeys.UNDEFINED
        )

        editChecklistItemPresenter.saveChecklistItem(checklistItemName, checklistItemAction, ChecklistItemType.NORMAL.toString())

        verify { mockEditChecklistItemView.displayNameRequiredViolation() }
    }

    @Test
    fun `should Display The Name Required Violation When Saving An Item With A Name Made Of Whitespace`() {
        val checklistItemName = "    "
        val checklistItem = testChecklistItem(
                checkListId = checklistId,
                name = checklistItemName,
                action = checklistItemAction
        )

        every { mockFFRepository.saveChecklistItem(checklistItem) } returns Completable.complete()

        val editChecklistItemPresenter = EditChecklistItemPresenter(
                editChecklistItemView = mockEditChecklistItemView,
                repository = mockFFRepository,
                checklistId = checklistId,
                itemId = BundleKeys.UNDEFINED
        )

        editChecklistItemPresenter.saveChecklistItem(checklistItemName, checklistItemAction, ChecklistItemType.NORMAL.toString())

        verify { mockEditChecklistItemView.displayNameRequiredViolation() }
    }

    @Test
    fun `should Update The Toolbar Information when adding a checklist item`() {

        val editChecklistItemPresenter = EditChecklistItemPresenter(
                editChecklistItemView = mockEditChecklistItemView,
                repository = mockFFRepository,
                checklistId = checklistId,
                itemId = BundleKeys.UNDEFINED
        )

        editChecklistItemPresenter.presentData()

        verify { mockEditChecklistItemView.updateToolbarInfoForAdd(checklistName) }
    }

    @Test
    fun `should Update The Toolbar Information when editing a checklist item`() {
        val checklistItem = testChecklistItem(type = ChecklistItemType.SECTION_HEADER.toString())

        every { mockFFRepository.getChecklistItem(itemId) } returns Single.just(checklistItem)

        val editChecklistItemPresenter = EditChecklistItemPresenter(
                editChecklistItemView = mockEditChecklistItemView,
                repository = mockFFRepository,
                checklistId = checklistId,
                itemId = itemId
        )

        editChecklistItemPresenter.presentData()

        verify { mockEditChecklistItemView.updateToolbarInfoForUpdate(checklistName) }
    }

    @Test
    fun `should Communicate a Successful Save to the EditChecklistItemView after Saving a ChecklistItem`() {
        val checklistItem = testChecklistItem(
                id = BundleKeys.UNDEFINED,
                checkListId = checklistId,
                name = checklistItemName,
                action = checklistItemAction
        )

        every { mockFFRepository.saveChecklistItem(checklistItem) } returns Completable.complete()

        val editChecklistItemPresenter = EditChecklistItemPresenter(
                editChecklistItemView = mockEditChecklistItemView,
                repository = mockFFRepository,
                checklistId = checklistId,
                itemId = BundleKeys.UNDEFINED
        )

        editChecklistItemPresenter.saveChecklistItem(checklistItemName, checklistItemAction, ChecklistItemType.NORMAL.toString())

        verify { mockEditChecklistItemView.saveSuccessful(checklistItemName) }
    }

    @Test
    fun `should Save a ChecklistItem`() {
        val checklistItem = testChecklistItem(
                id = BundleKeys.UNDEFINED,
                checkListId = checklistId,
                name = checklistItemName,
                action = checklistItemAction,
                type = ChecklistItemType.SECTION_HEADER.toString()
        )

        every { mockFFRepository.saveChecklistItem(checklistItem) } returns Completable.complete()

        val editChecklistItemPresenter = EditChecklistItemPresenter(
                editChecklistItemView = mockEditChecklistItemView,
                repository = mockFFRepository,
                checklistId = checklistId,
                itemId = BundleKeys.UNDEFINED
        )

        editChecklistItemPresenter.saveChecklistItem(
                checklistItemName,
                checklistItemAction,
                ChecklistItemType.SECTION_HEADER.toString())

        verify { mockFFRepository.saveChecklistItem(checklistItem) }
    }

    @Test
    fun `should Dispose of the CompositeDisposable`() {
        val compositeDisposable = CompositeDisposable()
        val editChecklistItemPresenter = EditChecklistItemPresenter(
                editChecklistItemView = mockEditChecklistItemView,
                repository = mockFFRepository,
                compositeDisposable = compositeDisposable,
                checklistId = checklistId,
                itemId = BundleKeys.UNDEFINED
        )

        editChecklistItemPresenter.dispose()

        assertThat(compositeDisposable.isDisposed, `is`(true))
    }

    @Test
    fun `should Add a Subscription when Presenting the Data of an Undefined ChecklistItem`() {
        val compositeDisposable = CompositeDisposable()
        val editChecklistItemPresenter = EditChecklistItemPresenter(
                editChecklistItemView = mockEditChecklistItemView,
                repository = mockFFRepository,
                compositeDisposable = compositeDisposable,
                checklistId = checklistId,
                itemId = BundleKeys.UNDEFINED
        )

        editChecklistItemPresenter.presentData()

        assertThat(compositeDisposable.size(), `is`(1))
    }

    @Test
    fun `should Add Two Subscriptions when Presenting the Data of a Defined ChecklistItem`() {

        every { mockFFRepository.getChecklistItem(itemId) } returns Single.just(testChecklistItem())

        val compositeDisposable = CompositeDisposable()
        val editChecklistItemPresenter = EditChecklistItemPresenter(
                editChecklistItemView = mockEditChecklistItemView,
                repository = mockFFRepository,
                compositeDisposable = compositeDisposable,
                checklistId = checklistId,
                itemId = itemId
        )

        editChecklistItemPresenter.presentData()

        assertThat(compositeDisposable.size(), `is`(2))
    }

    @Test
    fun `should Add a Subscription when Saving a ChecklistItem`() {
        val checklistItem = testChecklistItem(
                id = BundleKeys.UNDEFINED,
                checkListId = checklistId,
                name = checklistItemName,
                action = checklistItemAction
        )

        every { mockFFRepository.saveChecklistItem(checklistItem) } returns Completable.complete()

        val compositeDisposable = CompositeDisposable()
        val editChecklistItemPresenter = EditChecklistItemPresenter(
                editChecklistItemView = mockEditChecklistItemView,
                repository = mockFFRepository,
                compositeDisposable = compositeDisposable,
                checklistId = checklistId,
                itemId = BundleKeys.UNDEFINED
        )

        editChecklistItemPresenter.saveChecklistItem(checklistItemName, checklistItemAction, ChecklistItemType.NORMAL.toString())

        assertThat(compositeDisposable.size(), `is`(1))
    }
}