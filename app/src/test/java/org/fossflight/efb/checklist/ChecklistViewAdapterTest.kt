package org.fossflight.efb.checklist

import android.widget.TextView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.test.core.app.ActivityScenario
import androidx.test.ext.junit.runners.AndroidJUnit4
import io.mockk.every
import io.mockk.mockk
import io.reactivex.Flowable
import org.fossflight.efb.MainActivity
import org.fossflight.efb.R
import org.fossflight.efb.data.model.Aircraft
import org.fossflight.efb.data.model.ChecklistItemType
import org.fossflight.efb.data.persistence.FFRepository
import org.fossflight.efb.data.testChecklistItem
import org.hamcrest.CoreMatchers.`is`
import org.hamcrest.CoreMatchers.containsString
import org.junit.Assert.assertThat
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import java.util.*
import javax.inject.Inject

@RunWith(AndroidJUnit4::class)
class ChecklistViewAdapterTest {

    @Inject
    private val mockRepository = mockk<FFRepository>()

    @Before
    fun `set up mocks and activity`() {
        every { mockRepository.allAircraft } returns Flowable.just<List<Aircraft>>(listOf())
    }

    @Test
    fun `should Create a ChecklistItemViewHolder`() {
        ActivityScenario.launch(MainActivity::class.java).use { scenario ->
            scenario.onActivity { mainActivity ->
                val recyclerView = RecyclerView(mainActivity)
                recyclerView.layoutManager = LinearLayoutManager(mainActivity)

                val presenter = mockk<ChecklistItemListener>()
                val viewAdapter = ChecklistViewAdapter(presenter)

                val checklistItemViewHolder = viewAdapter.onCreateViewHolder(recyclerView, 0)

                assertThat(checklistItemViewHolder.itemView.id, `is`(R.id.listable_card))
            }
        }
    }

    @Test
    fun `should Bind the ViewHolder to an Item`() {
        ActivityScenario.launch(MainActivity::class.java).use { scenario ->
            scenario.onActivity { mainActivity ->

                val recyclerView = RecyclerView(mainActivity)
                recyclerView.layoutManager = LinearLayoutManager(mainActivity)

                val checklistItems = listOf(
                        testChecklistItem(),
                        testChecklistItem(),
                        testChecklistItem(),
                        testChecklistItem(),
                        testChecklistItem()
                )

                val checklistItemListener = mockk<ChecklistItemListener>()
                val viewAdapter = ChecklistViewAdapter(checklistItemListener)

                viewAdapter.setChecklistItems(checklistItems)

                val randomTypeIndex = Random().nextInt(ChecklistItemType.values().size)
                val viewHolder = viewAdapter.onCreateViewHolder(recyclerView, randomTypeIndex)

                val indexToBind = Random().nextInt(checklistItems.size)
                viewAdapter.onBindViewHolder(viewHolder, indexToBind)

                val contentView = viewHolder.itemView.findViewById<TextView>(R.id.content)

                val actualItemName = contentView.text.toString()
                val expectedItemName = checklistItems[indexToBind].name

                assertThat(actualItemName, `is`(expectedItemName))
            }
        }
    }

    @Test
    fun `should Get Normal View Type by Default when the Item List is Empty`() {
        ActivityScenario.launch(MainActivity::class.java).use { scenario ->
            scenario.onActivity { mainActivity ->

                val recyclerView = RecyclerView(mainActivity)
                recyclerView.layoutManager = LinearLayoutManager(mainActivity)

                val checklistItemListener = mockk<ChecklistItemListener>()
                val viewAdapter = ChecklistViewAdapter(checklistItemListener)

                viewAdapter.setChecklistItems(listOf())

                val itemOneViewType = viewAdapter.getItemViewType(0)
                val checklistItemOneType = ChecklistItemType.values()[itemOneViewType]

                assertThat(checklistItemOneType, `is`(ChecklistItemType.NORMAL))
            }
        }
    }

    @Test
    fun `should Get a ViewType Based on the Corresponding ChecklistItem Type`() {
        ActivityScenario.launch(MainActivity::class.java).use { scenario ->
            scenario.onActivity { mainActivity ->

                val recyclerView = RecyclerView(mainActivity)
                recyclerView.layoutManager = LinearLayoutManager(mainActivity)

                val sectionHeaderItem = testChecklistItem(
                        type = ChecklistItemType.SECTION_HEADER.toString()
                )
                val checklistItem = testChecklistItem(
                        type = ChecklistItemType.NORMAL.toString()
                )
                val checklistItems = listOf(sectionHeaderItem, checklistItem)

                val checklistItemListener = mockk<ChecklistItemListener>()
                val viewAdapter = ChecklistViewAdapter(checklistItemListener)

                viewAdapter.setChecklistItems(checklistItems)

                val itemOneViewType = viewAdapter.getItemViewType(0)
                val itemTwoViewType = viewAdapter.getItemViewType(1)

                val checklistItemOneType = ChecklistItemType.values()[itemOneViewType]
                val checklistItemTwoType = ChecklistItemType.values()[itemTwoViewType]

                assertThat(checklistItemOneType, `is`(ChecklistItemType.SECTION_HEADER))
                assertThat(checklistItemTwoType, `is`(ChecklistItemType.NORMAL))
            }
        }
    }

    @Test
    fun `should Create a SectionHeader ViewHolder`() {
        ActivityScenario.launch(MainActivity::class.java).use { scenario ->
            scenario.onActivity { mainActivity ->

                val recyclerView = RecyclerView(mainActivity)
                recyclerView.layoutManager = LinearLayoutManager(mainActivity)

                val checklistItemListener = mockk<ChecklistItemListener>()

                val viewAdapter = ChecklistViewAdapter(checklistItemListener)

                val fossFlightItemViewHolderOne = viewAdapter.onCreateViewHolder(recyclerView, ChecklistItemType.SECTION_HEADER.ordinal)
                fossFlightItemViewHolderOne.bind(testChecklistItem())

                assertThat(fossFlightItemViewHolderOne.toString(), containsString("ChecklistSectionHeaderViewHolder"))
            }
        }
    }

    @Test
    fun `should Create a Checklist Item ViewHolder`() {
        ActivityScenario.launch(MainActivity::class.java).use { scenario ->
            scenario.onActivity { mainActivity ->

                val recyclerView = RecyclerView(mainActivity)
                recyclerView.layoutManager = LinearLayoutManager(mainActivity)

                val checklistItemListener = mockk<ChecklistItemListener>()

                val viewAdapter = ChecklistViewAdapter(checklistItemListener)

                val fossFlightItemViewHolderOne = viewAdapter.onCreateViewHolder(recyclerView, ChecklistItemType.NORMAL.ordinal)
                fossFlightItemViewHolderOne.bind(testChecklistItem())

                assertThat(fossFlightItemViewHolderOne.toString(), containsString("ChecklistItemViewHolder"))

            }
        }
    }
}