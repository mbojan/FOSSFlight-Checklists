package org.fossflight.efb.checklist

import androidx.room.EmptyResultSetException
import io.mockk.every
import io.mockk.mockk
import io.mockk.verify
import io.reactivex.Completable
import io.reactivex.Single
import io.reactivex.disposables.CompositeDisposable
import org.fossflight.efb.ForceTrampolineSchedulers
import org.fossflight.efb.data.model.ChecklistItem
import org.fossflight.efb.data.model.ChecklistItemState
import org.fossflight.efb.data.model.ChecklistItemType
import org.fossflight.efb.data.persistence.FFRepository
import org.fossflight.efb.data.randomId
import org.fossflight.efb.data.randomName
import org.fossflight.efb.data.randomTailNumber
import org.fossflight.efb.data.testChecklist
import org.fossflight.efb.data.testChecklistItem
import org.hamcrest.core.Is.`is`
import org.junit.Assert.assertThat
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import java.util.*

class ChecklistPresenterTest {

    @get:Rule
    var forceTrampolineSchedulers = ForceTrampolineSchedulers()

    private val tailNumber = randomTailNumber()
    private val checklistName = randomName()
    private val checklistId = randomId()
    private val checklistCollectionId = randomId()
    private val idOfNextChecklist = randomId()
    private val nextChecklistName = randomName()

    private val itemOne = testChecklistItem(state = ChecklistItemState.PRECHECKED)
    private val itemTwo = testChecklistItem(state = ChecklistItemState.PRECHECKED)
    private val itemThree = testChecklistItem(state = ChecklistItemState.PRECHECKED)
    private val itemFour = testChecklistItem(state = ChecklistItemState.PRECHECKED)
    private val itemFive = testChecklistItem(state = ChecklistItemState.PRECHECKED)
    private val itemSix = testChecklistItem(state = ChecklistItemState.PRECHECKED)
    private val itemSeven = testChecklistItem(state = ChecklistItemState.PRECHECKED)
    private val itemEight = testChecklistItem(
            state = ChecklistItemState.PRECHECKED,
            type = ChecklistItemType.SECTION_HEADER.toString()
    )
    private val itemNine = testChecklistItem(state = ChecklistItemState.PRECHECKED)
    private val checklist = testChecklist(
            name = checklistName,
            checklistCollectionId = checklistCollectionId,
            items = listOf(itemOne, itemTwo, itemThree, itemFour, itemFive),
            orderNumber = 3
    )
    private val nextChecklist = testChecklist(
            id = idOfNextChecklist,
            name = nextChecklistName,
            items = listOf(itemSix, itemSeven, itemEight, itemNine),
            orderNumber = checklist.orderNumber + 1
    )

    private val compositeDisposable = CompositeDisposable()

    private val mockChecklistView = mockk<ChecklistView>()
    private val mockRepository = mockk<FFRepository>()

    private val checklistPresenter = ChecklistPresenter(
            checklistView = mockChecklistView,
            repository = mockRepository,
            compositeDisposable = compositeDisposable,
            checklistId = checklistId
    )

    @Before
    fun `set up mocks`() {
        every { mockChecklistView.updateToolbarChecklistName(any()) } returns Unit
        every { mockChecklistView.updateRecyclerView(any()) } returns Unit
        every { mockChecklistView.editChecklistItem(any()) } returns Unit
        every { mockChecklistView.updateToolbarTailNumber(any()) } returns Unit
        every { mockChecklistView.updateUIForNonEmptyItemList() } returns Unit
        every { mockChecklistView.activateChecklist() } returns Unit
        every { mockChecklistView.disableGoToPreviouslyCheckedButton() } returns Unit
        every { mockChecklistView.updateItemListScrolledToPosition(any()) } returns Unit
        every { mockChecklistView.enableGoToPreviouslyCheckedButton() } returns Unit
        every { mockChecklistView.displayChecklistCompleteDialog() } returns Unit
        every { mockChecklistView.displayChecklistCompleteWithSkippedItemsDialog() } returns Unit
        every { mockChecklistView.displayButtonsForInactiveChecklist() } returns Unit
        every { mockChecklistView.displayDeletedChecklistItemDialog(any()) } returns Unit
        every { mockChecklistView.updateUIForEmptyItemList() } returns Unit
        every { mockChecklistView.updateUIForChecklistActiveState(any(), any()) } returns Unit
        every { mockChecklistView.finishChecklist() } returns Unit
        every { mockChecklistView.updateCompletedChecklistButtons(any()) } returns Unit

        every { mockRepository.getAircraftTailNumberForChecklist(checklistId) } returns  Single.just(tailNumber)
        every { mockRepository.getAircraftTailNumberForChecklist(idOfNextChecklist) } returns Single.just(tailNumber)

        every { mockRepository.getChecklistIdFromChecklistCollectionId(checklist.checklistCollectionId, checklist.orderNumber + 1) } returns Single.just(idOfNextChecklist)

        every { mockRepository.getChecklist(checklistId) } returns Single.just(checklist)

        every { mockRepository.getChecklist(idOfNextChecklist) } returns Single.just(nextChecklist)

        every { mockRepository.deleteChecklistItem(itemTwo) } returns Completable.complete()

        every { mockRepository.getChecklistIdFromChecklistCollectionId(nextChecklist.checklistCollectionId, nextChecklist.orderNumber + 1) } returns Single.error(EmptyResultSetException(""))
    }

    @Test
    fun `should update the View for an empty Checklist when Checklist Items is Empty`() {
        val emptyChecklistId = randomId()
        val emptyChecklist = testChecklist(
                id = emptyChecklistId,
                items = listOf()
        )

        every { mockRepository.getChecklist(emptyChecklistId) } returns Single.just(emptyChecklist)

        every { mockRepository.getAircraftTailNumberForChecklist(emptyChecklistId) } returns Single.just(tailNumber)

        val checklistPresenter = ChecklistPresenter(
                checklistView = mockChecklistView,
                repository = mockRepository,
                checklistId = emptyChecklistId
        )

        checklistPresenter.presentData()

        verify { mockChecklistView.updateUIForEmptyItemList() }
    }

    @Test
    fun `should update the View for an Empty Checklist after deleting the last Checklist Item`() {
        val checklistId = randomId()

        val checklistItem = testChecklistItem(state = ChecklistItemState.PRECHECKED)

        val checklist = testChecklist(id = checklistId, items = listOf(checklistItem))

        every { mockRepository.getChecklist(checklistId) } returns Single.just(checklist)

        every { mockRepository.getAircraftTailNumberForChecklist(checklistId) } returns Single.just(tailNumber)

        every { mockRepository.deleteChecklistItem(checklistItem) } returns Completable.complete()

        val checklistPresenter = ChecklistPresenter(
                checklistView = mockChecklistView,
                repository = mockRepository,
                checklistId = checklistId
        )

        checklistPresenter.presentData()
        checklistPresenter.deleteChecklistItem(checklistItem)

        verify { mockChecklistView.updateUIForEmptyItemList() }
    }

    @Test
    fun `should update the RecyclerView with the List Of Checklists`() {
        checklistPresenter.presentData()

        val expectedChecklistItems = listOf(itemOne, itemTwo, itemThree, itemFour, itemFive)
        verify(exactly = 1) { mockChecklistView.updateRecyclerView(expectedChecklistItems) }
    }

    @Test
    fun `should update the Toolbar with the Tail Number`() {
        checklistPresenter.presentData()
        verify(exactly = 1) { mockChecklistView.updateToolbarChecklistName(checklistName) }
        verify(exactly = 1) { mockChecklistView.updateToolbarTailNumber(tailNumber) }
    }

    @Test
    fun `should hide the Empty List Message when Presenting the data at Least One Aircraft`() {
        checklistPresenter.presentData()
        verify(exactly = 1) { mockChecklistView.updateUIForNonEmptyItemList() }
    }

    @Test
    fun `should retain the current Checklist State when presenting the Data Multiple times`() {
        checklistPresenter.presentData()
        checklistPresenter.presentData()

        verify { mockRepository.getChecklist(checklistId) }
        verify { mockRepository.getAircraftTailNumberForChecklist(checklistId) }
    }

    @Test
    fun `should indicate as Editable when A Checklist is Inactive`() {
        checklistPresenter.presentData()
        assertThat(checklistPresenter.isEditable(), `is`(true))
    }

    @Test
    fun `should indicate as Not Editable when a Checklist is Active`() {
        checklistPresenter.presentData()
        checklistPresenter.markItemAsChecked()         // start playing the checklist and highlight itemOne
        assertThat(checklistPresenter.isEditable(), `is`(false))
    }

    @Test
    fun `should be able to retrieve the active checklist`() {
        checklistPresenter.presentData()
        assertThat(checklistPresenter.checklist, `is`(checklist))
    }

    @Test
    fun `should update the UI for an active checklist when presenting data for an active checklist`() {

        val activeChecklistId = randomId()

        val checkedItemOne = testChecklistItem(state = ChecklistItemState.CHECKED)
        val checkedItemTwo = testChecklistItem(state = ChecklistItemState.CHECKED)
        val checkedItemThree = testChecklistItem(state = ChecklistItemState.HIGHLIGHTED)
        val checkedItemFour = testChecklistItem(state = ChecklistItemState.PRECHECKED)

        val activeChecklist = testChecklist(
                id = activeChecklistId,
                items = listOf(checkedItemOne, checkedItemTwo, checkedItemThree, checkedItemFour)
        )

        val activeChecklistPlayer = ChecklistPlayer()
        activeChecklistPlayer.checklist = activeChecklist

        val checklistPresenter = ChecklistPresenter(
                checklistView = mockChecklistView,
                repository = mockRepository,
                compositeDisposable = compositeDisposable,
                checklistId = activeChecklistId,
                checklistPlayer = activeChecklistPlayer
        )

        checklistPresenter.presentData()

        verify { mockChecklistView.activateChecklist() }
    }

    @Test
    fun `should update the 'scrolled to position' when presenting data for an active checklist`() {
        val activeChecklistId = randomId()

        val checkedItemOne = testChecklistItem(state = ChecklistItemState.CHECKED)
        val checkedItemTwo = testChecklistItem(state = ChecklistItemState.CHECKED)
        val checkedItemThree = testChecklistItem(state = ChecklistItemState.HIGHLIGHTED)
        val checkedItemFour = testChecklistItem(state = ChecklistItemState.PRECHECKED)

        val activeChecklist = testChecklist(
                id = activeChecklistId,
                items = listOf(checkedItemOne, checkedItemTwo, checkedItemThree, checkedItemFour)
        )

        val activeChecklistPlayer = ChecklistPlayer()
        activeChecklistPlayer.checklist = activeChecklist

        val checklistPresenter = ChecklistPresenter(
                checklistView = mockChecklistView,
                repository = mockRepository,
                compositeDisposable = compositeDisposable,
                checklistId = activeChecklistId,
                checklistPlayer = activeChecklistPlayer
        )

        checklistPresenter.presentData()

        verify { mockChecklistView.updateItemListScrolledToPosition(2) }
    }

    @Test
    fun `should enable the 'go to previously skipped' button when presenting data after a rotation and playing an active checklist with skipped items`() {
        val activeChecklistId = randomId()

        val checkedItemOne = testChecklistItem(state = ChecklistItemState.CHECKED)
        val checkedItemTwo = testChecklistItem(state = ChecklistItemState.SKIPPED)
        val checkedItemThree = testChecklistItem(state = ChecklistItemState.HIGHLIGHTED)
        val checkedItemFour = testChecklistItem(state = ChecklistItemState.PRECHECKED)

        val activeChecklist = testChecklist(
                id = activeChecklistId,
                items = listOf(checkedItemOne, checkedItemTwo, checkedItemThree, checkedItemFour)
        )

        val activeChecklistPlayer = ChecklistPlayer()
        activeChecklistPlayer.checklist = activeChecklist

        val checklistPresenter = ChecklistPresenter(
                checklistView = mockChecklistView,
                repository = mockRepository,
                compositeDisposable = compositeDisposable,
                checklistId = activeChecklistId,
                checklistPlayer = activeChecklistPlayer
        )

        checklistPresenter.presentData()

        verify { mockChecklistView.enableGoToPreviouslyCheckedButton() }
    }

    @Test
    fun `should disable the 'go to previously skipped' button when presenting data after a rotation and playing an active checklist with skipped items`() {
        val activeChecklistId = randomId()

        val checkedItemOne = testChecklistItem(state = ChecklistItemState.CHECKED)
        val checkedItemTwo = testChecklistItem(state = ChecklistItemState.CHECKED)
        val checkedItemThree = testChecklistItem(state = ChecklistItemState.HIGHLIGHTED)
        val checkedItemFour = testChecklistItem(state = ChecklistItemState.PRECHECKED)

        val activeChecklist = testChecklist(
                id = activeChecklistId,
                items = listOf(checkedItemOne, checkedItemTwo, checkedItemThree, checkedItemFour)
        )

        val activeChecklistPlayer = ChecklistPlayer()
        activeChecklistPlayer.checklist = activeChecklist

        val checklistPresenter = ChecklistPresenter(
                checklistView = mockChecklistView,
                repository = mockRepository,
                compositeDisposable = compositeDisposable,
                checklistId = activeChecklistId,
                checklistPlayer = activeChecklistPlayer
        )

        checklistPresenter.presentData()

        verify { mockChecklistView.disableGoToPreviouslyCheckedButton() }
    }

    @Test
    fun `should display an Active Checklist when Marking an Item as Checked`() {
        checklistPresenter.presentData()

        checklistPresenter.markItemAsChecked()
        verify { mockChecklistView.activateChecklist() }
    }

    @Test
    fun `should Edit the Checklist Item information from the Checklist Item View`() {
        checklistPresenter.presentData()

        checklistPresenter.editChecklistItem(itemTwo.id)

        verify { mockChecklistView.editChecklistItem(itemTwo.id) }
    }

    @Test
    fun `should refresh the Checklist Data after Editing a Checklist Item`() {
        checklistPresenter.presentData()

        checklistPresenter.editChecklistItem(itemTwo.id)

        checklistPresenter.presentData()

        verify(exactly = 2) { mockRepository.getChecklist(checklistId) }
    }

    @Test
    fun `should dispose of the Composite Disposable`() {
        checklistPresenter.presentData()
        checklistPresenter.dispose()
        assertThat(compositeDisposable.isDisposed, `is`(true))
    }

    @Test
    fun `should Add Two More subscription to the CompositeDisposable when starting the Next Checklist`() {
        checklistPresenter.presentData()
        checklistPresenter.startNextChecklist()
        assertThat(compositeDisposable.size(), `is`(4))
    }

    @Test
    fun `should add another subscription to the CompositeDisposable after deleting a Checklist`() {

        checklistPresenter.presentData()
        checklistPresenter.deleteChecklistItem(itemTwo)
        assertThat(compositeDisposable.size(), `is`(3))
    }

    @Test
    fun `should Highlight the First Item in the Checklist the First Time 'Mark Item as Checked' is Called`() {
        checklistPresenter.presentData()

        checklistPresenter.markItemAsChecked()
        verify(exactly = 1) { mockChecklistView.updateItemListScrolledToPosition(0) }
    }

    @Test
    fun `should Highlight the Second Item in the Checklist, the Second Time Mark Item as Checked is Called`() {
        checklistPresenter.presentData()

        checklistPresenter.markItemAsChecked()
        checklistPresenter.markItemAsChecked()
        verify(exactly = 1) { mockChecklistView.updateItemListScrolledToPosition(0) }
    }

    @Test
    fun `should Highlight the Second Item in the Checklist when 'Mark the Next Item' as Skipped is Called`() {
        checklistPresenter.presentData()

        checklistPresenter.markItemAsChecked()
        checklistPresenter.markItemAsSkipped()
        verify(exactly = 1) { mockChecklistView.updateItemListScrolledToPosition(0) }
        verify(exactly = 1) { mockChecklistView.updateItemListScrolledToPosition(1) }
    }

    @Test
    fun `should show the Checklist Completed AlertDialog when All Items are Checked, Last Item is Highlighted and Mark Item as Checked is Called`() {
        checklistPresenter.presentData()

        checklistPresenter.markItemAsChecked()  // start playing the checklist and highlight itemOne
        checklistPresenter.markItemAsChecked()  // mark itemOne as checked, highlight itemTwo
        checklistPresenter.markItemAsChecked()  // mark itemTwo as checked, highlight itemThree
        checklistPresenter.markItemAsChecked()  // mark itemThree as checked, highlight itemFour
        checklistPresenter.markItemAsChecked()  // mark itemFour as checked, highlight itemFive
        checklistPresenter.markItemAsChecked()  // mark itemFive as checked, display checklist complete dialog

        verify(exactly = 1) { mockChecklistView.displayChecklistCompleteDialog() }
    }

    @Test
    fun `should update the Item List Scrolled to the Last Item when Last Item is Highlighted and Mark Item as Checked is Called`() {
        checklistPresenter.presentData()

        checklistPresenter.markItemAsChecked()  // start playing the checklist and highlight itemOne
        checklistPresenter.markItemAsChecked()  // mark itemOne as checked, highlight itemTwo
        checklistPresenter.markItemAsChecked()  // mark itemTwo as checked, highlight itemThree
        checklistPresenter.markItemAsChecked()  // mark itemThree as checked, highlight itemFour
        checklistPresenter.markItemAsChecked()  // mark itemFour as checked, highlight itemFive
        checklistPresenter.markItemAsChecked()  // mark itemFive as checked, display checklist complete dialog

        verify(exactly = 1) { mockChecklistView.updateItemListScrolledToPosition(5) }
    }

    @Test
    fun `should show the Checklist Completed With Skipped Items AlertDialog when Last Item is marked as Skipped`() {
        checklistPresenter.presentData()

        checklistPresenter.markItemAsChecked()  // start playing the checklist and highlight 0 (itemOne)
        checklistPresenter.markItemAsChecked()  // mark 0 (itemOne) as checked, highlight 1 (itemTwo)
        checklistPresenter.markItemAsSkipped()  // mark 1 (itemTwo) as skipped, highlight 2 (itemThree)
        checklistPresenter.markItemAsChecked()  // mark 2 (itemThree) as checked, highlight 3 (itemFour)
        checklistPresenter.markItemAsChecked()  // mark 3 (itemFour) as checked, highlight 4 (itemFive)
        checklistPresenter.markItemAsSkipped()  // mark 4 (itemFive) as skipped, display checklist complete dialog

        verify(exactly = 1) { mockChecklistView.displayChecklistCompleteWithSkippedItemsDialog() }
    }

    @Test
    fun `should show the Checklist Completed With Skipped Items AlertDialog when Last Item is marked as Checked and Skipped items exist`() {
        checklistPresenter.presentData()

        checklistPresenter.markItemAsChecked()  // start playing the checklist and highlight 0 (itemOne)
        checklistPresenter.markItemAsChecked()  // mark 0 (itemOne) as checked
        checklistPresenter.markItemAsSkipped()  // mark 1 (itemTwo) as skipped
        checklistPresenter.markItemAsChecked()  // mark 2 (itemThree) as checked
        checklistPresenter.markItemAsChecked()  // mark 3 (itemFour) as checked
        checklistPresenter.markItemAsChecked()  // mark 4 (itemFive) as checked, display dialog

        verify(exactly = 1) { mockChecklistView.displayChecklistCompleteWithSkippedItemsDialog() }
    }

    @Test
    fun `should Display Buttons for an Inactive Checklist when Resetting a Checklist`() {
        checklistPresenter.presentData()

        checklistPresenter.markItemAsChecked()  // start playing the checklist and highlight itemOne
        checklistPresenter.markItemAsChecked()  // mark itemOne as checked, highlight itemTwo
        checklistPresenter.markItemAsChecked()  // mark itemTwo as checked, display checklist complete dialog

        checklistPresenter.resetChecklistItems()

        verify { mockChecklistView.displayButtonsForInactiveChecklist() }
    }

    @Test
    fun `should Update the List View when Resetting a Checklist`() {
        checklistPresenter.presentData()

        checklistPresenter.markItemAsChecked()  // start playing the checklist and highlight itemOne
        checklistPresenter.markItemAsChecked()  // mark itemOne as checked, highlight itemTwo

        checklistPresenter.resetChecklistItems()

        verify(exactly = 2) { mockChecklistView.updateItemListScrolledToPosition(0) }
    }

    @Test
    fun `should Highlight the First Item when Restarting the Checklist After Resetting it`() {
        checklistPresenter.presentData()

        checklistPresenter.markItemAsChecked()  // start playing the checklist and highlight itemOne
        checklistPresenter.markItemAsChecked()  // mark itemOne as checked, highlight itemTwo

        checklistPresenter.resetChecklistItems()

        checklistPresenter.markItemAsChecked()  // start playing the checklist and highlight itemOne

        verify(exactly = 3) { mockChecklistView.updateItemListScrolledToPosition(0) }
    }

    @Test
    fun `should Update the Options Menu When the Checklist is at Rest`() {
        checklistPresenter.presentData()

        checklistPresenter.adjustOptionsMenu()

        verify(exactly = 1) { mockChecklistView.updateUIForChecklistActiveState(isActive = false, isCompleted = false) }
    }

    @Test
    fun `should Update the Options Menu when the Checklist is Being Played`() {
        checklistPresenter.presentData()
        checklistPresenter.markItemAsChecked()

        checklistPresenter.adjustOptionsMenu()

        verify(exactly = 1) { mockChecklistView.updateUIForChecklistActiveState(isActive = true, isCompleted = false) }
    }

    @Test
    fun `should Update the Options Menu when the Checklist is Completed`() {
        checklistPresenter.presentData()

        checklistPresenter.markItemAsChecked()      // start, highlight 0 (itemOne)
        checklistPresenter.markItemAsChecked()      // check 0 (itemOne), highlight 1 (itemTwo)
        checklistPresenter.markItemAsChecked()      // check 1 (itemTwo), highlight 2 (itemThree)
        checklistPresenter.markItemAsChecked()      // check 2 (itemThree), highlight 3 (itemFour)
        checklistPresenter.markItemAsChecked()      // check 3 (itemFour), highlight 4 (itemFive)
        checklistPresenter.markItemAsChecked()      // check 4 (itemFive), checklist is completed

        checklistPresenter.adjustOptionsMenu()

        verify(exactly = 1) { mockChecklistView.updateUIForChecklistActiveState(isActive = false, isCompleted = true) }
    }

    @Test
    fun `should Display a Message Dialog after Deleting a Checklist Item`() {
        checklistPresenter.presentData()

        checklistPresenter.deleteChecklistItem(itemTwo)

        verify { mockChecklistView.displayDeletedChecklistItemDialog(itemTwo.name) }
        verify { mockRepository.deleteChecklistItem(itemTwo) }
    }

    @Test
    fun `should Delete a Checklist Item from the Checklist`() {
        checklistPresenter.presentData()

        checklistPresenter.deleteChecklistItem(itemTwo)

        val predeleteItems = Arrays.asList<ChecklistItem>(itemOne, itemTwo, itemThree, itemFour, itemFive)

        verify(exactly = 1) { mockChecklistView.updateRecyclerView(predeleteItems) }
        verify(exactly = 1) { mockChecklistView.updateRecyclerView(checklist.items) }
    }

    @Test
    fun `should Start the Next Checklist`() {
        checklistPresenter.presentData()

        checklistPresenter.startNextChecklist()

        verify { mockChecklistView.updateRecyclerView(nextChecklist.items) }
        verify { mockChecklistView.updateToolbarChecklistName(nextChecklist.name) }
    }

    @Test
    fun `should Finish the Fragment when Starting the Next Checklist from the Last Checklist`() {
        checklistPresenter.presentData()

        checklistPresenter.startNextChecklist()    // start nextChecklist
        checklistPresenter.startNextChecklist()

        verify { mockChecklistView.finishChecklist() }
    }

    @Test
    fun `should Present the Next Checklist after Starting the Next Checklist`() {

        checklistPresenter.presentData()

        checklistPresenter.startNextChecklist()

        verify { mockRepository.getChecklist(idOfNextChecklist) }
    }

    @Test
    fun `should Reset the Checklist Buttons after Starting the Next Checklist`() {

        checklistPresenter.presentData()
        checklistPresenter.markItemAsChecked()  // highlight the first item (itemSix)

        checklistPresenter.startNextChecklist()

        verify(exactly = 1) { mockChecklistView.displayButtonsForInactiveChecklist() }
    }

    @Test
    fun `should Disable the 'Go To Previously Skipped' Button when All Items are Prechecked`() {
        checklistPresenter.presentData()
        checklistPresenter.markItemAsChecked()     // start playing the checklist and highlight itemOne

        verify { mockChecklistView.disableGoToPreviouslyCheckedButton() }
        verify(exactly = 0) { mockChecklistView.enableGoToPreviouslyCheckedButton() }
    }

    @Test
    fun `should Enable the 'Go To Previously Skipped' Button when an Item Is Skipped`() {
        checklistPresenter.presentData()
        checklistPresenter.markItemAsChecked()     // start playing the checklist and highlight itemOne
        checklistPresenter.markItemAsSkipped()     // mark itemOne as SKIPPED, highlight itemTwo

        verify { mockChecklistView.enableGoToPreviouslyCheckedButton() }
    }

    @Test
    fun `should Enable the 'Go To Previously Skipped' Button when at least One Item is Skipped`() {
        checklistPresenter.presentData()
        checklistPresenter.markItemAsChecked()     // start playing the checklist and highlight itemOne
        checklistPresenter.markItemAsSkipped()     // mark itemOne as SKIPPED, highlight itemTwo
        checklistPresenter.markItemAsChecked()     // mark itemTwo as checked, highlight itemTwo, display checklist complete dialog

        verify(exactly = 2) { mockChecklistView.enableGoToPreviouslyCheckedButton() }
    }

    @Test
    fun `should Highlight the Last Skipped Item`() {
        checklistPresenter.presentData()
        checklistPresenter.markItemAsChecked()  // start playing the checklist and highlight itemOne
        checklistPresenter.markItemAsChecked()  // mark itemOne as checked, highlight itemTwo
        checklistPresenter.markItemAsChecked()  // mark itemTwo as checked, highlight itemThree
        checklistPresenter.markItemAsChecked()  // mark itemThree as checked, highlight itemFour
        checklistPresenter.markItemAsSkipped()  // mark itemFour as SKIPPED, highlight highlight itemFive
        checklistPresenter.markItemAsChecked()  // mark itemFive as checked, display checklist complete dialog

        checklistPresenter.highlightLastSkippedItem()

        verify(exactly = 2) { mockChecklistView.updateItemListScrolledToPosition(3) }
    }

    @Test
    fun `should active the checklist buttons reHighlighting the Last Skipped Item`() {
        checklistPresenter.presentData()
        checklistPresenter.markItemAsChecked()  // start playing the checklist and highlight itemOne
        checklistPresenter.markItemAsChecked()  // mark itemOne as checked, highlight itemTwo
        checklistPresenter.markItemAsChecked()  // mark itemTwo as checked, highlight itemThree
        checklistPresenter.markItemAsChecked()  // mark itemThree as checked, highlight itemFour
        checklistPresenter.markItemAsSkipped()  // mark itemFour as SKIPPED, highlight highlight itemFive
        checklistPresenter.markItemAsChecked()  // mark itemFive as checked, display checklist complete dialog

        checklistPresenter.highlightLastSkippedItem()

        verify(exactly = 7) { mockChecklistView.activateChecklist() }
    }

    @Test
    fun `should Highlight the First Skipped Item`() {
        checklistPresenter.presentData()
        checklistPresenter.markItemAsChecked()  // start playing the checklist and highlight 0 (itemOne)
        checklistPresenter.markItemAsChecked()  // mark 0 (itemOne) as checked
        checklistPresenter.markItemAsSkipped()  // mark 1 (itemTwo) as SKIPPED
        checklistPresenter.markItemAsChecked()  // mark 2 (itemThree) as checked
        checklistPresenter.markItemAsSkipped()  // mark 3 (itemFour) as SKIPPED
        checklistPresenter.markItemAsChecked()  // mark 4 (itemFive) as checked

        checklistPresenter.highlightFirstSkippedItem()

        verify(exactly = 2) { mockChecklistView.updateItemListScrolledToPosition(1) }
    }

    @Test
    fun `should display the Completed Checklist buttons when the checklist has been completed`() {
        val testChecklist = testChecklist(
                items = listOf(
                        testChecklistItem(state = ChecklistItemState.CHECKED),
                        testChecklistItem(state = ChecklistItemState.CHECKED),
                        testChecklistItem(
                                state = ChecklistItemState.PRECHECKED,
                                type = ChecklistItemType.SECTION_HEADER.toString()
                        ),
                        testChecklistItem(state = ChecklistItemState.CHECKED),
                        testChecklistItem(state = ChecklistItemState.CHECKED)
                )
        )

        val checklistPlayer = ChecklistPlayer()
        checklistPlayer.checklist = testChecklist

        val checklistPresenter = ChecklistPresenter(
                checklistView = mockChecklistView,
                repository = mockRepository,
                checklistId = testChecklist.id,
                checklistPlayer = checklistPlayer
        )

        checklistPresenter.presentData()

        verify { mockChecklistView.updateCompletedChecklistButtons(hasSkippedItems = false) }
        verify { mockChecklistView.disableGoToPreviouslyCheckedButton() }
    }

    @Test
    fun `should display the Completed Checklist With Skipped Items buttons when the checklist has been completed`() {
        val testChecklist = testChecklist(
                items = listOf(
                        testChecklistItem(state = ChecklistItemState.SKIPPED),
                        testChecklistItem(state = ChecklistItemState.CHECKED),
                        testChecklistItem(
                                state = ChecklistItemState.PRECHECKED,
                                type = ChecklistItemType.SECTION_HEADER.toString()
                        ),
                        testChecklistItem(state = ChecklistItemState.SKIPPED),
                        testChecklistItem(state = ChecklistItemState.CHECKED)
                )
        )

        val checklistPlayer = ChecklistPlayer()
        checklistPlayer.checklist = testChecklist

        val checklistPresenter = ChecklistPresenter(
                checklistView = mockChecklistView,
                repository = mockRepository,
                checklistId = testChecklist.id,
                checklistPlayer = checklistPlayer
        )

        checklistPresenter.presentData()

        verify { mockChecklistView.updateCompletedChecklistButtons(hasSkippedItems = true) }
        verify { mockChecklistView.enableGoToPreviouslyCheckedButton() }
    }
}