package org.fossflight.efb

import android.content.Context
import dagger.Module
import dagger.Provides
import io.mockk.mockk
import org.fossflight.efb.data.io.FossFlightFileWriter
import org.fossflight.efb.data.persistence.FFRepository
import javax.inject.Singleton

@Module
class TestAppModule(
        context: Context,
        val repository: FFRepository = mockk(),
        val permissionsManager: PermissionsManager = mockk(),
        val fossFlightFileWriter: FossFlightFileWriter = mockk()
) : AppModule(context) {

    @Provides
    @Singleton
    override fun provideRepository(): FFRepository {
        return this.repository
    }

    @Provides
    @Singleton
    override fun providePermissionsManager(): PermissionsManager {
        return this.permissionsManager
    }

    @Provides
    @Singleton
    override fun provideFossFlightFileWriter(): FossFlightFileWriter {
        return this.fossFlightFileWriter
    }
}
