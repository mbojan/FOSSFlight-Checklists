package org.fossflight.efb

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.RecyclerView


abstract class FossFlightListFragment : Fragment(), FossFlightListView {

    lateinit var recyclerView: RecyclerView

    lateinit var emptyListTextView: TextView

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        super.onCreateView(inflater, container, savedInstanceState)

        val view = inflateView(inflater, container!!)
        recyclerView = view.findViewById(R.id.list)
        emptyListTextView = view.findViewById(R.id.empty)

        applyImages()
        updateToolbar()

        return view
    }

    open fun inflateView(inflater: LayoutInflater, container: ViewGroup): View =
            inflater.inflate(R.layout.list_fragment, container, false)

    override fun updateUIForNonEmptyItemList() {
        emptyListTextView.visibility = View.GONE
    }

    override fun updateUIForEmptyItemList() {
        emptyListTextView.visibility = View.VISIBLE
    }

    open fun applyImages() {}

    abstract fun updateToolbar()
}
