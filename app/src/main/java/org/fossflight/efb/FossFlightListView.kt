package org.fossflight.efb

interface FossFlightListView {
    fun updateUIForNonEmptyItemList()
    fun updateUIForEmptyItemList()
}
