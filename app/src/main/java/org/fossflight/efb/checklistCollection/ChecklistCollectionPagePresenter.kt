package org.fossflight.efb.checklistCollection

import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.functions.Consumer
import io.reactivex.schedulers.Schedulers
import org.fossflight.efb.data.model.Checklist
import org.fossflight.efb.data.persistence.FFRepository

internal class ChecklistCollectionPagePresenter(
        private val checklistCollectionPageView: ChecklistCollectionPageView,
        private val repository: FFRepository,
        private val compositeDisposable: CompositeDisposable = CompositeDisposable(),
        private val checklistCollectionId: Long
) : ChecklistCollectionPageItemListener {

    private lateinit var checklists: MutableList<Checklist>

    fun presentData() {
        compositeDisposable.add(
                repository.getChecklists(checklistCollectionId)
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(Consumer<List<Checklist>> { this.updateChecklistCollection(it) })
        )
    }

    override fun deleteChecklist(checklist: Checklist) {
        checklists.remove(checklist)
        updateChecklistCollection(checklists)
        compositeDisposable.add(
                repository.deleteChecklist(checklist)
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe { checklistCollectionPageView.displayDeletedChecklistDialog(checklist.name) }
        )
    }

    private fun updateChecklistCollection(checklists: List<Checklist>) {
        this.checklists = checklists.toMutableList()
        checklistCollectionPageView.updateRecyclerView(checklists)

        when (this.checklists.size) {
            0 -> checklistCollectionPageView.displayEmptyListMessage()
            else -> checklistCollectionPageView.hideEmptyListMessage()
        }
    }

    override fun startChecklistFragment(checklistId: Long) =
            checklistCollectionPageView.startChecklistFragment(checklistId)

    override fun editChecklist(checklistId: Long) = checklistCollectionPageView.editChecklist(checklistId)

    fun dispose() = compositeDisposable.dispose()
}
