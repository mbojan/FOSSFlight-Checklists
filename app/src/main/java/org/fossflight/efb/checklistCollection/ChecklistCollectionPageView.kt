package org.fossflight.efb.checklistCollection

import org.fossflight.efb.data.model.Checklist

internal interface ChecklistCollectionPageView {
    fun updateRecyclerView(checklists: List<Checklist>)
    fun startChecklistFragment(checklistId: Long)
    fun hideEmptyListMessage()
    fun displayEmptyListMessage()
    fun displayDeletedChecklistDialog(checklistName: String)
    fun editChecklist(checklistId: Long)
}
