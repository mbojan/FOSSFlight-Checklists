package org.fossflight.efb.checklistCollection

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.EditText
import android.widget.Spinner
import com.google.android.material.textfield.TextInputLayout
import org.fossflight.efb.BundleKeys
import org.fossflight.efb.FossFlightApplication
import org.fossflight.efb.FossFlightEditFragment
import org.fossflight.efb.FossFlightFragmentListener
import org.fossflight.efb.R
import org.fossflight.efb.data.persistence.FFRepository
import javax.inject.Inject

class EditChecklistFragment : FossFlightEditFragment(), EditChecklistView {

    private lateinit var checklistNameEditText: EditText
    private lateinit var checklistNameLayout: TextInputLayout
    private lateinit var spinner: Spinner

    @Inject
    lateinit var repository: FFRepository

    private lateinit var editChecklistPresenter: EditChecklistPresenter
    private lateinit var fossFlightFragmentListener: FossFlightFragmentListener

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        (requireActivity().application as FossFlightApplication).appComponent.inject(this)

        val checklistId = requireArguments().getLong(BundleKeys.CHECKLIST_ID, BundleKeys.UNDEFINED)
        val aircraftId = requireArguments().getLong(BundleKeys.AIRCRAFT_ID, BundleKeys.UNDEFINED)

        this.editChecklistPresenter = EditChecklistPresenter(
                editChecklistView = this,
                repository = repository,
                aircraftId = aircraftId,
                checklistId = checklistId
        )
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        checklistNameEditText = view.findViewById(R.id.checklist_name_edit)
        checklistNameLayout = view.findViewById(R.id.input_layout_name)
        spinner = view.findViewById(R.id.checklist_type_spinner)
    }

    override fun inflateView(inflater: LayoutInflater, container: ViewGroup): View =
            inflater.inflate(R.layout.edit_checklist, container, false)

    override fun onStart() {
        super.onStart()
        editChecklistPresenter.presentData()
    }

    override fun onDestroy() {
        super.onDestroy()
        editChecklistPresenter.dispose()
    }

    override fun setParentActivity(listener: FossFlightFragmentListener) {
        super.setParentActivity(listener)
        fossFlightFragmentListener = listener
    }

    override fun updateToolbarInfoForAdd(tailNumber: String) {
        fossFlightFragmentListener.updateToolbarTitle(resources.getString(R.string.add_checklist))
        fossFlightFragmentListener.updateToolbarSubtitle(tailNumber)
    }

    override fun updateToolbarInfoForUpdate(tailNumber: String) {
        fossFlightFragmentListener.updateToolbarTitle(resources.getString(R.string.edit_checklist))
        fossFlightFragmentListener.updateToolbarSubtitle(tailNumber)
    }

    override fun updateChecklistName(checklistName: String) = checklistNameEditText.setText(checklistName)

    override fun updateCollectionNames(types: List<String>) {
        val adapter = ArrayAdapter(requireActivity(), android.R.layout.simple_spinner_item, types)
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spinner.adapter = adapter
    }

    override fun updateSelectedCollectionIndex(selectionIndex: Int) = spinner.setSelection(selectionIndex)

    override fun save() {
        val name = checklistNameEditText.text.toString()
        val type = spinner.selectedItem.toString()
        editChecklistPresenter.saveChecklist(name, type)
    }

    override fun displayNameRequiredViolation() {
        val errorMessage = getString(R.string.name_required_checklist_error_message)
        fossFlightFragmentListener.displayToast(errorMessage)
        checklistNameLayout.error = errorMessage
    }
}

fun newAddChecklistFragment(aircraftId: Long): EditChecklistFragment {
    val editChecklistFragment = EditChecklistFragment()
    val arguments = Bundle()
    arguments.putLong(BundleKeys.AIRCRAFT_ID, aircraftId)
    arguments.putLong(BundleKeys.CHECKLIST_ID, BundleKeys.UNDEFINED)
    editChecklistFragment.arguments = arguments
    return editChecklistFragment
}

fun newEditChecklistFragment(checklistId: Long): EditChecklistFragment {
    val editChecklistFragment = EditChecklistFragment()
    val arguments = Bundle()
    arguments.putLong(BundleKeys.AIRCRAFT_ID, BundleKeys.UNDEFINED)
    arguments.putLong(BundleKeys.CHECKLIST_ID, checklistId)
    editChecklistFragment.arguments = arguments
    return editChecklistFragment
}
