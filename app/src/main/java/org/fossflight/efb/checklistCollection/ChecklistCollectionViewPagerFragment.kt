package org.fossflight.efb.checklistCollection

import android.os.Bundle
import android.view.LayoutInflater
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import androidx.fragment.app.Fragment
import androidx.viewpager.widget.PagerAdapter
import androidx.viewpager.widget.ViewPager
import org.fossflight.efb.BundleKeys
import org.fossflight.efb.FossFlightApplication
import org.fossflight.efb.R
import org.fossflight.efb.data.model.ChecklistCollection
import org.fossflight.efb.data.persistence.FFRepository
import javax.inject.Inject

class ChecklistCollectionViewPagerFragment : Fragment(), ChecklistCollectionViewPagerView {

    @Inject
    lateinit var repository: FFRepository

    private lateinit var checklistCollectionViewPagerPresenter: ChecklistCollectionViewPagerPresenter
    private lateinit var checklistCollectionViewPagerFragmentListener: ChecklistCollectionViewPagerFragmentListener
    private lateinit var adapter: ChecklistCollectionViewPagerViewAdapter

    private var aircraftId: Long = BundleKeys.UNDEFINED

    private lateinit var pager: ViewPager

    override fun onCreate(arguments: Bundle?) {
        super.onCreate(arguments)
        (requireActivity().application as FossFlightApplication).appComponent.inject(this)
        activity?.window?.addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON)

        this.aircraftId = getArguments()?.getLong(BundleKeys.AIRCRAFT_ID) ?: BundleKeys.UNDEFINED
        checklistCollectionViewPagerPresenter = ChecklistCollectionViewPagerPresenter(
                checklistCollectionViewPagerView = this,
                ffRepository = repository,
                aircraftId = aircraftId
        )
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.checklist_collection_options, menu)
        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.pager_fragment, container, false)

        setHasOptionsMenu(true)

        pager = view.findViewById(R.id.pager_fragment)
        adapter = ChecklistCollectionViewPagerViewAdapter(childFragmentManager)
        pager.adapter = adapter

        return view
    }

    fun setParentActivity(listener: ChecklistCollectionViewPagerFragmentListener) {
        this.checklistCollectionViewPagerFragmentListener = listener
    }

    override fun onAttachFragment(childFragment: Fragment) {
        super.onAttachFragment(childFragment)
        if (childFragment is ChecklistCollectionPageFragment)
            childFragment.setParentActivity(this.checklistCollectionViewPagerFragmentListener)
    }

    override fun onResume() {
        super.onResume()
        checklistCollectionViewPagerPresenter.presentData()
    }

    override fun onDestroy() {
        super.onDestroy()
        checklistCollectionViewPagerPresenter.dispose()
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.add_checklist -> addChecklistsClick()
            android.R.id.home -> checklistCollectionViewPagerFragmentListener.finishFragment()
        }

        return super.onOptionsItemSelected(item)
    }

    private fun addChecklistsClick() =
        checklistCollectionViewPagerFragmentListener.startAddChecklistFragment(aircraftId)


    override fun updateToolbarTailNumber(tailNumber: String) {
        checklistCollectionViewPagerFragmentListener.updateToolbarTitle(getString(R.string.all_checklists))
        checklistCollectionViewPagerFragmentListener.updateToolbarSubtitle(tailNumber)
    }

    override fun setPageAdapterTabs(checklistCollections: List<ChecklistCollection>) {
        adapter.setCollections(checklistCollections)
        (pager.adapter as PagerAdapter).notifyDataSetChanged()
    }
}

fun newChecklistCollectionViewPagerFragment(aircraftId: Long): ChecklistCollectionViewPagerFragment {
    val arguments = Bundle()
    arguments.putLong(BundleKeys.AIRCRAFT_ID, aircraftId)

    val fragment = ChecklistCollectionViewPagerFragment()
    fragment.arguments = arguments
    return fragment
}
