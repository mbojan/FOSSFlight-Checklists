package org.fossflight.efb.checklistCollection

import org.fossflight.efb.data.model.ChecklistCollection

internal interface ChecklistCollectionViewPagerView {
    fun setPageAdapterTabs(checklistCollections: List<ChecklistCollection>)
    fun updateToolbarTailNumber(tailNumber: String)
}