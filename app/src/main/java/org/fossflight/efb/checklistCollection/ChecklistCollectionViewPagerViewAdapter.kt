package org.fossflight.efb.checklistCollection

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentStatePagerAdapter
import org.fossflight.efb.data.model.ChecklistCollection

class ChecklistCollectionViewPagerViewAdapter
    internal constructor(
            fragmentManager: FragmentManager
            ) : FragmentStatePagerAdapter(fragmentManager) {

    private var checklistCollections: List<ChecklistCollection> = listOf()

    fun setCollections(collections: List<ChecklistCollection>) {
        this.checklistCollections = collections
        this.notifyDataSetChanged()
    }

    override fun getItem(position: Int): Fragment =
            newChecklistCollectionPageFragment(checklistCollections[position].id)

    override fun getCount(): Int = checklistCollections.size

    override fun getPageTitle(position: Int): CharSequence? = checklistCollections[position].name

    override fun getItemPosition(ignored: Any): Int {
        return POSITION_NONE
    }
}
