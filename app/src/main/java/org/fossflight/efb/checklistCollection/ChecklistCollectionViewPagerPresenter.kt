package org.fossflight.efb.checklistCollection

import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.functions.Consumer
import io.reactivex.schedulers.Schedulers
import org.fossflight.efb.BundleKeys
import org.fossflight.efb.data.model.ChecklistCollection
import org.fossflight.efb.data.persistence.FFRepository

class ChecklistCollectionViewPagerPresenter internal constructor(
        private val checklistCollectionViewPagerView: ChecklistCollectionViewPagerView,
        private val ffRepository: FFRepository,
        private val compositeDisposable: CompositeDisposable = CompositeDisposable(),
        private val aircraftId: Long = BundleKeys.UNDEFINED) {

    fun presentData() {
        compositeDisposable.add(
                ffRepository.getChecklistCollections(aircraftId)
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(
                                Consumer<List<ChecklistCollection>> {
                                    this.updateChecklistCollections(it)
                                }
                        )
        )

        compositeDisposable.add(
                ffRepository.getAircraftTailNumber(aircraftId)
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(
                                Consumer<String> {
                                    checklistCollectionViewPagerView.updateToolbarTailNumber(it)
                                }
                        )
        )
    }

    private fun updateChecklistCollections(checklistCollections: List<ChecklistCollection>) {
        val checklistCollectionsToSet = when {
            checklistCollections.isEmpty() -> {
                listOf(
                        ChecklistCollection(
                                aircraftId = aircraftId,
                                name = BundleKeys.STANDARD_CHECKLIST_COLLECTION_NAME
                        )
                )
            }
            else -> checklistCollections
        }
        checklistCollectionViewPagerView.setPageAdapterTabs(checklistCollectionsToSet)
    }

    fun dispose() = compositeDisposable.dispose()
}
