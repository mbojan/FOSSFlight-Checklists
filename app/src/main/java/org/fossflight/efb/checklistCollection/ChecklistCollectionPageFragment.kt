package org.fossflight.efb.checklistCollection

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.RecyclerView
import org.fossflight.efb.BundleKeys
import org.fossflight.efb.FossFlightApplication
import org.fossflight.efb.R
import org.fossflight.efb.data.model.Checklist
import org.fossflight.efb.data.persistence.FFRepository
import javax.inject.Inject

class ChecklistCollectionPageFragment : Fragment(), ChecklistCollectionPageView {

    private lateinit var emptyListTextView: TextView

    @Inject
    lateinit var repository: FFRepository

    private var checklistCollectionId: Long = BundleKeys.UNDEFINED

    private lateinit var fragmentListener: ChecklistCollectionViewPagerFragmentListener
    private lateinit var presenter: ChecklistCollectionPagePresenter
    private lateinit var adapter: ChecklistCollectionPageViewAdapter


    override fun onCreate(arguments: Bundle?) {
        super.onCreate(arguments)
        setRetainInstance(true);
        (requireActivity().application as FossFlightApplication).appComponent.inject(this)

        checklistCollectionId = getArguments()?.getLong(BundleKeys.CHECKLIST_COLLECTION_ID) ?: BundleKeys.UNDEFINED

        presenter = ChecklistCollectionPagePresenter(
                        checklistCollectionPageView = this,
                        repository = repository,
                        checklistCollectionId = checklistCollectionId
                )
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.list_fragment, container, false)
        emptyListTextView = view.findViewById(R.id.empty)

        adapter = ChecklistCollectionPageViewAdapter(presenter)
        view.findViewById<RecyclerView>(R.id.list).adapter = adapter

        return view
    }

    fun setParentActivity(listener: ChecklistCollectionViewPagerFragmentListener) {
        this.fragmentListener = listener
    }

    override fun onResume() {
        super.onResume()
        presenter.presentData()
    }

    override fun onDestroy() {
        super.onDestroy()
        presenter.dispose()
    }

    override fun updateRecyclerView(checklists: List<Checklist>) {
        adapter.setChecklists(checklists)
        adapter.notifyDataSetChanged()
    }

    override fun hideEmptyListMessage() {
        emptyListTextView.visibility = View.GONE
    }

    override fun displayEmptyListMessage() {
        emptyListTextView.visibility = View.VISIBLE
        emptyListTextView.setText(R.string.empty_checklist_collecgtion_message)
    }

    override fun displayDeletedChecklistDialog(checklistName: String) {
        val message = getString(R.string.deleted_checklist_template, checklistName)
        fragmentListener.displaySnackbar(message)
    }

    override fun startChecklistFragment(checklistId: Long) =
            fragmentListener.startChecklistFragment(checklistId)

    override fun editChecklist(checklistId: Long) =
        fragmentListener.startEditChecklistFragment(checklistId)
}

fun newChecklistCollectionPageFragment(checklistCollectionId: Long): ChecklistCollectionPageFragment {
    val arguments = Bundle()
    arguments.putLong(BundleKeys.CHECKLIST_COLLECTION_ID, checklistCollectionId)
    val fragment = ChecklistCollectionPageFragment()
    fragment.arguments = arguments
    return fragment
}
