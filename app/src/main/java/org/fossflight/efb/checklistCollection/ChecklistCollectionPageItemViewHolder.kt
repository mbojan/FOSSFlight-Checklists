package org.fossflight.efb.checklistCollection

import android.view.View
import android.widget.PopupMenu
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView

import org.fossflight.efb.R
import org.fossflight.efb.data.model.Checklist

internal class ChecklistCollectionPageItemViewHolder(
        view: View,
        private val checklistCollectionPageItemListener: ChecklistCollectionPageItemListener
) : RecyclerView.ViewHolder(view), View.OnClickListener, View.OnLongClickListener {

    private val contentView: TextView = view.findViewById(R.id.content)
    private val shortDescriptionView: TextView = view.findViewById(R.id.short_description)
    private lateinit var checklist: Checklist

    init {
        shortDescriptionView.visibility = View.GONE
        view.setOnClickListener(this)
        view.setOnLongClickListener(this)
    }

    fun bind(checklist: Checklist) {
        this.checklist = checklist
        contentView.text = checklist.name
    }

    override fun toString(): String = "${super.toString()} '${contentView.text}'"

    override fun onClick(view: View) =
        checklistCollectionPageItemListener.startChecklistFragment(this.checklist.id)

    override fun onLongClick(view: View): Boolean {
        val popupMenu = PopupMenu(view.context, view)
        popupMenu.inflate(R.menu.item_card_context)
        popupMenu.setOnMenuItemClickListener { menuItem ->
            when (menuItem.itemId) {
                R.id.delete_item -> {
                    checklistCollectionPageItemListener.deleteChecklist(checklist)
                    true
                }
                R.id.edit_item -> {
                    checklistCollectionPageItemListener.editChecklist(checklist.id)
                    true
                }
                else -> false
            }
        }
        popupMenu.show()
        return true
    }
}
