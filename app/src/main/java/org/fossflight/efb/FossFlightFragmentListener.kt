package org.fossflight.efb

interface FossFlightFragmentListener {
    fun displayAlertDialog(messageId: Int)
    fun displayToast(message: String)
    fun updateToolbarTitle(title: String)
    fun updateToolbarSubtitle(subtitle: String)
    fun finishFragment()
}
