package org.fossflight.efb

import android.Manifest
import android.app.Activity
import android.content.pm.PackageManager
import android.os.Environment
import androidx.core.app.ActivityCompat
import org.fossflight.efb.RequestCodes.WRITE_EXTERNAL_STORAGE
import java.io.File

@Mockable
class PermissionsManager {
    fun hasWritePermission(activity: Activity): Boolean =
        ActivityCompat.checkSelfPermission(activity, Manifest.permission.WRITE_EXTERNAL_STORAGE) ==
                PackageManager.PERMISSION_GRANTED

    fun requestWritePermission(activity: Activity) =
        ActivityCompat.requestPermissions(activity, arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE), WRITE_EXTERNAL_STORAGE)

    fun canWriteToStorage(): Boolean =
            Environment.MEDIA_MOUNTED == Environment.getExternalStorageState()

    fun getWritableDirectory(directory: String): File = Environment.getExternalStoragePublicDirectory(directory)
}
