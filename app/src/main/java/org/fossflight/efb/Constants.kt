package org.fossflight.efb

object BundleKeys {
    const val UNDEFINED = 0L

    const val AIRCRAFT_ID = "aircraft_id"
    const val CHECKLIST_COLLECTION_ID = "checklist_collection_id"
    const val CHECKLIST_ID = "checklist_id"
    const val CHECKLIST_ITEM_ID = "checklist_item_id"

    const val CHECKLIST_TYPE = "checklist_type"
    const val STANDARD_CHECKLIST_COLLECTION_NAME = "Standard Checklist"
    const val EMERGENCY_CHECKLIST_COLLECTION_NAME = "Emergency Checklist"

    const val TITLE = "title"
    const val TAIL_NUMBER = "tail_number"

    const val CHECKLIST = "checklist"
}

object RequestCodes {
    const val WRITE_EXTERNAL_STORAGE = 42
    const val GET_CSV = 3
}

object Parser {
    const val FOSSFLIGHT_CSV_REGEX_STRING = ",(?=(?:[^\\\"]*\\\"[^\\\"]*\\\")*[^\\\"]*\$)"
    const val FLTPLANGO_CSV_TOKEN_DELIMITER = "\",\""
}