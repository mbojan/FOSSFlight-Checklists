package org.fossflight.efb

interface FossFlightEditView {
    fun saveSuccessful(name: String)
    fun abortEdit()
}
