package org.fossflight.efb.aircraftList

import org.fossflight.efb.FossFlightListView
import org.fossflight.efb.data.model.Aircraft

interface AircraftListView : FossFlightListView {
    fun updateRecyclerView(aircrafts: List<Aircraft>)
    fun startChecklistCollectionFragment(aircraftId: Long)
    fun editAircraft(aircraftId: Long)
    fun displayImportProblemDialog()
    fun displayExportAircraftDialog(aircraftId: Long, tailNumber: String)
    fun displayDeletedAircraftDialog(tailNumber: String)
}
