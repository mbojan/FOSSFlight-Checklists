package org.fossflight.efb.aircraftList

import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.functions.Consumer
import io.reactivex.schedulers.Schedulers
import org.fossflight.efb.BundleKeys
import org.fossflight.efb.BundleKeys.EMERGENCY_CHECKLIST_COLLECTION_NAME
import org.fossflight.efb.BundleKeys.STANDARD_CHECKLIST_COLLECTION_NAME
import org.fossflight.efb.data.model.Aircraft
import org.fossflight.efb.data.model.ChecklistCollection
import org.fossflight.efb.data.persistence.FFRepository

internal class EditAircraftPresenter(
        private val editAircraftView: EditAircraftView,
        private val repository: FFRepository,
        private val compositeDisposable: CompositeDisposable = CompositeDisposable(),
        private val aircraftId: Long) {

    private lateinit var editAction: EditAction

    fun saveAircraft(
            tailNumber: String,
            manufacturer: String = "",
            model: String = "",
            year: String = ""
    ) {
        if (tailNumber.trim().isEmpty()) editAircraftView.displayTailNumberRequiredViolation()
        else {
            when (editAction) {
                EditAction.ADD -> {
                    val defaultChecklistCollections = listOf(
                            ChecklistCollection(
                                    name = STANDARD_CHECKLIST_COLLECTION_NAME,
                                    orderNumber = 0
                            ),
                            ChecklistCollection(
                                    name = EMERGENCY_CHECKLIST_COLLECTION_NAME,
                                    orderNumber = 1
                            )
                    )

                    compositeDisposable.add(
                            repository.saveAircraft(
                                    Aircraft(
                                            tailNumber = tailNumber,
                                            manufacturer = manufacturer,
                                            model = model,
                                            year = year.toIntOrNull() ?: 0,
                                            checklistCollections = defaultChecklistCollections
                                    )
                            ).subscribeOn(Schedulers.io())
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribe { editAircraftView.saveSuccessful(tailNumber) }
                    )
                }
                EditAction.UPDATE -> {
                    val aircraftToUpdate = Aircraft(
                            id = aircraftId,
                            tailNumber = tailNumber,
                            manufacturer = manufacturer,
                            model = model,
                            year = year.toIntOrNull() ?: 0
                    )
                    compositeDisposable.add(
                            repository.updateAircraft(
                                    aircraftToUpdate
                            ).subscribeOn(Schedulers.io())
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribe { editAircraftView.saveSuccessful(tailNumber) }
                    )
                }
            }
        }
    }

    fun presentData() {
        when (aircraftId) {
            BundleKeys.UNDEFINED -> {
                editAction = EditAction.ADD
                updateAircraftInformation()
            }
            else -> {
                editAction = EditAction.UPDATE
                compositeDisposable.add(
                        repository.getShallowAircraft(aircraftId)
                                .subscribeOn(Schedulers.io())
                                .observeOn(AndroidSchedulers.mainThread())
                                .subscribe(Consumer<Aircraft> { this.updateAircraftInformation(it) })
                )
            }
        }
    }

    private fun updateAircraftInformation(aircraft: Aircraft = Aircraft()) {
        when (editAction) {
            EditAction.ADD -> editAircraftView.updateToolbarForAdd()
            EditAction.UPDATE -> editAircraftView.updateToolbarForUpdate()
        }
        editAircraftView.updateAircraftInfo(aircraft)
    }

    fun dispose() = compositeDisposable.dispose()
}

enum class EditAction { ADD, UPDATE }
