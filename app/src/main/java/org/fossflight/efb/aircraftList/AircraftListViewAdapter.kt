package org.fossflight.efb.aircraftList


import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import org.fossflight.efb.R
import org.fossflight.efb.data.model.Aircraft

internal class AircraftListViewAdapter(
        private val aircraftListListener: AircraftListItemListener
) : RecyclerView.Adapter<AircraftListItemViewHolder>() {

    private var aircraftList: List<Aircraft> = listOf()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AircraftListItemViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.listable_card, parent, false)
        return AircraftListItemViewHolder(view, aircraftListListener)
    }

    override fun onBindViewHolder(holder: AircraftListItemViewHolder, position: Int) {
        val aircraft = aircraftList[position]
        holder.bind(aircraft)
    }

    override fun getItemCount() = aircraftList.size

    fun setAircrafts(aircrafts: List<Aircraft>) {
        this.aircraftList = aircrafts
    }
}
