package org.fossflight.efb.aircraftList

import android.view.Menu
import android.view.View
import android.widget.PopupMenu
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView

import org.fossflight.efb.R
import org.fossflight.efb.data.model.Aircraft

internal class AircraftListItemViewHolder(
        view: View,
        private val aircraftListItemListener: AircraftListItemListener
) :
        RecyclerView.ViewHolder(view),
        View.OnClickListener,
        View.OnLongClickListener {

    private val contentView: TextView
    private val shortDescriptionView: TextView
    private var aircraft = Aircraft()

    init {
        this.contentView = view.findViewById(R.id.content)
        this.shortDescriptionView = view.findViewById(R.id.short_description)
        view.setOnClickListener(this)
        view.setOnLongClickListener(this)
    }

    override fun toString(): String = "${javaClass.simpleName} - '${aircraft.tailNumber}'"

    fun bind(aircraft: Aircraft) {
        this.aircraft = aircraft
        this.contentView.text = aircraft.tailNumber
        val yearText = if (aircraft.year == 0) "" else aircraft.year.toString()

        val shortDescriptionString =
                if (yearText.isEmpty() && aircraft.manufacturer.isEmpty() &&  aircraft.model.isEmpty())
                    contentView.context.getString(R.string.aircraft_list_short_description_default)
                else
                    contentView.context.getString(
                            R.string.aircraft_list_short_description_template,
                            yearText, aircraft.manufacturer, aircraft.model
        )
        shortDescriptionView.text = shortDescriptionString.trim()
    }

    override fun onClick(view: View) {
        aircraftListItemListener.startChecklistCollectionFragment(aircraft.id)
    }

    override fun onLongClick(view: View): Boolean {
        val popupMenu = PopupMenu(view.context, view)
        popupMenu.inflate(R.menu.item_card_context)
        popupMenu.setOnMenuItemClickListener { menuItem ->
            when (menuItem.itemId) {
                R.id.delete_item -> aircraftListItemListener.deleteAircraft(aircraft)
                R.id.edit_item -> aircraftListItemListener.editAircraft(aircraft.id)
                R.id.export_aircraft -> aircraftListItemListener.exportAircraft(aircraft.id)
            }
            view.isClickable = false
            true
        }

        popupMenu.menu.add(Menu.NONE, R.id.export_aircraft, Menu.NONE, R.string.export_aircraft)
        popupMenu.show()
        return true
    }
}
