package org.fossflight.efb.aircraftList.dialog

interface ImportAircraftDialogListener {
    fun addAircraft(which: Int)
    fun importChecklistType(which: Int)
}