package org.fossflight.efb.aircraftList.dialog

import android.app.Dialog
import android.os.Bundle
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatDialogFragment

import org.fossflight.efb.BundleKeys
import org.fossflight.efb.R

class ImportAircraftDialogFragment : AppCompatDialogFragment() {

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val title = requireArguments().getString(BundleKeys.TITLE)

        return AlertDialog.Builder(requireActivity(), R.style.alert_dialog)
                .setTitle(title)
                .setIcon(R.drawable.ic_airplane_black)
                .setItems(R.array.add_aircraft_dialog_items) { _, which ->
                    (targetFragment as ImportAircraftDialogListener).addAircraft(which)
                    dismiss()
                }
                .setNegativeButton(R.string.cancel) { _, _ -> dismiss() }
                .create()
    }
}

fun newImportAircraftDialogFragment(title: String): ImportAircraftDialogFragment {
    val args = Bundle()
    args.putString(BundleKeys.TITLE, title)

    val frag = ImportAircraftDialogFragment()
    frag.arguments = args
    return frag
}
