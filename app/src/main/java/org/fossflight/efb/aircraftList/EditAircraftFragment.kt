package org.fossflight.efb.aircraftList

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import com.google.android.material.textfield.TextInputLayout
import org.fossflight.efb.BundleKeys
import org.fossflight.efb.FossFlightApplication
import org.fossflight.efb.FossFlightEditFragment
import org.fossflight.efb.FossFlightFragmentListener
import org.fossflight.efb.R
import org.fossflight.efb.data.model.Aircraft
import org.fossflight.efb.data.persistence.FFRepository
import javax.inject.Inject

class EditAircraftFragment : FossFlightEditFragment(), EditAircraftView {

    @Inject
    lateinit var repository: FFRepository

    private lateinit var tailNumberEditText: EditText
    private lateinit var tailNumberLayout: TextInputLayout
    private lateinit var manufacturerEditText: EditText
    private lateinit var modelEditText: EditText
    private lateinit var yearEditText: EditText

    private lateinit var editAircraftPresenter: EditAircraftPresenter
    private lateinit var fossFlightFragmentListener: FossFlightFragmentListener

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        (requireActivity().application as FossFlightApplication).appComponent.inject(this)

        val aircraftId = arguments?.getLong(BundleKeys.AIRCRAFT_ID) ?: BundleKeys.UNDEFINED
        this.editAircraftPresenter = EditAircraftPresenter(
                editAircraftView = this,
                repository = repository,
                aircraftId = aircraftId
        )
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        tailNumberEditText = view.findViewById(R.id.aircraft_tail_number_edit)
        tailNumberLayout = view.findViewById(R.id.input_layout_aircraft_tail_number)
        manufacturerEditText = view.findViewById(R.id.aircraft_manufacturer_edit)
        modelEditText = view.findViewById(R.id.aircraft_model_edit)
        yearEditText = view.findViewById(R.id.aircraft_year_edit)
    }

    override fun inflateView(inflater: LayoutInflater, container: ViewGroup): View {
        return inflater.inflate(R.layout.edit_aircraft, container, false)
    }

    override fun onStart() {
        super.onStart()
        editAircraftPresenter.presentData()
    }

    override fun setParentActivity(listener: FossFlightFragmentListener) {
        super.setParentActivity(listener)
        fossFlightFragmentListener = listener
    }

    override fun onDestroy() {
        super.onDestroy()
        editAircraftPresenter.dispose()
    }

    override fun updateToolbarForAdd() {
        fossFlightFragmentListener.updateToolbarTitle(resources.getString(R.string.add_aircraft))
        fossFlightFragmentListener.updateToolbarSubtitle("")
    }

    override fun updateToolbarForUpdate() {
        fossFlightFragmentListener.updateToolbarTitle(resources.getString(R.string.edit_aircraft))
        fossFlightFragmentListener.updateToolbarSubtitle("")
    }

    override fun updateAircraftInfo(aircraft: Aircraft) {
        tailNumberEditText.setText(aircraft.tailNumber)
        manufacturerEditText.setText(aircraft.manufacturer)
        modelEditText.setText(aircraft.model)
        val yearText = if (aircraft.year == 0) "" else aircraft.year.toString()
        yearEditText.setText(yearText)

    }

    override fun save() {
        val tailNumber = tailNumberEditText.text.toString()
        val manufacturer = manufacturerEditText.text.toString()
        val model = modelEditText.text.toString()
        val year = yearEditText.text.toString()

        editAircraftPresenter.saveAircraft(
                tailNumber = tailNumber,
                manufacturer = manufacturer,
                model = model,
                year = year
        )
    }

    override fun displayTailNumberRequiredViolation() {
        val errorMessage = getString(R.string.tail_number_required_aircraft_error_message)
        fossFlightFragmentListener.displayToast(errorMessage)
        tailNumberLayout.error = errorMessage
    }
}

fun newEditAircraftFragment(aircraftId: Long): EditAircraftFragment {
    val arguments = Bundle()
    arguments.putLong(BundleKeys.AIRCRAFT_ID, aircraftId)
    val editAircraftFragment = EditAircraftFragment()
    editAircraftFragment.arguments = arguments
    return editAircraftFragment
}
