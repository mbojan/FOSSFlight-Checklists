package org.fossflight.efb.aircraftList.dialog

interface ExportAircraftDialogListener {
    fun exportAircraft(aircraftId: Long)
}
