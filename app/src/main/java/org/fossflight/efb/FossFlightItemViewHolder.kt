package org.fossflight.efb

import android.view.View
import android.widget.PopupMenu
import androidx.annotation.CallSuper
import androidx.recyclerview.widget.RecyclerView
import org.fossflight.efb.checklist.ChecklistItemListener
import org.fossflight.efb.data.model.ChecklistItem

abstract class FossFlightItemViewHolder(
        view: View,
        private val checklistItemListener: ChecklistItemListener
) : RecyclerView.ViewHolder(view), View.OnLongClickListener {

    private lateinit var checklistItem: ChecklistItem

    init {
        view.setOnLongClickListener(this)
    }

    override fun toString(): String = "${javaClass.simpleName} - '${checklistItem.name}'"

    @CallSuper
    open fun bind(checklistItem: ChecklistItem) {
        this.checklistItem = checklistItem
    }

    override fun onLongClick(view: View): Boolean {
        if (checklistItemListener.isEditable()) {
            val popupMenu = PopupMenu(view.context, view)
            popupMenu.inflate(R.menu.item_card_context)
            popupMenu.setOnMenuItemClickListener { menuItem ->
                when (menuItem.itemId) {
                    R.id.delete_item -> checklistItemListener.deleteChecklistItem(checklistItem)
                    R.id.edit_item -> checklistItemListener.editChecklistItem(checklistItem.id)
                }
                true
            }
            popupMenu.show()
        }
        return true
    }
}
