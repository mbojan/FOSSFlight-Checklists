package org.fossflight.efb

import dagger.Component
import org.fossflight.efb.aircraftList.AircraftListFragment
import org.fossflight.efb.aircraftList.EditAircraftFragment
import org.fossflight.efb.checklist.ChecklistFragment
import org.fossflight.efb.checklist.EditChecklistItemFragment
import org.fossflight.efb.checklistCollection.ChecklistCollectionPageFragment
import org.fossflight.efb.checklistCollection.ChecklistCollectionViewPagerFragment
import org.fossflight.efb.checklistCollection.EditChecklistFragment
import javax.inject.Singleton

@Component(modules = arrayOf(AppModule::class))
@Singleton
interface AppComponent {
    fun inject(editChecklistFragment: EditChecklistFragment)
    fun inject(editAircraftFragment: EditAircraftFragment)
    fun inject(aircraftListFragment: AircraftListFragment)
    fun inject(checklistCollectionPageFragment: ChecklistCollectionPageFragment)
    fun inject(checklistCollectionViewPagerFragment: ChecklistCollectionViewPagerFragment)
    fun inject(checklistFragment: ChecklistFragment)
    fun inject(editChecklistItemFragment: EditChecklistItemFragment)
}
