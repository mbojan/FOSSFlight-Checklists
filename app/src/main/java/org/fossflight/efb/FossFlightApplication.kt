package org.fossflight.efb

import android.app.Application

import org.fossflight.efb.data.persistence.FFRepository


open class FossFlightApplication : Application() {

    lateinit var appComponent: AppComponent
        set

    open lateinit var repository: FFRepository
        set

    open var appModule: AppModule
        get() = AppModule(this)
        set(newAppModule) {
            this.appModule = newAppModule
        }


    override fun onCreate() {
        super.onCreate()
        initComponent()
    }

    fun initComponent() {
        appComponent = DaggerAppComponent.builder().appModule(appModule).build()
    }
}
