package org.fossflight.efb.checklist

import org.fossflight.efb.FossFlightFragmentListener

interface ChecklistFragmentListener : FossFlightFragmentListener {
    fun displaySnackbar(message: String)
    fun startEditChecklistItemFragment(checklistId: Long, checklistItemId: Long)
}
