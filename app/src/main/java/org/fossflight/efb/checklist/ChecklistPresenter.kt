package org.fossflight.efb.checklist

import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.functions.Consumer
import io.reactivex.schedulers.Schedulers
import org.fossflight.efb.BundleKeys.UNDEFINED
import org.fossflight.efb.data.model.Checklist
import org.fossflight.efb.data.model.ChecklistItem
import org.fossflight.efb.data.persistence.FFRepository

internal class ChecklistPresenter(
        private val checklistView: ChecklistView,
        private val repository: FFRepository,
        private val compositeDisposable: CompositeDisposable = CompositeDisposable(),
        private var checklistId: Long,
        val checklistPlayer: ChecklistPlayer = ChecklistPlayer()
    ) : ChecklistItemListener {

    val checklist : Checklist
        get() = checklistPlayer.checklist


    fun presentData() {
        when (checklistPlayer.checklist.id) {
            UNDEFINED -> {
                refreshChecklistFromDatabase()
                compositeDisposable.add(
                        repository.getAircraftTailNumberForChecklist(checklistId)
                                .subscribeOn(Schedulers.io())
                                .observeOn(AndroidSchedulers.mainThread())
                                .subscribe(Consumer<String> { checklistView.updateToolbarTailNumber(it) })
                )
            }
            else -> updateChecklistItemData(checklistPlayer.checklist)
        }

    }

    private fun updateChecklistItemData(checklist: Checklist) {
        checklistPlayer.checklist = checklist
        checklistView.updateToolbarChecklistName(checklistPlayer.checklist.name)

        if (checklist.items.isEmpty()) checklistView.updateUIForEmptyItemList()
        else {
            checklistView.updateRecyclerView(checklistPlayer.checklist.items)
            checklistView.updateUIForNonEmptyItemList()

            if (checklistPlayer.isActive()) {
                checklistView.activateChecklist()
                checklistView.updateItemListScrolledToPosition(checklistPlayer.activeItemIndex)
            }

            if (checklistPlayer.hasSkippedItems())
                checklistView.enableGoToPreviouslyCheckedButton()
            else
                checklistView.disableGoToPreviouslyCheckedButton()

            if (checklistPlayer.isCompleted())
                checklistView.updateCompletedChecklistButtons(checklistPlayer.hasSkippedItems())
        }
    }

    fun markItemAsChecked() {
        val position = checklistPlayer.markItemAsChecked()
        updateUIAfterMarkingItem(position)
    }

    fun markItemAsSkipped() {
        val position = checklistPlayer.markItemAsSkipped()
        updateUIAfterMarkingItem(position)
    }

    private fun updateUIAfterMarkingItem(position: Int) {
        checklistView.updateItemListScrolledToPosition(position)

        checklistView.activateChecklist()

        if (checklistPlayer.hasSkippedItems()) checklistView.enableGoToPreviouslyCheckedButton()
        else checklistView.disableGoToPreviouslyCheckedButton()

        if (checklistPlayer.isCompleted()) {
            if (checklistPlayer.hasSkippedItems())
                checklistView.displayChecklistCompleteWithSkippedItemsDialog()
            else
                checklistView.displayChecklistCompleteDialog()
        }
    }

    fun resetChecklistItems() {
        checklistPlayer.resetChecklist()
        checklistView.displayButtonsForInactiveChecklist()
        checklistView.updateItemListScrolledToPosition(0)
    }

    fun adjustOptionsMenu() =
            checklistView.updateUIForChecklistActiveState(
                    checklistPlayer.isActive(),
                    checklistPlayer.isCompleted()
            )

    fun startNextChecklist() {
        val checklistCollectionId = checklistPlayer.checklist.checklistCollectionId
        val nextOrderNumber = checklistPlayer.checklist.orderNumber + 1
        compositeDisposable.add(
                repository.getChecklistIdFromChecklistCollectionId(checklistCollectionId, nextOrderNumber)
                        .onErrorReturnItem(UNDEFINED)
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(Consumer<Long> { this.updateChecklistData(it) })
        )
    }

    private fun updateChecklistData(checklistId: Long) {
        checklistPlayer.resetChecklist()
        if (checklistId == UNDEFINED) checklistView.finishChecklist()
        else {
            this.checklistId = checklistId
            checklistView.displayButtonsForInactiveChecklist()
            refreshChecklistFromDatabase()
        }
    }

    private fun refreshChecklistFromDatabase() {
        compositeDisposable.add(
                repository.getChecklist(this.checklistId)
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(Consumer<Checklist> { this.updateChecklistItemData(it) })
        )
    }

    override fun deleteChecklistItem(checklistItem: ChecklistItem) {
        checklistPlayer.deleteChecklistItem(checklistItem)
        checklistView.updateRecyclerView(checklistPlayer.checklist.items)
        compositeDisposable.add(
                repository.deleteChecklistItem(checklistItem)
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe { checklistView.displayDeletedChecklistItemDialog(checklistItem.name) }
        )

        if (checklistPlayer.checklist.items.isEmpty()) checklistView.updateUIForEmptyItemList()
    }

    override fun editChecklistItem(checklistItemId: Long) {
        checklistPlayer.checklist.id = UNDEFINED
        checklistView.editChecklistItem(checklistItemId)
    }

    fun highlightLastSkippedItem() {
        val indexOfLastSkippedItem = checklistPlayer.highlightLastSkippedItem()
        updateUIAfterMarkingItem(indexOfLastSkippedItem)
    }

    fun highlightFirstSkippedItem() {
        val indexOfFirstSkippedItem = checklistPlayer.highlightFirstSkippedItem()
        checklistView.updateItemListScrolledToPosition(indexOfFirstSkippedItem)
    }

    override fun isEditable(): Boolean = !checklistPlayer.isActive()

    fun dispose() = compositeDisposable.dispose()
}
