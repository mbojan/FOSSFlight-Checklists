package org.fossflight.efb.data.io

import org.fossflight.efb.Parser.FOSSFLIGHT_CSV_REGEX_STRING
import org.fossflight.efb.data.model.Aircraft
import org.fossflight.efb.data.model.Checklist
import org.fossflight.efb.data.model.ChecklistCollection
import org.fossflight.efb.data.model.ChecklistItem
import org.fossflight.efb.data.model.ChecklistItemType
import java.util.*

class FossFlightCSVParser : ChecklistParser {

    private val checklistIndicator = " checklist"
    private val sectionHeaderIndicator = "-----"

    override fun parse(fileContents: List<String>): Aircraft {

        val cleanFileContents = fileContents.map { it.trimEnd(',') }

        val tailNumber = parseAircraftDetail(cleanFileContents, "TailNumber")
        val manufacturer = parseAircraftDetail(cleanFileContents, "AircraftManufacturer")
        val model = parseAircraftDetail(cleanFileContents, "AircraftModel")
        val year = parseAircraftDetail(cleanFileContents, "AircraftYear").toIntOrNull() ?: 0

        val allSectionHeaders = cleanFileContents
                .mapIndexed { i, line -> Pair(i, tokenize(line)) }
                .filter { isChecklistType(it.second) }

        val checklistChunks = allSectionHeaders
                .mapIndexed { i, currentSectionHeader ->
                    val lastIndexOfChecklistLines = if (i == allSectionHeaders.size - 1) cleanFileContents.size else allSectionHeaders[i + 1].first
                    cleanFileContents.subList(currentSectionHeader.first, lastIndexOfChecklistLines)
                }
                .filter { lines ->
                    val line = lines[0]
                    val tokens = tokenize(line)
                    isChecklistType(tokens)
                }

        val emptyCollections = checklistChunks
                .map { checklistLines ->
                    val tokens = tokenize(checklistLines[0])
                    ChecklistCollection(name = tokens[1])
                }
                .distinct()
                .mapIndexed { i, collection ->
                    collection.copy(orderNumber = i.toLong())
                }

        val unsortedChecklists = checklistChunks
                .mapIndexed { i, checklistLines ->
                    val tokens = tokenize(checklistLines[0])
                    val name = tokens[0]
                    val collectionName = tokens[1]
                    val itemLines = checklistLines.subList(1, checklistLines.size)
                    val items = collectChecklistItems(itemLines)
                    val checklist = Checklist(name = name, items = items, orderNumber = i.toLong())
                    Pair(collectionName, checklist)
                }

        val populatedCollections = emptyCollections
                .map { collection ->
                    val checklists = unsortedChecklists.filter { it.first == collection.name }.map { it.second }
                    collection.copy(checklists = checklists)
                }

        return Aircraft(
                tailNumber = tailNumber,
                manufacturer = manufacturer,
                model = model,
                year = year,
                checklistCollections = populatedCollections)
    }

    private fun parseAircraftDetail(fileContents: List<String>, tailNumberIndicator: String): String {
        val line = fileContents.filter { it.endsWith(tailNumberIndicator) }
        return if (line.isNotEmpty()) line.map { tokenize(it)[0] }.first()
               else ""
    }

    private fun collectChecklistItems(lines: List<String>): List<ChecklistItem> {
        val items = mutableListOf<ChecklistItem>()
        for (line in lines) {
            val tokens = tokenize(line)
            if (isChecklistItem(tokens)) {
                val item = ChecklistItem(
                        name = tokens[0],
                        action = tokens[1],
                        type = getChecklistItemType(tokens[1]).toString()
                )
                items.add(item)

            } else {
                break
            }
        }
        return items
    }

    private fun tokenize(line: String): List<String> {
        return line.split(FOSSFLIGHT_CSV_REGEX_STRING.toRegex())
                .dropLastWhile { it.isEmpty() }
                .map { token ->
                    var cleanedToken = token
                    if (token.startsWith("\"")) cleanedToken = token.drop(1)
                    if (token.endsWith("\"")) cleanedToken = cleanedToken.dropLast(1)
                    cleanedToken.trim()
                }
    }

    private fun getChecklistItemType(action: String): ChecklistItemType {
        return if (action.startsWith(sectionHeaderIndicator)) ChecklistItemType.SECTION_HEADER
               else ChecklistItemType.NORMAL
    }

    private fun isChecklistItem(tokens: List<String>): Boolean = tokens.size >= 2 && !isChecklistType(tokens)

    private fun isChecklistType(tokens: List<String>): Boolean =
            tokens.size == 2 && tokens[1].toLowerCase(Locale.ENGLISH).endsWith(checklistIndicator)
}
