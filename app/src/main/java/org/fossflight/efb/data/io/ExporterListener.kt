package org.fossflight.efb.data.io

interface ExporterListener {
    fun displayWriteProblems()
}
