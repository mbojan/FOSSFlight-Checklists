package org.fossflight.efb.data.persistence

import io.reactivex.Completable
import io.reactivex.Flowable
import io.reactivex.Observable
import io.reactivex.Single
import io.reactivex.functions.BiFunction
import org.fossflight.efb.data.model.Aircraft
import org.fossflight.efb.data.model.Checklist
import org.fossflight.efb.data.model.ChecklistCollection
import org.fossflight.efb.data.model.ChecklistItem
import java.util.*

class FFRepository(private val db: FFDatabase) {

    val allAircraft: Flowable<List<Aircraft>>
        get() {

            val loadedAircraft = ArrayList<Aircraft>()
            val aircraftFlowable = db.aircraftDao.aircraft.toFlowable()
            return aircraftFlowable
                    .concatMap { listofAircraft -> Flowable.fromIterable(listofAircraft) }
                    .map { aircraft ->
                        loadedAircraft.add(aircraft)
                        db.checklistCollectionDao.getChecklistCollections(aircraft.id)
                    }
                    .concatMap(Single<List<ChecklistCollection>>::toFlowable)
                    .map { checklistCollections ->
                        val aircraft = loadedAircraft[loadedAircraft.size - 1]
                        aircraft.checklistCollections = checklistCollections
                        checklistCollections
                    }
                    .concatMap { _ -> Flowable.just<List<Aircraft>>(loadedAircraft) }
        }

    fun insertAircraft(aircraft: Aircraft): Observable<Aircraft> {
        val collections = aircraft.checklistCollections
        return Observable.fromCallable { db.aircraftDao.insertAircraft(aircraft) }
                .map { aircraftId ->
                    aircraft.id = aircraftId
                    for (collection in collections) {
                        collection.aircraftId = aircraftId
                    }
                    db.checklistCollectionDao.insertChecklistCollections(collections)
                }
                .concatMap { collectionIds ->
                    for (i in collectionIds.indices) {
                        collections[i].id = collectionIds[i]
                    }
                    Observable.fromIterable(collections)
                }
                .concatMap { (id, _, _, _, _, checklists) ->
                    for (checklist in checklists) {
                        checklist.checklistCollectionId = id
                        val checklistId = db.checklistDao.insertChecklist(checklist)
                        checklist.id = checklistId
                    }

                    Observable.fromIterable(checklists)
                }
                .concatMap { (id, _, _, _, items) ->
                    for (item in items) {
                        item.checkListId = id
                    }
                    Observable.fromIterable(items)
                }
                .map { checklistItem ->
                    val itemId = db.checklistItemDao
                            .insertChecklistItem(checklistItem)
                    checklistItem.id = itemId
                    checklistItem
                }
                .map { (_, _, _, _, _, _, _) -> aircraft }
    }

    fun deleteAircraft(aircraft: Aircraft): Completable {
        return Completable.fromAction { db.aircraftDao.deleteAircraft(aircraft) }
    }

    fun getAircraftTailNumberForChecklist(checklistId: Long): Single<String> {
        val aircraftDao = db.aircraftDao
        val checklistCollectionDao = db.checklistCollectionDao
        return db.checklistDao
                .getChecklistCollectionId(checklistId)
                .flatMap(checklistCollectionDao::getAircraftIdForChecklistCollection)
                .flatMap(aircraftDao::getTailNumber)
    }

    fun getAircraftTailNumber(aircraftId: Long): Single<String> {
        return db.aircraftDao.getTailNumber(aircraftId)
    }

    fun saveChecklistItem(checklistItem: ChecklistItem): Completable {
        return Completable.fromCallable { db.checklistItemDao.insertChecklistItem(checklistItem) }
    }

    fun saveChecklist(checklist: Checklist): Completable {
        return Completable.fromCallable { db.checklistDao.insertChecklist(checklist) }
    }

    fun updateChecklist(checklist: Checklist): Completable {
        return Completable.fromCallable {
            db.checklistDao.updateChecklist(
                    checklist.id,
                    checklist.checklistCollectionId,
                    checklist.name,
                    checklist.orderNumber)
        }
    }

    fun saveAircraft(aircraft: Aircraft): Observable<Aircraft> {
        return Observable.fromCallable { db.aircraftDao.insertAircraft(aircraft) }
                .map { aircraftId ->
                    aircraft.id = aircraftId
                    for (collection in aircraft.checklistCollections) {
                        collection.aircraftId = aircraftId
                    }
                    db.checklistCollectionDao.insertChecklistCollections(aircraft.checklistCollections)
                }
                .map { aircraft }
    }

    fun updateAircraft(aircraft: Aircraft): Completable {
        return Completable.fromCallable {
            db.aircraftDao.updateTailNumber(
                    aircraft.id,
                    aircraft.tailNumber,
                    aircraft.manufacturer,
                    aircraft.model,
                    aircraft.year
            )
        }
    }

    fun getChecklist(checklistId: Long): Single<Checklist> {
        val mapItemsToChecklist = BiFunction<List<ChecklistItem>, Checklist, Checklist> { checklistItems, checklist ->
            checklist.items = checklistItems
            checklist
        }

        return db.checklistItemDao.getItemsForChecklist(checklistId)
                .toObservable()
                .zipWith(
                        db.checklistDao.getChecklist(checklistId).toObservable(),
                        mapItemsToChecklist
                )
                .singleOrError()
    }

    fun getShallowAircraft(aircraftId: Long): Single<Aircraft> {
        return db.aircraftDao.getAircraft(aircraftId)
    }

    fun getAircraft(aircraftId: Long): Single<Aircraft> {

        val function = BiFunction<List<ChecklistCollection>, Aircraft, Aircraft> { collections, aircraft ->

                aircraft.checklistCollections = collections
                for (collection in collections) {
                    val checklists = db.checklistDao
                            .getChecklistsForChecklistCollectionId(collection.id)
                    collection.checklists = checklists.blockingGet()

                    for (checklist in collection.checklists) {
                        val itemsForChecklist = db.checklistItemDao.getItemsForChecklist(checklist.id)
                        checklist.items = itemsForChecklist.blockingGet()
                    }
                }
                aircraft
            }


        val checklistCollections = getChecklistCollections(aircraftId)
        return checklistCollections
                .toObservable()
                .zipWith(
                        db.aircraftDao.getAircraft(aircraftId).toObservable(),
                        function
                ).singleOrError()
    }

    fun getChecklistCollections(aircraftId: Long): Single<List<ChecklistCollection>> {
        return db.checklistCollectionDao.getChecklistCollections(aircraftId)
    }

    fun getSiblingChecklistCollections(checklistId: Long): Single<List<ChecklistCollection>> {
        val checklistCollectionDao = db.checklistCollectionDao
        return db.checklistDao
                .getChecklistCollectionId(checklistId)
                .flatMap(checklistCollectionDao::getAircraftIdForChecklistCollection)
                .flatMap(checklistCollectionDao::getChecklistCollections)
    }

    fun getChecklists(checklistCollectionId: Long): Single<List<Checklist>> {
        return db.checklistDao.getChecklistsForChecklistCollectionId(checklistCollectionId)
    }

    fun deleteChecklistItem(checklistItem: ChecklistItem): Completable {
        return Completable.fromAction { db.checklistItemDao.deleteChecklistItem(checklistItem) }
    }

    fun deleteChecklist(checklist: Checklist): Completable {
        return Completable.fromAction { db.checklistDao.deleteChecklist(checklist) }
    }

    fun getChecklistIdFromChecklistCollectionId(checklistCollectionId: Long, orderNumber: Long): Single<Long> {
        return db.checklistDao.getChecklistIdForAircraftWithOrderNumber(checklistCollectionId, orderNumber)
    }

    fun getChecklistItem(checklistItemId: Long): Single<ChecklistItem> {
        return db.checklistItemDao.getChecklistItem(checklistItemId)
    }

    fun saveChecklistCollection(checklistCollection: ChecklistCollection): Single<Long> {
        return Single.fromCallable { db.checklistCollectionDao.insertChecklistCollection(checklistCollection) }
    }

    fun getChecklistOrderNumbersFromCollection(checklistCollectionId: Long): Single<List<Long>> {
        return db.checklistDao.getChecklistOrderNumbersFromCollection(checklistCollectionId)
    }
}
