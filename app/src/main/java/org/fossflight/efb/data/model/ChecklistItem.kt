package org.fossflight.efb.data.model

import android.os.Parcelable
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.ForeignKey
import androidx.room.ForeignKey.CASCADE
import androidx.room.ForeignKey.NO_ACTION
import androidx.room.Ignore
import androidx.room.Index
import androidx.room.PrimaryKey
import kotlinx.android.parcel.Parcelize
import org.fossflight.efb.BundleKeys

@Entity(foreignKeys =
    arrayOf(
            ForeignKey(
                    entity = Checklist::class,
                    onDelete = CASCADE,
                    onUpdate = NO_ACTION,
                    parentColumns = arrayOf("id"),
                    childColumns = arrayOf("checklist_id")
            )
    ),
    indices = arrayOf(
            Index("id"),
            Index("checklist_id")
    )
)
@Parcelize
data class ChecklistItem(
        @PrimaryKey(autoGenerate = true) var id: Long = 0,
        @ColumnInfo(name = "checklist_id") var checkListId: Long = 0,

        var name: String = "",
        var action: String = "",
        var type: String = "NORMAL",
        @ColumnInfo(name = "order_number") var orderNumber: Long = BundleKeys.UNDEFINED,

        @Ignore var state: ChecklistItemState = ChecklistItemState.PRECHECKED
): Parcelable {

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other !is ChecklistItem) return false

        val that = other as ChecklistItem?

        if (id != that!!.id) return false
        if (checkListId != that.checkListId) return false
        if (name != that.name) return false
        return if (type != that.type) false else action == that.action
    }

    override fun hashCode(): Int {
        var result = (id xor id.ushr(32)).toInt()
        result = 31 * result + name.hashCode()
        result = 31 * result + action.hashCode()
        result = 31 * result + (checkListId xor checkListId.ushr(32)).toInt()
        result = 31 * result + type.hashCode()
        return result
    }
}
