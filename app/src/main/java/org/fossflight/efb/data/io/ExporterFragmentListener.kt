package org.fossflight.efb.data.io

interface ExporterFragmentListener {
    fun displayNeedForWritePermissions()
}