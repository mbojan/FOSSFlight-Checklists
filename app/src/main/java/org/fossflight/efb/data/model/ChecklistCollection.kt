package org.fossflight.efb.data.model

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.ForeignKey
import androidx.room.ForeignKey.CASCADE
import androidx.room.ForeignKey.NO_ACTION
import androidx.room.Ignore
import androidx.room.Index
import androidx.room.PrimaryKey
import org.fossflight.efb.BundleKeys

@Entity(foreignKeys =
    arrayOf(
            ForeignKey(
                    entity = Aircraft::class,
                    onDelete = CASCADE,
                    onUpdate = NO_ACTION,
                    parentColumns = arrayOf("id"),
                    childColumns = arrayOf("aircraft_id")
            )
    ),
    indices = arrayOf(
            Index("id"),
            Index("aircraft_id")
    )
)
data class ChecklistCollection(
        @PrimaryKey(autoGenerate = true) var id: Long = BundleKeys.UNDEFINED,
        @ColumnInfo(name = "aircraft_id") var aircraftId: Long = BundleKeys.UNDEFINED,

        var name: String = "A Standard Checklist Collection",
        var type: String = "NORMAL",
        @ColumnInfo(name = "order_number") var orderNumber: Long = BundleKeys.UNDEFINED,

        @Ignore var checklists: List<Checklist> = listOf()
)