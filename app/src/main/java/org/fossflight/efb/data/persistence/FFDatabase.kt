package org.fossflight.efb.data.persistence


import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.migration.Migration
import androidx.sqlite.db.SupportSQLiteDatabase
import org.fossflight.efb.data.model.Aircraft
import org.fossflight.efb.data.model.Checklist
import org.fossflight.efb.data.model.ChecklistCollection
import org.fossflight.efb.data.model.ChecklistItem

@Database(version = 5, entities = arrayOf(Aircraft::class, Checklist::class, ChecklistItem::class, ChecklistCollection::class))
abstract class FFDatabase : RoomDatabase() {
    abstract val aircraftDao: AircraftDao
    abstract val checklistDao: ChecklistDao
    abstract val checklistItemDao: ChecklistItemDao
    abstract val checklistCollectionDao: ChecklistCollectionDao

    object Migrations {
        val MIGRATION_1_2: Migration = object : Migration(1, 2) {
            override fun migrate(database: SupportSQLiteDatabase) {
                val addOrderNumberStatement =
                        "ALTER TABLE Checklist " +
                        "ADD COLUMN order_number INTEGER NOT NULL DEFAULT 999"
                database.execSQL(addOrderNumberStatement)
            }
        }

        val MIGRATION_2_3: Migration = object : Migration(2, 3) {
            override fun migrate(database: SupportSQLiteDatabase) {
                val addOrderNumberStatement =
                        "ALTER TABLE ChecklistItem " +
                        "ADD COLUMN type TEXT NOT NULL DEFAULT 'NORMAL'"
                database.execSQL(addOrderNumberStatement)
            }
        }

        val MIGRATION_3_4: Migration = object : Migration(3, 4) {
            override fun migrate(database: SupportSQLiteDatabase) {
                val createTempAircraftTable = """
                    CREATE TABLE `Aircraft_temp`
                    (
                        `id` INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
                        `tailNumber` TEXT NOT NULL DEFAULT ''
                    );""".trimMargin()

                val populateTempAircraftTable = """
                    INSERT INTO Aircraft_temp
                    (
                        id,
                        tailNumber
                    )
                        SELECT
                            id,
                            tailNumber
                        FROM Aircraft;
                """.trimMargin()

                val dropAircraftTable = """DROP TABLE Aircraft;"""

                val renameTempAircraftTable = """ALTER TABLE Aircraft_temp RENAME TO Aircraft;"""

                val dropAircraftIndex = """DROP INDEX IF EXISTS index_ChecklistItem_id;"""

                val createAircraftIndex = """CREATE INDEX `index_Aircraft_id` ON `Aircraft` (`id`);"""

                val createTempChecklistCollectionTable =
                        """CREATE TABLE `checklist_collection_temp`
                            (
                                `id` INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
                                `aircraft_id` INTEGER NOT NULL,
                                `name` TEXT NOT NULL
                            )""".trimMargin()

                val populateTempChecklistCollectionTable =
                        """INSERT INTO checklist_collection_temp ( aircraft_id, name )
                                SELECT DISTINCT aircraft_id, type
                                FROM checklist""".trimMargin()

                val createChecklistCollectionTable =
                        """CREATE TABLE IF NOT EXISTS `ChecklistCollection`
                            (
                                `id` INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
                                `aircraft_id` INTEGER NOT NULL,
                                `name` TEXT NOT NULL,
                                `type` TEXT NOT NULL,
                                `order_number` INTEGER NOT NULL,
                                FOREIGN KEY(`aircraft_id`) REFERENCES `Aircraft`(`id`)
                                ON UPDATE NO ACTION
                                ON DELETE CASCADE
                            )""".trimMargin()

                val populateChecklistCollectionTable =
                        """INSERT INTO ChecklistCollection ( aircraft_id, name, type, order_number )
                                SELECT aircraft_id, name, "NORMAL", id
                                FROM checklist_collection_temp""".trimMargin()

                val createChecklistCollectionIndex =
                        """CREATE INDEX `index_ChecklistCollection_id` ON `ChecklistCollection` (`id`)"""

                val createChecklistCollectionForeignKeyIndex =
                        """CREATE INDEX `index_ChecklistCollection_aircraft_id`
                            |   ON `ChecklistCollection` (`aircraft_id`)""".trimMargin()

                val dropTempChecklistCollectionTable = """DROP TABLE checklist_collection_temp"""

                val createTempChecklistTable =
                        """CREATE TABLE IF NOT EXISTS `checklist_temp`
                            (
                                `id` INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
                                `checklist_collection_id` INTEGER NOT NULL,
                                `name` TEXT NOT NULL,
                                `order_number` INTEGER NOT NULL,
                                `previous_checklist_id` INTEGER NOT NULL,
                                FOREIGN KEY(`checklist_collection_id`) REFERENCES `ChecklistCollection`(`id`)
                                ON UPDATE NO ACTION
                                ON DELETE CASCADE
                            )""".trimMargin()

                val populateTempChecklistTable =
                        """INSERT INTO `checklist_temp`
                            (
                                checklist_collection_id,
                                name,
                                order_number,
                                previous_checklist_id
                            )
                                SELECT
                                    `ChecklistCollection`.id,
                                    'Checklist'.name,
                                    'Checklist'.order_number,
                                    'Checklist'.id
                                FROM `Checklist`
                                INNER JOIN `ChecklistCollection` ON `Checklist`.type = `ChecklistCollection`.name
                                                                 AND `Checklist`.aircraft_id = `ChecklistCollection`.aircraft_id
                                """.trimMargin()

                val dropChecklistIdIndex = """DROP INDEX IF EXISTS `index_Checklist_id`"""
                val dropChecklistForeignKeyIndex = """DROP INDEX IF EXISTS `index_Checklist_aircraft_id`"""
                val dropChecklistTable = """DROP TABLE `Checklist`"""

                val createNewChecklistTable =
                        """CREATE TABLE IF NOT EXISTS `Checklist`
                            (
                                `id` INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
                                `checklist_collection_id` INTEGER NOT NULL,
                                `name` TEXT NOT NULL,
                                `order_number` INTEGER NOT NULL,
                                FOREIGN KEY(`checklist_collection_id`) REFERENCES `ChecklistCollection`(`id`)
                                ON UPDATE NO ACTION
                                ON DELETE CASCADE
                            )""".trimMargin()

                val populateNewChecklistTable = """
                    INSERT INTO `Checklist`
                    (
                        checklist_collection_id,
                        name,
                        order_number
                    )
                        SELECT
                            checklist_collection_id,
                            name,
                            order_number
                        FROM `checklist_temp`
                        """.trimIndent()

                val createNewChecklistIdIndex = """CREATE INDEX `index_Checklist_id` ON `Checklist` (`id`)"""
                val createNewChecklistForeignKeyIndex =
                        """CREATE INDEX `index_Checklist_checklist_collection_id`
                            |   ON `Checklist` (`checklist_collection_id`)"""
                            .trimMargin()

                val createTempChecklistItemTable =
                        """CREATE TABLE IF NOT EXISTS `checklist_item_temp`
                            (
                                `id` INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
                                `checklist_id` INTEGER NOT NULL,
                                `name` TEXT NOT NULL,
                                `action` TEXT NOT NULL,
                                `type` TEXT NOT NULL,
                                `order_number` INTEGER NOT NULL,
                                FOREIGN KEY(`checklist_id`) REFERENCES `Checklist`(`id`)
                                ON UPDATE NO ACTION
                                ON DELETE CASCADE
                            )""".trimMargin()

                val populateTempChecklistItemTable =
                        """INSERT INTO `checklist_item_temp`
                            (
                                checklist_id,
                                name,
                                action,
                                type,
                                order_number
                            )
                                SELECT
                                    `checklist_temp`.id,
                                    `ChecklistItem`.name,
                                    `ChecklistItem`.action,
                                    `ChecklistItem`.type,
                                    `ChecklistItem`.id
                                FROM ChecklistItem
                                INNER JOIN `checklist_temp` ON ChecklistItem.checklist_id = checklist_temp.previous_checklist_id
                                """.trimMargin()

                val dropChecklistItemIdIndex =
                        """DROP INDEX IF EXISTS index_ChecklistItem_id"""

                val dropChecklistItemForeignKeyIndex =
                        """DROP INDEX IF EXISTS index_ChecklistItem_checklist_id"""

                val dropChecklistItemTable = """DROP TABLE `ChecklistItem`"""

                val renameChecklistItemTempTable =
                        """ALTER TABLE `checklist_item_temp` RENAME TO `ChecklistItem`"""

                val createChecklistItemIdIndex =
                        """CREATE INDEX `index_ChecklistItem_id` ON `ChecklistItem` (`id`)"""

                val createChecklistItemForeignKeyIndex =
                        """CREATE INDEX `index_ChecklistItem_checklist_id`
                            |   ON `ChecklistItem`(`checklist_id`)""".trimMargin()

                val dropTempChecklistTable = """DROP TABLE checklist_temp"""

                database.execSQL(dropAircraftIndex)
                database.execSQL(createTempAircraftTable)
                database.execSQL(populateTempAircraftTable)
                database.execSQL(dropAircraftTable)
                database.execSQL(renameTempAircraftTable)
                database.execSQL(createAircraftIndex)

                database.execSQL(createTempChecklistCollectionTable)
                database.execSQL(populateTempChecklistCollectionTable)
                database.execSQL(createChecklistCollectionTable)
                database.execSQL(createChecklistCollectionTable)
                database.execSQL(populateChecklistCollectionTable)
                database.execSQL(createChecklistCollectionIndex)
                database.execSQL(createChecklistCollectionForeignKeyIndex)
                database.execSQL(dropTempChecklistCollectionTable)

                database.execSQL(createTempChecklistTable)
                database.execSQL(populateTempChecklistTable)
                database.execSQL(dropChecklistIdIndex)
                database.execSQL(dropChecklistForeignKeyIndex)
                database.execSQL(dropChecklistTable)

                database.execSQL(createNewChecklistTable)
                database.execSQL(populateNewChecklistTable)
                database.execSQL(createNewChecklistIdIndex)
                database.execSQL(createNewChecklistForeignKeyIndex)

                database.execSQL(createTempChecklistItemTable)
                database.execSQL(populateTempChecklistItemTable)
                database.execSQL(dropChecklistItemIdIndex)
                database.execSQL(dropChecklistItemForeignKeyIndex)
                database.execSQL(dropChecklistItemTable)
                database.execSQL(renameChecklistItemTempTable)
                database.execSQL(createChecklistItemIdIndex)
                database.execSQL(createChecklistItemForeignKeyIndex)
                database.execSQL(dropTempChecklistTable)
            }
        }

        val MIGRATION_4_5: Migration = object : Migration(4, 5) {
            override fun migrate(database: SupportSQLiteDatabase) {
                val addManufacturerColumnToAircraft =
                        """ALTER TABLE Aircraft ADD COLUMN `manufacturer` TEXT NOT NULL DEFAULT ''"""
                val addModelColumnToAircraft =
                        """ALTER TABLE Aircraft ADD COLUMN `model` TEXT NOT NULL DEFAULT ''"""
                val addYearColumnToAircraft =
                        """ALTER TABLE Aircraft ADD COLUMN `year` INTEGER NOT NULL DEFAULT 0"""

                database.execSQL(addManufacturerColumnToAircraft)
                database.execSQL(addModelColumnToAircraft)
                database.execSQL(addYearColumnToAircraft)
            }
        }
    }
}
