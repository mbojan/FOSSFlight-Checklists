package org.fossflight.efb.data.io

import org.fossflight.efb.Mockable
import org.fossflight.efb.data.model.Aircraft
import java.io.InputStream

@Mockable
class FileImporter(
        private val fossFlightFileReader: FossFlightFileReader,
        private val checklistParser: ChecklistParser
) {
    @Throws(Exception::class)
    fun importAircraftFromStream(path: String, inputStream: InputStream): Aircraft {
        val fileContents = fossFlightFileReader.readFile(inputStream)
        val aircraft = checklistParser.parse(fileContents)
        return if (aircraft.tailNumber.isNotEmpty()) aircraft
               else aircraft.copy(tailNumber = parseTailNumberFromPath(path))
    }

    private fun parseTailNumberFromPath(path: String): String {
        val split = path.split('/').dropLastWhile { it.isEmpty() }.toTypedArray()
        val filename = split[split.size - 1]

        return if (filename.contains('.')) filename.substring(0, filename.lastIndexOf('.'))
               else filename
    }
}
