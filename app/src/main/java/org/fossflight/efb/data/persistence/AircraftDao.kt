package org.fossflight.efb.data.persistence


import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import io.reactivex.Single
import org.fossflight.efb.data.model.Aircraft

@Dao
interface AircraftDao {

    @get:Query("SELECT * FROM aircraft")
    val aircraft: Single<List<Aircraft>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAircraft(aircraft: Aircraft): Long

    @Query("SELECT * FROM aircraft WHERE id IS :aircraftId")
    fun getAircraft(aircraftId: Long): Single<Aircraft>

    @Query("SELECT tailNumber FROM aircraft WHERE id IS :aircraftId")
    fun getTailNumber(aircraftId: Long): Single<String>

    @Query("UPDATE Aircraft SET tailNumber = :tailNumber, manufacturer = :manufacturer, model = :model, year = :year WHERE id = :aircraftId")
    fun updateTailNumber(aircraftId: Long, tailNumber: String, manufacturer: String, model: String, year: Int): Int

    @Delete
    fun deleteAircraft(aircraft: Aircraft)
}