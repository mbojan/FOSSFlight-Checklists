package org.fossflight.efb.data.model

import androidx.room.Entity
import androidx.room.Ignore
import androidx.room.Index
import androidx.room.PrimaryKey

@Entity(indices = arrayOf(Index("id")))
data class Aircraft(
        @PrimaryKey(autoGenerate = true) var id: Long = 0,
        var tailNumber: String = "",
        var manufacturer: String = "",
        var model: String = "",
        var year: Int = 0,
        @Ignore var checklistCollections: List<ChecklistCollection> = mutableListOf()
)