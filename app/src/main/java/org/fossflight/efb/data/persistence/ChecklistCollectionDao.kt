package org.fossflight.efb.data.persistence

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import io.reactivex.Single
import org.fossflight.efb.data.model.ChecklistCollection

@Dao
interface ChecklistCollectionDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertChecklistCollection(collection: ChecklistCollection): Long

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertChecklistCollections(collections: List<ChecklistCollection>): List<Long>

    @Query("SELECT aircraft_id FROM ChecklistCollection WHERE id IS :checklistCollectionId")
    fun getAircraftIdForChecklistCollection(checklistCollectionId: Long): Single<Long>

    @Query("SELECT * FROM ChecklistCollection WHERE aircraft_id IS :aircraftId")
    fun getChecklistCollections(aircraftId: Long): Single<List<ChecklistCollection>>

    @Query("DELETE FROM ChecklistCollection where id IS :checklistCollectionId")
    fun deleteChecklistCollection(checklistCollectionId: Long)
}