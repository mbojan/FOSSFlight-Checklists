package org.fossflight.efb

import android.os.Bundle
import android.view.LayoutInflater
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment

abstract class FossFlightEditFragment : Fragment(), FossFlightEditView {

    private lateinit var fossFlightFragmentListener: FossFlightFragmentListener

    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        super.onCreateView(inflater, container, savedInstanceState)
        val view = inflateView(inflater, container!!)

        updateToolbar()

        return view
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.edit_options, menu)
        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.save -> save()
            android.R.id.home -> abortEdit()
        }

        return super.onOptionsItemSelected(item)
    }

    open fun updateToolbar() = setHasOptionsMenu(true)

    open fun setParentActivity(listener: FossFlightFragmentListener) {
        fossFlightFragmentListener = listener
    }

    override fun saveSuccessful(name: String) {
        fossFlightFragmentListener.displayToast(getString(R.string.saved_template, name))
        fossFlightFragmentListener.finishFragment()
    }

    override fun abortEdit() = fossFlightFragmentListener.finishFragment()

    abstract fun inflateView(inflater: LayoutInflater, container: ViewGroup): View

    abstract fun save()
}
