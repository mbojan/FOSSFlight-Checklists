### Development Environment Setup
This document assumes you are already familiar with Android Studio and using Gradle from the command line.
if you'd like to contribute, please see the [CONTRIBUTING](./CONTRIBUTING.md) document.

1. clone this repository  
1. import project into Android Studio
1. Install SDK (Android SDK Platform 27 at the time of this writing)
1. Install correct Build Tools (v27.0.3 at the time of this writing)
(Application should build in Android Studio at this point)
1. Create AVD if not created already (at the time of this writing, currently using API 28 x86_64 locally)
1. Start the AVD or connect your physical device
1. Upload the file: <project root>/support/N12345.csv to /sdcard/Download on the AVD/device

1. Tests should now be runnable.
   1. I usually run them using the command line with the command:  
   `./gradlew clean build test connectedAndroidTest`
   1. Jenkins runs them with the command:  
   `./gradlew clean build jacocoTestReport`